from typing import Callable, Dict, List, Tuple

import hypothesis.strategies as st
import numpy as np
import pytest
from hypothesis import Phase, Verbosity, assume, settings
from hypothesis.stateful import (
    Bundle,
    RuleBasedStateMachine,
    consumes,
    initialize,
    rule,
)

from gecocoo.utils.bitfieldsarray import (
    Bitfield,
    BitfieldsArray,
    Mode,
    make_bitfields_array,
)


class BitfieldsArrayComparsion(RuleBasedStateMachine):
    bucket_count: int
    bucket_size: int
    bitfieldsarray: BitfieldsArray

    def __init__(self):
        super(BitfieldsArrayComparsion, self).__init__()
        self.model: np.typing.ArrayLike
        self.fields = None
        self.array = None

    writes = Bundle("writes")
    modifications = Bundle("modifications")

    @initialize(
        bucket_count=st.integers(min_value=100, max_value=10 ** 4),
        bucket_size=st.integers(min_value=1, max_value=16),
        widths=st.lists(
            elements=st.integers(min_value=1, max_value=64), min_size=1, max_size=8
        ),
        write_mode=st.sampled_from(Mode),
    )
    def init_array(
        self,
        bucket_count: int,
        bucket_size: int,
        widths: List[int],
        write_mode: Mode,
    ):
        assume(write_mode != Mode.ATOMIC_SLOTS or sum(widths) <= 128)

        self.fields = list(
            [Bitfield(f"field{i}", width=w) for i, w in enumerate(widths)]
        )
        self.bucket_count = bucket_count
        self.bucket_size = bucket_size
        self.bitfieldsarray = make_bitfields_array(
            bucket_count, self.fields, bucket_size, write_mode
        )
        self.array = np.zeros(self.bitfieldsarray.size, dtype=np.uint64)

        # the model is a NumPy structured array for simplicity
        # all fields are uint64 so that all fields can fit regardless of the field's width
        model_dtype = np.dtype(
            list([(f"field{i}", np.uint64) for i in range(len(widths))])
        )
        self.model = np.zeros((bucket_count, bucket_size), model_dtype)

    @rule(
        target=writes,
        index=st.integers(min_value=0),
        slot=st.integers(min_value=0),
        values=st.lists(elements=st.integers(min_value=0), min_size=8, max_size=8),
    )
    def generate_write(self, index, slot, values):
        return (
            index % self.bucket_count,
            slot % self.bucket_size,
            tuple(
                map(lambda x: x[0] & (2 ** x[1].width - 1), zip(values, self.fields))
            ),
        )

    @rule(target=modifications, w=consumes(writes))
    def execute_write(self, w):
        index, slot, values = w

        self.bitfieldsarray.set_entry(self.array, index, slot, *values)
        self.model[index][slot] = values

        return (index, slot)

    @rule(target=modifications, w=consumes(writes))
    def execute_swap(self, w):
        index, slot, swap_values = w

        # Get the current values and use them as the compare
        # value for cmp_and_swap.
        cmp_values = tuple(self.model[index][slot])
        result = self.bitfieldsarray.cmp_swap_entry(
            self.array, index, slot, *cmp_values, *swap_values
        )
        assert result == True
        self.model[index][slot] = swap_values

        return (index, slot)

    @rule(w=consumes(writes))
    def execute_swap_failing(self, w):
        index, slot, swap_values = w

        # Create modified compare values based on the actual values.
        # This should make cmp_swap_entry fail every time.
        cmp_values = tuple(map(lambda v: v + 1, tuple(self.model[index][slot])))
        result = self.bitfieldsarray.cmp_swap_entry(
            self.array, index, slot, *cmp_values, *swap_values
        )
        assert result == False

    @rule(m=modifications)
    def check_content_at_index(self, m: Tuple[int, int]):
        index, slot = m

        values_actual = self.bitfieldsarray.get_entry(self.array, index, slot)
        values_expected = tuple(self.model[index][slot])

        assert values_actual == values_expected, f"{values_actual} == {values_expected}"


BitfieldsArrayComparsion.TestCase.settings = settings(
    max_examples=100,
    stateful_step_count=100,
    phases=[Phase.generate],
    deadline=None,
)
TestBitfieldsArray = BitfieldsArrayComparsion.TestCase


@pytest.mark.parametrize(
    "mode",
    [
        pytest.param(Mode.ALIGNED_BUCKETS, marks=pytest.mark.xfail(strict=False)),
        pytest.param(Mode.ALIGNED_LOCKED_BUCKETS),
        pytest.param(Mode.PACKED_BUCKETS, marks=pytest.mark.xfail(strict=False)),
        pytest.param(Mode.PACKED_LOCKED_BUCKETS),
        pytest.param(Mode.ATOMIC_SLOTS),
    ],
)
def test_bitfields_concurrency_safe(mode: Mode):
    from numba import njit, prange

    width = 16
    fields = [Bitfield("counter1", width), Bitfield("counter2", width)]
    bucket_count = 1000
    bucket_size = 5
    bfa = make_bitfields_array(bucket_count, fields, bucket_size, mode)
    array = np.zeros(bfa.size, dtype=np.uint64)

    threads = 8
    repeat = 1000
    all_indices = np.repeat(np.arange(bucket_count), repeat)
    indices = np.array([np.random.permutation(all_indices) for _ in range(threads)])

    @njit(parallel=True)
    def work(array, indices):
        for i in prange(threads):
            for index in indices[i]:
                for slot in range(bucket_size):
                    while True:
                        c1, c2 = bfa.get_entry(array, index, slot)
                        if bfa.cmp_swap_entry(
                            array, index, slot, c1, c2, c1 + 1, c2 + 1
                        ):
                            break

    work(array, indices)

    for index in range(bucket_count):
        for slot in range(bucket_size):
            counters = bfa.get_entry(array, index, slot)
            expected = repeat * threads % 2 ** width
            assert counters == (
                expected,
                expected,
            ), f"unexpected counters for index={index} and slot={slot}"
