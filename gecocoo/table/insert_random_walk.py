"""The module provides the implementation of the hash table's insertion function."""

from functools import lru_cache
from typing import Callable

import numpy as np
from numba import boolean, from_dtype, njit, uint64
from numba.types import Tuple, Type, UniTuple

from ..config.lowlevel import LLConfig
from .codegen_utils import make_call_by_index
from .storage import StorageAccess


@lru_cache(maxsize=5)
def define_insert_random_walk(
    config: LLConfig, storage_type: Type, access: StorageAccess
) -> Callable:
    """
    Generate a function that inserts a single key with its associated value into the hash table.

    :param config: the hash table configuration
    :param storage_type: the type of the hash table's storage object
    :param access: the access abstraction object for the hash table
    :return: the generated function
    """
    bucket_count = config.num_buckets
    bucket_size = config.bucket_size
    hash_forward = make_call_by_index(
        UniTuple(uint64, 2)(uint64), [h.forward for h in config.hash_functions]
    )
    hash_backward = make_call_by_index(
        uint64(uint64, uint64), [h.backward for h in config.hash_functions]
    )
    hash_functions_count = len(config.hash_functions)

    first_insert = config.value_module.first_insert
    update_value = config.value_module.update_value
    parameter_count = config.value_module.parameter_count

    relocation_path_dtype = np.dtype(
        [
            # the bucket index and slot of the entry to relocate
            ("index", np.uint64),
            ("slot", np.uint64),
            # the hash index and key part of the entry to relocate
            ("hash_index", np.uint64),
            ("key_part", np.uint64),
            # the hash index used to reach this bucket
            ("source_hash_index", np.uint64),
        ]
    )

    @njit(
        Tuple([boolean, boolean, uint64, uint64, uint64, uint64, uint64])(
            storage_type, uint64
        )
    )
    def find_by_key(storage, key):
        found_relocating = False
        for hash_index in range(hash_functions_count):
            hash, quotient = hash_forward(hash_index, key)
            index = hash % bucket_count
            for slot in range(bucket_size):
                matched, relocating, value = access.compare_entry(
                    storage, index, slot, hash_index, quotient
                )
                found_relocating |= relocating
                if matched and not relocating:
                    return True, False, hash_index, index, slot, quotient, value

        return False, found_relocating, 0, 0, 0, 0, 0

    @njit(Tuple([boolean, uint64, uint64, uint64, uint64])(storage_type, uint64))
    def find_empty_slot_for_key(storage, key):
        for hash_index in range(hash_functions_count):
            hash, quotient = hash_forward(hash_index, key)
            index = hash % bucket_count
            for slot in range(bucket_size):
                (
                    occupied,
                    _,
                    _,
                    _,
                    _,
                ) = access.get_entry(storage, index, slot)

                if not occupied:
                    return True, index, slot, hash_index, quotient

        return False, 0, 0, 0, 0

    @njit(
        Tuple([boolean, uint64, uint64, uint64, uint64, uint64, uint64])(
            storage_type, uint64
        )
    )
    def find_random_slot_for_key(storage, key):
        relocating = True
        while relocating:
            random_value = np.random.randint(0, hash_functions_count * bucket_size)
            hash_index = random_value // bucket_size
            hash, quotient = hash_forward(hash_index, key)
            index = hash % bucket_count
            slot = random_value % bucket_size

            (
                occupied,
                relocating,
                relocate_hash_index,
                relocate_key_part,
                _,
            ) = access.get_entry(storage, index, slot)

        return (
            occupied,
            index,
            slot,
            hash_index,
            quotient,
            relocate_hash_index,
            relocate_key_part,
        )

    @njit(
        Tuple([boolean, uint64])(
            storage_type, from_dtype(relocation_path_dtype)[:], uint64
        )
    )
    def find_relocation_path(storage, path, key):
        for i in range(len(path)):
            # First try finding an empty slot
            (
                found_empty,
                relocate_index,
                relocate_slot,
                hash_index,
                quotient,
            ) = find_empty_slot_for_key(storage, key)
            occupied = False
            relocate_hash_index = uint64(0)
            relocate_key_part = uint64(0)

            # Fall back to finding a random slot
            if not found_empty:
                (
                    occupied,
                    relocate_index,
                    relocate_slot,
                    hash_index,
                    quotient,
                    relocate_hash_index,
                    relocate_key_part,
                ) = find_random_slot_for_key(storage, key)

            # We now found a slot that is either empty or
            # containing a key that is not already being relocated
            current_path_entry = path[i]
            current_path_entry.index = relocate_index
            current_path_entry.slot = relocate_slot
            current_path_entry.hash_index = relocate_hash_index
            current_path_entry.key_part = relocate_key_part
            current_path_entry.source_hash_index = hash_index

            if not occupied:
                # We found an empty slot, our relocation path ends here
                return True, i + 1

            # Restore key to find a relocation slot for it next
            key = hash_backward(relocate_hash_index, relocate_index, relocate_key_part)

        return False, 0

    @njit(boolean(storage_type, from_dtype(relocation_path_dtype)[:], uint64))
    def execute_replacement_path(storage, path, path_length):
        for i in range(1, path_length):
            target_path_entry = path[path_length - i]
            source_path_entry = path[path_length - i - 1]

            # Get the current value and occupancy status of the slot to relocate
            (occupied, _, _, _, relocate_value,) = access.get_entry(
                storage, source_path_entry.index, source_path_entry.slot
            )

            if not occupied:
                # The slot to relocate is already empty, leaving us
                # with nothing to do for this step. Skip to the next one.
                continue

            # Mark the slot as being relocated, so nobody modifies it
            if not access.mark_relocating_entry(
                storage,
                source_path_entry.index,
                source_path_entry.slot,
                source_path_entry.hash_index,
                source_path_entry.key_part,
                relocate_value,
                True,
            ):
                # If the marking failed, either our assumptions about the slot's
                # content are not valid anymore or somebody is already relocating it.
                # Either way our replacement path is now invalid.
                return False

            # We successfully marked the entry as relocating, now move it to the target location.
            relocate_key = hash_backward(
                source_path_entry.hash_index,
                source_path_entry.index,
                source_path_entry.key_part,
            )
            hash, quotient = hash_forward(
                target_path_entry.source_hash_index, relocate_key
            )

            # TODO: Remove, as it should always be true
            assert hash == target_path_entry.index

            # Target entry should be an empty slot, as we have either relocated it already or it is the end of our path.
            if not access.set_empty_entry(
                storage,
                target_path_entry.index,
                target_path_entry.slot,
                False,  # relocating
                target_path_entry.source_hash_index,
                quotient,
                relocate_value,
            ):
                # The target slot seems not to be empty anymore.
                # Cleanup by removing relocating flag from slot as we have no valid target anymore.
                while not access.mark_relocating_entry(
                    storage,
                    source_path_entry.index,
                    source_path_entry.slot,
                    source_path_entry.hash_index,
                    source_path_entry.key_part,
                    relocate_value,
                    False,
                ):
                    # This should only happen for some storage modes when nearby entries
                    # are modified, so we just keep trying.
                    pass

                return False

            # We successfully relocated the entry to the target slot.
            # Clear the source slot, implicitly unmarking the relocating flag
            # and leaving it empty for the next step of our relocation path.
            while not access.clear_entry(
                storage,
                source_path_entry.index,
                source_path_entry.slot,
                True,  # relocating
                source_path_entry.hash_index,
                source_path_entry.key_part,
                relocate_value,
            ):
                # In a correct implementation this is unlikely to happen, but
                # is possible for some storage modes when nearby entries are modified
                # so we just keep trying.
                pass

        # Relocation path was successfully executed
        return True

    @njit(UniTuple(boolean, 2)(storage_type, uint64, UniTuple(uint64, parameter_count)))
    def insert_into_empty_slot(storage, key, params):
        # Try finding an empty slot to put our key in
        found, index, slot, hash_index, quotient = find_empty_slot_for_key(storage, key)

        # Inserting into an empty slot failed, as we could not find one
        if not found:
            return False, False

        # Try replacing the empty entry,
        # but mark it as relocated so no one modifies it
        value = uint64(first_insert(key, *params))
        if not access.set_empty_entry(
            storage,
            index,
            slot,
            True,  # mark as relocating
            hash_index,
            quotient,
            value,
        ):
            # Replacing the empty entry failed, this means it was already claimed
            # by a different thread, possibly having inserted our key.
            return False, False

        # Check for duplications of our key in the table, as somebody else
        # could have inserted the key in a different slot.
        for hash_index_other in range(hash_functions_count):
            hash, quotient_other = hash_forward(hash_index_other, key)
            index_other = hash % bucket_count
            for slot_other in range(bucket_size):
                matched, _, _ = access.compare_entry(
                    storage,
                    index_other,
                    slot_other,
                    hash_index_other,
                    quotient_other,
                )

                if matched and (index_other != index or slot_other != slot):
                    # Another entry of our key was found. We have to remove ours
                    # again so that there are no duplicated entries.
                    while not access.clear_entry(
                        storage,
                        index,
                        slot,
                        True,  # relocating
                        hash_index,
                        quotient,
                        value,
                    ):
                        # This should only happen for some storage modes when nearby entries
                        # are modified, so we just keep trying.
                        pass

                    # We found an empty slot, but did not successfully insert the key
                    return True, False

        # We found no either entries of the same key.
        # Potential other insertions of the same key should find ours
        # and delete theirs so there are no duplicates.
        while not access.mark_relocating_entry(
            storage,
            index,
            slot,
            hash_index,
            quotient,
            value,
            False,
        ):
            # This should only happen for some storage modes when nearby entries
            # are modified, so we just keep trying.
            pass

        # We successfully inserted the key
        return True, True

    @njit
    def insert_into_existing_slot(storage, key, params):
        # Keep looking for an existing entry as long as we find one that
        # is being relocated or our update on the key fails.
        found = False
        relocating = True
        while found or relocating:
            (
                found,
                relocating,
                hash_index,
                index,
                slot,
                key_part,
                old_value,
            ) = find_by_key(storage, key)

            if found:
                new_value = update_value(key, old_value, *params)
                if access.cmp_swap_entry(
                    storage,
                    index,
                    slot,
                    hash_index,
                    key_part,
                    old_value,
                    hash_index,
                    key_part,
                    new_value,
                ):
                    return True

                # TODO: Optimize by retrying the update and not searching for the key again

        return False

    @njit(boolean(storage_type, uint64, UniTuple(uint64, parameter_count)))
    def insert_random_walk(storage, key, params):
        # The algorithm follows the following steps and stops as soon as
        # the we have either performed an update or an initial insert.
        # 1. Try finding an existing key and try updating it
        # 2. Try finding an empty slot and insert the new key, verify that this creates no duplicates
        # 3. Try relocating slots to leave an empty slot for the new key
        # 4. Restart
        # Works similar to algorithms outlined in https://eourcs.github.io/LockFreeCuckooHash/finalreport

        while True:
            # Try finding an existing slot and update it
            if insert_into_existing_slot(storage, key, params):
                return False

            # Try finding an empty slot to put our key in
            empty_slot_found, insert_success = insert_into_empty_slot(
                storage, key, params
            )

            if insert_success:
                # We successfully inserted a new key: Increment the key count and exit
                access.inc_key_count(storage)
                return True

            if not empty_slot_found:
                # We could not find an empty slot for our key.
                # Now calculate a path of slot replacements that leaves us with an empty slot
                # for our key.
                # TODO: Make max_path_length an option or find a good constant. Highly filled tables require longer paths!
                max_path_length = uint64(1000)
                # TODO: This array could be kept in thread local storage to avoid the allocation
                relocation_path = np.zeros(max_path_length, dtype=relocation_path_dtype)
                path_found, path_length = find_relocation_path(
                    storage, relocation_path, key
                )

                if not path_found:
                    # We didn't find a relocation path within the specified max path length
                    # That means our insertion failed.
                    raise Exception("failed to find replacement path")

                execute_replacement_path(storage, relocation_path, path_length)
                # TODO: if executing fails, we could just try finding a new path, instead of starting from the beginning

    # Wrapper to make the star args work
    # TODO: Maybe there is a way to avoid this
    @njit(boolean(storage_type, uint64, UniTuple(uint64, parameter_count)))
    def insert_random_walk_starargs_wrapper(storage, key: uint64, *params) -> boolean:
        return insert_random_walk(storage, key, params)

    return insert_random_walk_starargs_wrapper
