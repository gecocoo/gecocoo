.. _CustomValueModules:

Custom Value Modules
====================

A Value module is an interface between your applications semantics and the bit-representation inside the table.
It has to translate whichever types you want to use to :attr:`~numba.types.uint64` and back.

You can define your Value Module using parameters from your configuration, either using a function which receives these parameters or as a subclass of :attr:`~gecocoo.valuemodules.baseclass.ValueModuleBase`.
In either case you need to define three different functions which translate between your application and the raw bits and the number of bits your encoding needs inside the table.

These are the Functions you will need to define, all of which need to be annotated with :attr:`~numba.@njit`:

:attr:`first_insert(key,*params)`
  This function is only used when inserting a key which was not present in the table before.
  It is has to return a :attr:`~numba.types.uint64` type value which will be stored in the Table.
  The :attr:`*params` are directly passed through from calls to :attr:`insert(storage,key,*params)`, so they can hold whichever additional information your encoding needs (e.g. the value you want to store).

:attr:`update_value(key,stored_value,*params)`
  This does the same as :attr:`first_insert` but for all subsequent insert operations.
  The steps were separated so a user can focus on their semantics and not have to check if the previous value was empty.
  Checking if the entry was empty here would redundant, because the inserting algorithm has to check for that anyways and then decides which function to call.

:attr:`map_value(key,stored_value)`
  This is where you reverse from :attr:`~numba.types.uint64` to whatever value your application uses.
  This is called whenever you use :attr:`search(storage,key)`.

Example Code
------------

These are multiple examples either as :attr:`~gecocoo.valuemodules.baseclass.ValueModuleBase` or simple generator functions.

.. literalinclude:: ../../../examples/valuemodules.py
  :language: python
  :linenos:
