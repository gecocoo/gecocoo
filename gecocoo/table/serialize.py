"""The module provides serialization and de-serialization of generated hash tables."""

import json
from json.encoder import JSONEncoder
from typing import IO, Any, Dict, Optional, Tuple, Type, Union

import numpy as np

from gecocoo.config.lowlevel import LLConfig, LLConfigJSONEncoder, from_json
from gecocoo.generator import generate_hashtable
from gecocoo.interface import HashtableInterface, HashtableUtilities


def serialize(
    file: Union[str, IO],
    config: LLConfig,
    data_array: np.ndarray,
    metadata: Optional[Dict[str, Any]] = None,
    compress: bool = False,
) -> None:
    """
    Serialize the hash table to a file on the file system.

    An exact copy of the hash table at the time of serialization can then later be restored using :func:`deserialize`.

    :param file:
        Either the filename (string) or an open file (file-like object) where the data will be saved.
        If file is a string or a Path, the .npz extension will be appended to the filename if it is not already there.
        (See ``file`` argument of :func:`numpy.savez`)
    :param config:  the hash table's configuration for restoring it later
    :param data_array:  the hash table’s backing storage array
    :param metadata: optionally a dictionary of to append to the hash table as metadata, defaults to None
    :param compress:
        If ``True`` the saved data will be compressed using :attr:`zipfile.ZIP_DEFLATED`.
        See :func:`numpy.savez_compressed` for details.
    """

    config_array = _serialize_to_array(config, encoder_class=LLConfigJSONEncoder)
    metadata_array = _serialize_to_array(metadata)

    save = np.savez if not compress else np.savez_compressed
    save(file, config=config_array, data=data_array, metadata=metadata_array)


def deserialize(
    file: str,
) -> Tuple[HashtableInterface, HashtableUtilities]:
    """
    Deserialize a hash table that was saved previously saved to the file system.

    The result is basically the same as with calling :func:`gecocoo.generator.generate_hashtable`,
    except that the hash table comes filled with the serialized data.
    Also :attr:`gecocoo.interface.HashtableUtilities.metadata` will be filled with the previously stored metadata.

    :param file:
        Either the filename (string) or an open file (file-like object) where the data will be saved.
        If file is a string or a path, the .npz extension will be appended to the filename if it is not already there.
        (See ``file`` argument of :func:`numpy.load`)
    :return:
        a tuple consisting of

        - the hash table interface, usable in jit-compiled functions
        - the hash table utilities, **not** usable in jit-compiled functions
    """

    with np.load(file, allow_pickle=False) as npz_file:
        config_array = npz_file["config"]
        data_array = npz_file["data"]
        metadata_array = npz_file["metadata"]

        config = _deserialize_from_array(config_array, from_json)
        interface, utils = generate_hashtable(config, reuse_array=data_array)
        utils.metadata = _deserialize_from_array(metadata_array)

        return interface, utils


def _serialize_to_array(
    obj: Any, encoder_class: Optional[Type[JSONEncoder]] = None
) -> np.ndarray:
    """Serialize the object to a Numpy array using json encoding."""

    json_str = json.dumps(obj, cls=encoder_class)
    return np.frombuffer(json_str.encode("utf-8"), dtype=np.uint8)


def _deserialize_from_array(array: np.ndarray, object_hook=None):
    """Deserialize the object from a Numpy array using json decoding."""

    return json.loads(array.tobytes().decode("utf-8"), object_hook=object_hook)
