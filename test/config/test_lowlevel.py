import hypothesis.strategies as st
import pytest
from hypothesis import assume, given, settings

from gecocoo.config import (
    Config,
    ConfigError,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.config.lowlevel import resolve_config
from gecocoo.hashing import swaplinear, xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.valuemodules import counting, keyset, store


def default_config(
    bucket_size=4,
    k=16,
    concurrent=True,
    load_factor=0.9,
    value_module=None,
    hash_functions=None,
    storage: StorageConfig = None,
    n=None,
):
    return Config(
        bucket_size=bucket_size,
        k=k,
        load_factor=load_factor,
        value_module=value_module
        if value_module is not None
        else ValueModuleConfig(get_qualified_name(counting), [32]),
        hash_functions=hash_functions
        if hash_functions is not None
        else [
            HashFunction(get_qualified_name(xorlinear), []),
            HashFunction(get_qualified_name(swaplinear), []),
        ],
        storage=storage,
        n=n,
    )


@given(
    bucket_size=st.integers(min_value=1, max_value=32),
    k=st.integers(min_value=1, max_value=32),
    load_factor=st.floats(min_value=0.5, max_value=1.00),
    value_module=st.sampled_from(
        [
            get_qualified_name(counting),
            get_qualified_name(store),
            get_qualified_name(keyset),
            "counting",
            "store",
            "keyset",
        ]
    ).map(ValueModuleConfig),
    hash_functions=st.lists(
        min_size=1,
        elements=st.sampled_from(
            [
                get_qualified_name(xorlinear),
                get_qualified_name(swaplinear),
                "swaplinear",
                "xorlinear",
            ]
        ).map(HashFunction),
    ),
    storage_mode=st.sampled_from(StorageMode),
    concurrent=st.booleans(),
    disable_quotienting=st.booleans(),
    n=st.one_of([st.none(), st.integers(1, 10 ** 6)]),
    override_n=st.one_of([st.none(), st.integers(10 ** 3, 10 ** 6)]),
)
@settings(deadline=None)
def test_resolve_config(
    bucket_size,
    k,
    load_factor,
    value_module,
    hash_functions,
    storage_mode,
    concurrent,
    disable_quotienting,
    n,
    override_n,
):
    # Assume either n in the config or override_n is specified
    assume(n or override_n)

    # fast storage only works with quotienting disabled
    disable_quotienting = storage_mode == StorageMode.FAST or disable_quotienting

    config = Config(
        bucket_size=bucket_size,
        k=k,
        load_factor=load_factor,
        value_module=value_module,
        hash_functions=hash_functions,
        storage=StorageConfig(
            mode=storage_mode,
            concurrent=concurrent,
            disable_quotienting=disable_quotienting,
        ),
        n=n,
    )

    llc = resolve_config(config, override_n)

    assert llc.key_bits <= 2 ** (2 * k)
    assert llc.value_bits >= 0
    assert llc.num_buckets * load_factor - (override_n or n) < 100
    assert llc.bucket_size == bucket_size
    assert llc.value_module.name == value_module.module_name
    assert callable(llc.value_module.first_insert)
    assert callable(llc.value_module.update_value)
    assert llc.storage is not None
    assert llc.storage.mode == storage_mode
    assert llc.storage.concurrent == concurrent
    assert llc.storage.disable_quotienting == disable_quotienting
    assert len(llc.hash_functions) == len(hash_functions)

    # Assert hash functions have been resolved correctly
    for unresolved, resolved in zip(hash_functions, llc.hash_functions):
        assert resolved.name == unresolved.name
        assert callable(resolved.forward)
        assert callable(resolved.backward)


@given(bucket_size=st.integers())
def test_invalid_bucket_size(bucket_size: int):
    # We only want to check invalid values
    assume(bucket_size < 1)

    config = default_config(bucket_size=bucket_size)
    with pytest.raises(ConfigError):
        resolve_config(config, 10 ** 6)


@given(k=st.integers())
def test_invalid_k(k: int):
    # We only want to check invalid values
    assume(k < 1 or k > 32)

    config = default_config(k=k)
    with pytest.raises(ConfigError):
        resolve_config(config, 10 ** 6)


@given(load_factor=st.floats())
def test_invalid_load_factor(load_factor: float):
    # We only want to check invalid values
    assume(load_factor < 0 or load_factor > 1)

    config = default_config(load_factor=load_factor)
    with pytest.raises(ConfigError):
        resolve_config(config, 10 ** 6)


def test_zero_hash_functions():
    config = default_config(hash_functions=[])
    with pytest.raises(ConfigError):
        resolve_config(config, 10 ** 6)


@given(
    hash_functions=st.lists(
        min_size=1,
        elements=st.sampled_from(
            ["invalid", "invalid_name", "gecocoo.hashing.invalid_name", "gecocoo.foo"]
        ).map(HashFunction),
    )
)
@settings(deadline=None)
def test_hash_functions_invalid_name(hash_functions):
    config = default_config(hash_functions=hash_functions)
    with pytest.raises(AttributeError):
        resolve_config(config, 10 ** 6)


@given(
    hash_functions=st.lists(
        min_size=1,
        elements=st.sampled_from(["foo.bar.invalid", "gecocoo.bar.invalid_name"]).map(
            HashFunction
        ),
    )
)
@settings(deadline=None)
def test_hash_functions_invalid_module(hash_functions):
    config = default_config(hash_functions=hash_functions)
    with pytest.raises(ModuleNotFoundError):
        resolve_config(config, 10 ** 6)


@given(
    value_module=st.sampled_from(
        ["invalid", "invalid_name", "gecocoo.valuemodules.invalid_name", "gecocoo.foo"]
    ).map(ValueModuleConfig),
)
def test_value_module_invalid_name(value_module):
    config = default_config(value_module=value_module)
    with pytest.raises(AttributeError):
        resolve_config(config, 10 ** 6)


@given(
    value_module=st.sampled_from(["foo.bar.invalid", "gecocoo.bar.invalid_name"]).map(
        ValueModuleConfig
    )
)
def test_value_module_invalid_module(value_module):
    config = default_config(value_module=value_module)
    with pytest.raises(ModuleNotFoundError):
        resolve_config(config, 10 ** 6)


@pytest.mark.parametrize("n", [-10000, -1231, -1, 0])
def test_invalid_n(n):
    config = default_config(n=n)
    with pytest.raises(ConfigError) as error:
        resolve_config(config)
    assert error.value.attribute == "n"
