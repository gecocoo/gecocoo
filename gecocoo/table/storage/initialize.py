"""The module contains the initialization function for the available storage modes."""

from typing import Any, Optional, Tuple, Type

from numba.core.types.npytypes import Array

from gecocoo.config import StorageMode
from gecocoo.config.lowlevel import LLConfig

from .buckets import generate_storage_bitfields_buckets
from .fast import generate_storage_fast
from .interface import StorageAccess


def initialize_storage(
    config: LLConfig,
    given_array: Optional[Array] = None,
) -> Tuple[Any, Type, StorageAccess]:
    """
    Initialize a new instance of the storage based on the given configuration.

    :param config: the configuration
    :param given_array: optionally an array to reuse for the storage, which must have the required size
    :return:
        a tuple consisting of

        - the storage's backing object
        - the storage object's type for Numba signatures
        - the storage access abstraction functions
    """

    if (
        config.storage.mode == StorageMode.BUCKETS
        or config.storage.mode == StorageMode.PACKED
    ):
        return generate_storage_bitfields_buckets(config, given_array)

    if config.storage.mode == StorageMode.FAST:
        return generate_storage_fast(config, given_array)

    raise NotImplementedError(f"storage mode {config.storage.mode} is not implemented")
