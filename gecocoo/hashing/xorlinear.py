"""The module provides the :func:`xorlinear` hash function."""

from math import floor, log2
from typing import List

import numpy as np
from numba import njit, uint64
from numba.types import UniTuple

from .utils import inverse_mult


def xorlinear(num_buckets: int, k: int, seed: int, parameters: List[int]):
    """
    Generate a reversible hash function for given parameters.

    The hash function works similarly to :func:`~gecocoo.hashing.swaplinear`, but also
    applies an xor to the key to get a better hashing distribution especially for small keys.

    :param num_buckets: the number of buckets to address
    :param k: specifies the key space, where ``k`` means k-mers of size ``k``
    :param seed: a seed for a random hash function
    :param parameters: optional parameters for the hash function (currently unused)
    :return:
        a tuple consisting of

        - the forward function for hashing
        - the backward function for restoring a key
        - the number of bits the hash function requires for restoring keys
    """

    rng = np.random.RandomState(int(seed) % 32 ** 2)
    hash_seed = 0
    while hash_seed % 2 == 0:
        hash_seed = rng.randint(1, 2 ** 64 - 1, dtype=np.uint64)
    mask_2k_bits = uint64(2 ** (2 * k) - 1)
    mask_k_bits = uint64(2 ** k - 1)
    mask_upper_k_bits = uint64(mask_2k_bits - mask_k_bits)
    inverse_seed = uint64(inverse_mult(int(hash_seed), 4 ** k))
    keybits = max(0, 2 * k - floor(log2(num_buckets)))

    # Enforce types for numba
    k = uint64(k)
    num_buckets = uint64(num_buckets)

    @njit(
        UniTuple(uint64, 2)(uint64),
        locals=dict(
            shifted_upper_half=uint64,
            shifted_lower_half=uint64,
            rot=uint64,
            g=uint64,
            hash_index=uint64,
            quotient=uint64,
        ),
    )
    def forward(key: uint64):
        shifted_upper_half = key << k & mask_2k_bits
        shifted_lower_half = (key ^ shifted_upper_half) >> k
        rot = shifted_upper_half | shifted_lower_half
        g = (hash_seed * rot) & mask_2k_bits
        hash_index = g % num_buckets
        quotient = g // num_buckets
        return hash_index, quotient

    @njit(
        uint64(uint64, uint64),
        locals=dict(
            g=uint64,
            rot=uint64,
            shifted_upper_half=uint64,
            shifted_lower_half=uint64,
            key=uint64,
        ),
    )
    def backward(hash_index: uint64, quotient: uint64):
        g = quotient * num_buckets + hash_index
        rot = (inverse_seed * g) & mask_2k_bits
        shifted_upper_half = rot & mask_upper_k_bits
        shifted_lower_half = (rot & mask_k_bits) ^ (shifted_upper_half >> k)
        key = shifted_lower_half << k ^ shifted_upper_half >> k
        return key

    return forward, backward, keybits
