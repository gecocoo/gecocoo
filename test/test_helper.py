import numpy as np
import pytest

import gecocoo.hashing
import gecocoo.valuemodules
from gecocoo.helper import generate_random_kmers, get_qualified_name


@pytest.mark.parametrize(
    "input,expected_result",
    [
        (gecocoo.hashing.swaplinear, "gecocoo.hashing.swaplinear.swaplinear"),
        (
            gecocoo.valuemodules.default.counting,
            "gecocoo.valuemodules.default.counting",
        ),
        (gecocoo.valuemodules.counting, "gecocoo.valuemodules.default.counting"),
    ],
)
def test_get_qualified_name(input, expected_result):
    result = get_qualified_name(input)

    assert result == expected_result


@pytest.mark.parametrize("k", [10, 15, 27, 32])
def test_generate_random_kmers_uniformly_distributed(k):
    bins = 4
    bin_width = 4 ** k / bins
    count = 10 ** 8

    # Generate data and check distribution using histogram
    data = generate_random_kmers(k, count)
    hist, _ = np.histogram(data, bins=bins, range=(0, 4 ** k - 1), density=True)

    assert len(data) == count
    assert np.sum(hist * bin_width) == pytest.approx(1, 0.005)
    assert hist * bin_width == pytest.approx(np.full(bins, 1 / bins), 0.005)


@pytest.mark.parametrize("k", [10, 15, 27, 32])
@pytest.mark.parametrize("seed", [1234, 5678])
def test_generate_random_kmers_seed(k, seed):
    count = 10 ** 8
    data1 = generate_random_kmers(
        k,
        count,
        seed=seed,
    )
    data2 = generate_random_kmers(k, count, seed=seed)

    assert len(data1) == count
    assert len(data2) == count
    assert np.all(data1 == data2)
