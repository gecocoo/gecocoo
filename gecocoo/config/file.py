"""The module provides functions for loading a hash table configuration from a file."""

from typing import IO, Optional

from strictyaml import EmptyList, Float, Int, Map
from strictyaml import Optional as YamlOptional
from strictyaml import Regex, Seq, Str, load
from strictyaml.scalar import Bool, EmptyDict, EmptyNone, Enum

from gecocoo.config.definition import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)

# strictyaml schema for configuation file
schema = Map(
    {
        "dimensions": Map(
            {
                YamlOptional("n"): Int(),
                "k": Int(),
                "bucket_size": Int(),
                "load_factor": Float(),
            }
        ),
        "hashfunctions": Map(
            {
                "choices": Int(),
                "default_hash": Str(),
                YamlOptional("functions"): Seq(
                    Regex("default")
                    | Map(
                        {
                            "name": Str(),
                            YamlOptional("parameters", default=None): EmptyList()
                            | Seq(Int()),
                        }
                    )
                ),
            }
        ),
        "values": Map(
            {
                "module_name": Str(),
                YamlOptional("parameters", default=None): EmptyList() | Seq(Int()),
            }
        ),
        YamlOptional("storage", drop_if_none=False): EmptyNone()
        | EmptyDict()
        | Map(
            {
                YamlOptional("mode", default="BUCKETS"): Enum(
                    list(map(lambda x: x.name, StorageMode))
                ),
                YamlOptional("concurrent", default=False): Bool(),
                YamlOptional("disable_quotienting", default=False): Bool(),
                YamlOptional("disable_occupied_flag", default=False): Bool(),
            }
        ),
    }
)


def load_from_yaml(file: IO) -> Config:
    """
    Load a :class:`~gecocoo.config.definition.Config` object from a YAML file.

    :param file: a file object to read the configuration from
    :return: the config object
    """

    content = file.read()
    config = load(content, schema, label=file.name)

    default_hash = config.data["hashfunctions"]["default_hash"]
    hash_functions = []
    for i in range(config.data["hashfunctions"]["choices"]):
        if (
            "functions" in config.data["hashfunctions"]
            and len(config.data["hashfunctions"]["functions"]) > i
        ):
            function = config.data["hashfunctions"]["functions"][i]
            if function == "default":
                hash_functions.append(HashFunction(default_hash, []))
            else:
                hash_functions.append(
                    HashFunction(
                        function["name"], function.get("parameters", None) or []
                    )
                )
        else:
            hash_functions.append(HashFunction(default_hash, []))

    value_module = ValueModuleConfig(
        config.data["values"]["module_name"],
        config.data["values"].get("parameters", None) or [],
    )

    if config.data["storage"] is None:
        storage = None
    else:
        storage = StorageConfig(
            mode=StorageMode[config.data["storage"]["mode"]],
            concurrent=config.data["storage"]["concurrent"],
            disable_quotienting=config.data["storage"]["disable_quotienting"],
            disable_occupied_flag=config.data["storage"]["disable_occupied_flag"],
        )

    return Config(
        n=config.data["dimensions"].get("n", None),
        bucket_size=config.data["dimensions"]["bucket_size"],
        k=config.data["dimensions"]["k"],
        load_factor=config.data["dimensions"]["load_factor"],
        value_module=value_module,
        hash_functions=hash_functions,
        storage=storage,
    )
