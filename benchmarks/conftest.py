import os
import sys

import pytest

MODULES = [
    "benchutils.py",
    # add more modules here
    # file names / paths should be relative to this file
]

base_dir = os.path.dirname(__file__)
for module in MODULES:
    sys.path.append(os.path.join(base_dir, module))


@pytest.hookimpl(hookwrapper=os.environ.get("DEBUG_HANGING") == "1")
def pytest_runtest_call(item):
    file, line, function = item.location
    test = f"{file}:{line} {function} (process {os.getpid()})"
    print("starting", test)
    yield
    print("done", test)
