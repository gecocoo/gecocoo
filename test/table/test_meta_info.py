import pytest

from gecocoo.config.definition import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.valuemodules import store


def test_meta_info():
    config = Config(
        load_factor=0.5,
        k=14,
        bucket_size=2,
        value_module=ValueModuleConfig(get_qualified_name(store)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
        n=1000,
        storage=StorageConfig(mode=StorageMode.FAST, disable_quotienting=True),
    )
    table, utils = generate_hashtable(config)

    assert utils.current_key_count == 0
    assert utils.current_load_factor == pytest.approx(0.0)
    assert utils.capacity == pytest.approx(config.n / config.load_factor)

    for i in range(200):
        table.insert_single(table.storage, i, i)

    assert utils.current_key_count == 200
    assert utils.current_load_factor == pytest.approx(0.1)
    assert utils.byte_size == pytest.approx(utils.capacity * 16, 100)
