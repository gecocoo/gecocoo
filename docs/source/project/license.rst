License
========

GeCoCoo is licensed under the MIT License:

.. literalinclude:: ../../../LICENSE.md
    :language: text