"""The module contains the implementation for the `FAST` storage mode."""

from functools import lru_cache
from os import access
from typing import Any, Tuple

import numpy as np
from numba import boolean, njit, uint64
from numba.core.types.abstract import Type

from gecocoo.config.lowlevel import LLConfig

from .interface import StorageAccess, get_storage_access_signatures
from .keycount import make_key_count_accessors_array


@lru_cache(maxsize=5)
def _get_storage_access(config: LLConfig):
    from numba.core.types import Tuple as NumbaTuple

    from gecocoo.utils.codegen import FunctionGenerator
    from gecocoo.utils.llvm import cmp_xchg_array_128

    bucket_size = config.bucket_size
    # The +1 in the size is reserved for storing the key-count inside the array in the last address
    size = 2 * config.num_buckets * config.bucket_size
    key_count_index = size
    storage_type = uint64[:]
    signatures = get_storage_access_signatures(storage_type)

    assert (
        config.value_bits
        + config.key_bits
        + 1  # occupancy flag
        + int(config.storage.concurrent)  # relocation flag
        <= 128
    ), "key and value bits leave no space for required flags"
    assert (
        config.storage.disable_quotienting
    ), "fast storage only works with quotienting disabled"

    code = FunctionGenerator(
        "values_from_chunk", ["chunk_lo", "chunk_hi"], {"boolean": boolean}
    )
    if config.key_bits < 64:
        code += "occupied = boolean(chunk_lo & (1 << 63))"
    else:
        code += "occupied = boolean(chunk_hi & (1 << 63))"
    if config.key_bits < 63:
        code += "relocating = boolean(chunk_lo & (1 << 62))"
    elif config.key_bits == 64:
        code += "relocating = boolean(chunk_hi & (1 << 62))"
    else:
        code += "relocating = boolean(chunk_hi & (1 << 63))"
    mask = 2 ** config.key_bits - 1
    code += f"key = chunk_lo & 0x{mask:x}"
    mask = 2 ** config.value_bits - 1
    code += f"value = chunk_hi & 0x{mask:x}"
    code += "return occupied, relocating, key, value"
    values_from_chunk = code.generate_jit(
        NumbaTuple([boolean, boolean, uint64, uint64])(uint64, uint64)
    )

    code = FunctionGenerator(
        "values_from_chunk", ["occupied", "relocating", "key", "value"]
    )
    code += "chunk_lo = key"
    code += "chunk_hi = value"
    if config.key_bits < 64:
        code += "chunk_lo |= int(occupied) << 63"
    else:
        code += "chunk_hi |= int(occupied) << 63"
    if config.key_bits < 63:
        code += "chunk_lo |= int(relocating) << 62"
    elif config.key_bits == 64:
        code += "chunk_hi |= int(relocating) << 62"
    else:
        code += "chunk_hi |= int(relocating) << 63"
    code += "return chunk_lo, chunk_hi"
    chunk_from_values = code.generate_jit(
        NumbaTuple([uint64, uint64])(boolean, boolean, uint64, uint64)
    )

    @njit(NumbaTuple([uint64, uint64])(uint64[:], uint64, uint64))
    def get_slot(array, index, slot):
        idx = index * bucket_size * 2 + slot * 2
        return array[idx], array[idx + 1]

    if config.storage.concurrent:
        # Atomically swap the slot's content
        @njit(boolean(uint64[:], uint64, uint64, uint64, uint64, uint64, uint64))
        def cmp_swap_slot(array, index, slot, compare_lo, compare_hi, swap_lo, swap_hi):
            idx = index * bucket_size * 2 + slot * 2
            return cmp_xchg_array_128(
                array, idx, compare_lo, compare_hi, swap_lo, swap_hi
            )

    else:
        # Swap the slot's content without using an atomic instruction.
        # This seems to perform a bit faster, so it makes sense to have
        # differing implementations.
        @njit(boolean(uint64[:], uint64, uint64, uint64, uint64, uint64, uint64))
        def cmp_swap_slot(array, index, slot, compare_lo, compare_hi, swap_lo, swap_hi):
            lo, hi = get_slot(array, index, slot)
            idx = index * bucket_size * 2 + slot * 2
            if compare_lo == lo and compare_hi == hi:
                array[idx] = swap_lo
                array[idx + 1] = swap_hi
                return True
            else:
                return False

    @njit(signatures.compare_entry)
    def compare_entry(storage, index, slot, hash_index, key_part):
        chunk_lo, chunk_hi = get_slot(storage, index, slot)
        occupied, relocating, key, value = values_from_chunk(chunk_lo, chunk_hi)

        if occupied and key_part == key:
            return True, relocating, value
        else:
            return False, False, 0

    @njit(signatures.cmp_swap_entry)
    def cmp_swap_entry(
        storage,
        index,
        slot,
        cmp_hash_index,
        cmp_key_part,
        cmp_value,
        swap_hash_index,
        swap_key_part,
        swap_value,
    ):
        cmp_chunk_lo, cmp_chunk_hi = chunk_from_values(
            True, False, cmp_key_part, cmp_value
        )
        swap_chunk_lo, swap_chunk_hi = chunk_from_values(
            True, False, swap_key_part, swap_value
        )
        return cmp_swap_slot(
            storage,
            index,
            slot,
            cmp_chunk_lo,
            cmp_chunk_hi,
            swap_chunk_lo,
            swap_chunk_hi,
        )

    @njit(signatures.set_empty_entry)
    def set_empty_entry(storage, index, slot, relocating, hash_index, key_part, value):
        cmp_chunk_lo, cmp_chunk_hi = chunk_from_values(False, False, 0, 0)
        swap_chunk_lo, swap_chunk_hi = chunk_from_values(
            True, relocating, key_part, value
        )
        return cmp_swap_slot(
            storage,
            index,
            slot,
            cmp_chunk_lo,
            cmp_chunk_hi,
            swap_chunk_lo,
            swap_chunk_hi,
        )

    @njit(signatures.clear_entry)
    def clear_entry(storage, index, slot, relocating, hash_index, key_part, value):
        cmp_chunk_lo, cmp_chunk_hi = chunk_from_values(
            True, relocating, key_part, value
        )
        swap_chunk_lo, swap_chunk_hi = chunk_from_values(False, False, 0, 0)
        return cmp_swap_slot(
            storage,
            index,
            slot,
            cmp_chunk_lo,
            cmp_chunk_hi,
            swap_chunk_lo,
            swap_chunk_hi,
        )

    @njit(signatures.get_entry)
    def get_entry(storage, index, slot):
        chunk_lo, chunk_hi = get_slot(storage, index, slot)
        occupied, relocating, key, value = values_from_chunk(chunk_lo, chunk_hi)
        return (
            occupied,
            relocating,
            0,  # hash_index
            key,
            value,
        )

    @njit(signatures.mark_relocating_entry)
    def mark_relocating_entry(
        storage, index, slot, hash_index, key_part, value, relocating
    ):
        cmp_chunk_lo, cmp_chunk_hi = chunk_from_values(
            True, not relocating, key_part, value
        )
        swap_chunk_lo, swap_chunk_hi = chunk_from_values(
            True, relocating, key_part, value
        )
        return cmp_swap_slot(
            storage,
            index,
            slot,
            cmp_chunk_lo,
            cmp_chunk_hi,
            swap_chunk_lo,
            swap_chunk_hi,
        )

    (
        get_key_count,
        set_key_count,
        inc_key_count,
        dec_key_count,
    ) = make_key_count_accessors_array(storage_type, key_count_index)

    access = StorageAccess(
        compare_entry=compare_entry,
        cmp_swap_entry=cmp_swap_entry,
        set_empty_entry=set_empty_entry,
        clear_entry=clear_entry,
        get_entry=get_entry,
        mark_relocating_entry=mark_relocating_entry,
        get_key_count=get_key_count,
        set_key_count=set_key_count,
        inc_key_count=inc_key_count,
        dec_key_count=dec_key_count,
    )

    return size + 1, access


def generate_storage_fast(
    config: LLConfig, given_array=None
) -> Tuple[Any, Type, StorageAccess]:
    """
    Generate the an instance of the `FAST` storage mode for the given configuration.

    The `FAST` storage mode trades memory efficiency in favor of accessing performance.
    In general `FAST` will be the fastest supported storage mode, but will require more
    memory (a bigger backing array), since no quotienting is used and more bits are "wasted"
    for alignment.

    :param config: the configuration
    :param given_array: optionally an array to reuse for the storage which must have the required size
    :return:
        a tuple consisting of

        - the storage's backing object
        - the storage object's type for Numba signatures
        - the storage access abstraction functions
    """

    size, access = _get_storage_access(config)
    storage_type = uint64[:]
    assert given_array is None or len(given_array) == size
    array = given_array if given_array is not None else np.zeros(size, dtype=np.uint64)

    return array, storage_type, access
