"""The package contains the interface for the storage abstraction of the hash table and its implementations."""

from .initialize import initialize_storage
from .interface import StorageAccess
