from gecocoo.config import Config, HashFunction, ValueModuleConfig
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import swaplinear, xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.table.serialize import deserialize
from gecocoo.valuemodules.default import store

if __name__ == "__main__":
    config = Config(
        load_factor=0.95,
        k=14,
        bucket_size=4,
        value_module=ValueModuleConfig(get_qualified_name(store)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear), []),
            HashFunction(get_qualified_name(swaplinear), []),
        ],
        n=1000,
    )

    # Generate a hash table to work with
    table, utils = generate_hashtable(config)

    # Work with the table: insert keys etc.
    table.insert_single(table.storage, 123, 456)
    table.insert_single(table.storage, 456, 789)
    table.insert_single(table.storage, 789, 123)

    # Attach meta data to the hash table that will also be saved to the file.
    utils.metadata = dict(
        contents=["nothing", "nichts", "nada", "niente"],
        message="An empty Table.",
    )

    # Save using path string
    utils.serialize("mydata1.npz")

    # Note that NumPy will automatically append the ".npz" file extension
    # when saving to paths, if does not already exist in the name.
    # So either add it yourself or be prepared for the look for the correct file.
    # The following will save to a file named "mydata2.npz".
    utils.serialize("mydata2")

    # Save using file object. This allows using any file extension.
    # Make sure to use binary mode!
    with open("mydata.gecocoo", "wb") as file:
        utils.serialize(file)

    # It is also possible to compress the saved data.
    # Especially for almost empty tables this will greatly
    # reduce the file size. By default files are saved uncompressed.
    utils.serialize("mydata3.npz", compress=True)

    # Loading the hash table is similiar to generating one,
    # but instead of a config you pass a path or file object
    table_deserialized, utils_deserialized = deserialize("mydata.gecocoo")

    # You can work with the hash table as normal.
    # The hash table's content will be same as the one you saved.
    assert utils_deserialized.metadata == utils.metadata
    assert list(table.key_value_iterator(table.storage)) == list(
        table_deserialized.key_value_iterator(table_deserialized.storage)
    )

    # Modifying the loaded table obviously does not effect the old table
    table_deserialized.insert_single(table_deserialized.storage, 999, 111)
    assert table_deserialized.search_single(table_deserialized.storage, 999) == 111
    assert table.search_single(table.storage, 999) is None
