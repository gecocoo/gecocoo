Hash Table Usage
================

The gecocoo hashtable package implements a cuckoo hashtable for the storing of key-value pairs with up to a size of 64 bits each.

The package supports four main functionalities:
* The configuration of the table
* The typical main memory operations on the table such as searching, inserting, updating, deleting and iterating entries
* Saving and loading the hash table from the file
* Benchmarking and visualizing the access time and occupancy of the table


Configuring a Hash Table
--------------------------
As every scenario of using the hash table requires the presence of a hash table in the main memory, a table needs to be either loaded from a file or configured at some point.

The process of configuration can be performed in two ways, either by creating and initializing a configuration object at runtime or by loading a configuration from a file.
Both methods of configuring a hash table require the same information to be input, just through a different interface. The required information consists of:

The load factor
^^^^^^^^^^^^^^^
While the number of expected keys to be inserted into the table gets set when the table gets generated, the load factor decides how large a portion of the table is going to be taken up by that given number.
Effectively this means, that the table will always reserve so much memory that the expected key count takes up the load factor of the table.
Choosing load factors close to 1.0 may result in a table that cannot accommodate all the keys that are supposed to be inserted into the table.

The k-mer size
^^^^^^^^^^^^^^
Because the hash table has been designed with the management of k-mers in mind, it's configuration is in part limited by this fact.
Therefore the size of each entry in the table cannot be specified in bits or bytes but insteas in multiples of k-mer units which each take up 2 bits.
Taking the 64 bit limitation into consideration this allows for values from 1 to 32 to be passed as k-mer sizes resulting in twice the specified amount of bits being taken up by each key.

The bucket size
^^^^^^^^^^^^^^^
The number of keys that can be stored in a full bucket. This parameter has no major influence on the amount of memory that gets reserved.
It has however an influence on the number of cache misses and therefore performance of operations on the table. When choosing it, it is recommended to calculate the expected size of one table in such a manner, that one bucket fits as closely as possible into a cache line.
Using larger bucket sizes also statistically correlates with higher possible load factors.

The value module
^^^^^^^^^^^^^^^^
A value module, that defines the type of value that is going to be stored in the hash table and the way it is going to be accessed in insert and update operations.
Three value modules are already predefined and they are the counting, keyset and store value module. Further value modules can be defined by overriding the base class.

* The counting value module causes the single_insert function to not require parameters because it simply increments the stored value by a user defined increment.
* The store value module requires one parameter for the single_insert function which is the new value to be stored in the spot of the given key.
* The keyset value module makes the hash table function as a simple set thus requiring no value at all and also no parameters for the single_insert function.

The reason for defining value modules this way is that operations which require the knowledge of the previous value to compute a new one for an update would require one search followed by an insert call.
By defining value modules the computations for the position in the hash table only need to be performed once.

The storage configuration
^^^^^^^^^^^^^^^^^^^^^^^^^
The storage configuration allows further configuration for the way the memory is structured that mainly influences the performance and memory usage of the hash table.
The storage configuration contains three attributes, the storage mode, the concurrency flag and the quotienting flag.

The storage mode has three different modes: packed, fast or buckets.
Using the packed mode results in a poor performance but will result in no memory being wasted for allignment.
Using the the fast mode will store every individual piece of data, like the keys and values of each entry, in a seperate entry of a numpy array. This means that every piece of data is can be accessed without bit operations but every entry component that does not require 64 bits will waste the missing bits. It will also not work with quotienting thus resulting in fewer operations for each lookup, update and so on.
Finally using buckets mode will result in a midpoint between packed and fast mode, which balances performance and memory efficieny. Every bucket in total will be alligned to a cache line thus reducing cache misses while memory will only be wasted to allign to 64 byte cache lines and not to 64 bit memory cells.

Having the concurreny flag set to true will result in a small additional cost in memory while allowing the calling of the same access functions such as insert or delete from multiple threads.

Having the flag for the disabling of quotienting set to true will cause the keys to be stored as they are, resulting in no decrease in memory usage, whereas setting it to false will derive the key from the index of the bucket and some compressed information thus resulting in a lower memory usage.
The fast mode only works with quotienting disabled.

The hash functions
^^^^^^^^^^^^^^^^^^
A list of hash functions to use for the hashing of the keys.
The currently supported hash functions are the swaplinear and xorlinear functions.

Configuring a Hash Table in code
--------------------------------
Configuring a hash table in code allows for a lot of dynamic choices to be made, while it also makes the configuration less reusable.
Doing it requires the necessary packages to be included which include the predefined options for the configuration such as predefined hash functions or storage modes.

The following code segment represents the most common choices that can be made to configure a hash table in code::

	# Import hash table generator
	from gecocoo.generator import generate_hashtable
	
	# Import basic config classes
	from gecocoo.config import (
		Config,
		HashFunction,
		StorageConfig,
		StorageMode,
		ValueModuleConfig,
	)
	# Import the two predefined hash functions
	from gecocoo.hashing import xorlinear, swaplinear
	# Import name loader
	from gecocoo.helper import get_qualified_name
	# Import predefined value modules
	from gecocoo.valuemodules import counting, store, keyset
	
	# Create configuration object:
	config = Config(
		load_factor=0.95, # Defines a load factor 
		k=14,  # k-mer size: 1-32
		bucket_size=4, # Number of entries per bucket
		# Define the value module:
		# Alternatives to 'store', are 'counting' and 'keyset' or a self defined value module using the given base class
		value_module=ValueModuleConfig(get_qualified_name(store)),
		# Defines the hash functions, in this case 2 are being used.
		# Alternatively the swaplinear function may be used and both can be parametrized according to their definition.
		hash_functions=[
			HashFunction(get_qualified_name(xorlinear)), 
			HashFunction(get_qualified_name(xorlinear)),
		],
		# Defines the storage mode:
		storage=StorageConfig(
			# mode=StorageMode.PACKED, # Best for best memory efficiency, but worst performance
			# mode=StorageMode.FAST, # Best for best performance, but worst memory efficiency
			mode=StorageMode.BUCKETS,  # Good memory efficiency + good performance
			concurrent=False,  # True to enable conccurrency support
			disable_quotienting=False,  # must be True for FAST storage mode!
		),  # storage=None for default configuration
	)
	
	# Generates the table and also passes the number of expected keys to be stored (in this case 10^6):
	table, utils = generate_hashtable(config, 10 ** 6)


Configuring a Hash Table in file
-------------------------------- 
Configuring a hash table in file requires the definition of a .yaml file with the following structure::

	# Define the size-determining parameters of the table:
	dimensions:
	  # Specify the number of unique keys to be stored in the hash table.
	  # This can be overriden on generation of the hash table if the
	  # value is not previously known.
	  n: -1

	  # Specify the k-mer size as an integer 1 <= k <= 32, 
	  # resulting in a key size of 2k bits in the hash table.
	  k: 31

	  # Specify the bucket size of the hash table.
	  bucket_size: 4

	  # Specify the desired load factor of the hash table.
	  load_factor: 0.98

	# Define how values are stored in the hash table.
	values:
	  # Specify the fully qualified name of the value module constructor.
	  # For value modules in gecocoo.valuemodules.default` a short name can be used.
	  module_name: "gecocoo.valuemodules.store"
	  
	  # Specify parameters for the value module that will be passed to the constructor.
	  parameters:
		- 32

	# Define the number and type and parameters of the hash functions.
	hashfunctions:
	  # Specify the number of hash functions, 1 <= choices <= 255.
	  choices: 5

	  # Specify the default hash function to use for the hash table.
	  # Either specify the fully qualified name of the constructor or
	  # use the short name for hash functions included in gecocoo.
	  default_hash: xorlinear

	  # Optionally define concrete hash functions with parameters.
	  # The order of definitions here matters, as it defines the order
	  # of the hash function in the hash table.
	  functions:
		# Specify using explicit name and parameters.
		- name: swaplinear
		  parameters:
			- 23
		# Use the default hash function specified above.
		- default
		# Specify using explicit name, but use default parameters (-> randomly seeded hash function).
		- name: gecocoo.hashing.swaplinear
		  parameters:
		# Same as before: specify only using name.
		- name: xorlinear


Once an appropriate config file has been defined like that it can be used in code like this::

	import os

	from gecocoo.config.file import load_from_yaml
	from gecocoo.generator import generate_hashtable

	config_path = os.path.join(os.path.dirname(__file__), "example_config.yaml")

	if __name__ == "__main__":
		with open(config_path) as file:
			config = load_from_yaml(file)

		# Generates the table and also passes the number of expected keys to be stored (in this case 10^6):
		table, utils = generate_hashtable(config, 10**6)


The basic operations on a Hash Table
------------------------------------

Once configured the hash table is ready to be used as a map data structure. In this state many of the basic operations known from map datastructures can be performed on the table.

These include the following:

Inserting or updating a single key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Performs an insert if the key is not present and returns true in that case, if it is already present, otherwise it updates the existing value and returns False::

   from gecocoo.generator import generate_hashtable
   
   # An initialized hash table:
   table, utils = generate_hashtable(config)
   
   # Prints True
   print(table.insert_single(table.storage, 2, 1))
   
   # Prints False
   print(table.insert_single(table.storage, 2, 2))

Deleting a single key
^^^^^^^^^^^^^^^^^^^^^
Deletes the key along with it's value from the table and returns the value if it was present or None otherwise::

   from gecocoo.generator import generate_hashtable
   
   # An initialized hash table:
   table, utils = generate_hashtable(config)
   
   # Inserts 2
   table.insert_single(table.storage, 2, 1)
   
   # Prints 1
   print(table.remove_single(table.storage, 2))
   
   # Prints None
   print(table.remove_single(table.storage, 2))

Searching for a single key
^^^^^^^^^^^^^^^^^^^^^^^^^^
Searches for the key and returns its value if present or None otherwise::

   from gecocoo.generator import generate_hashtable
   
   # An initialized hash table:
   table, utils = generate_hashtable(config)

   # Inserts 2
   table.insert_single(table.storage, 2, 1)
   
   # Prints 1
   print(table.search_single(table.storage, 2))
   
   # Prints None
   print(table.search_single(table.storage, 3))

Clearing the table
^^^^^^^^^^^^^^^^^^
Can be used to reuse a given configuration on the same memory with different keys::

   from gecocoo.generator import generate_hashtable
   
   # An initialized hash table:
   table, utils = generate_hashtable(config)
   
   table.clear(table.storage)

Iterating over the entries
^^^^^^^^^^^^^^^^^^^^^^^^^^
Provides a simple iterator for the entries already present in the table. Though inserting new keys while iterating may or may not yield them in the iteration due to the nature of the hash tables implementation::

   from gecocoo.generator import generate_hashtable
   
   # An initialized hash table:
   table, utils = generate_hashtable(config)

   # Inserts a couple of keys
   table.insert_single(table.storage, 1, 1)
   table.insert_single(table.storage, 2, 2)

   # Prints 1 and 2 in an order that depends on the configuration:
   for key, val in table.debug_iterator(table.storage):
      print(key)

Getting the key count
^^^^^^^^^^^^^^^^^^^^^
Can be used to get a ruff estimate of how many keys can still be inserted into the table::

   from gecocoo.generator import generate_hashtable
   
   # An initialized hash table:
   table, utils = generate_hashtable(config)
   
   print(table.get_key_count(table.storage))

Initialize for efficient usage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
When a table has a known key set for which only the values will get changed or lookups performed then using the initialization function will significantly improve the average access time.
To do this the initial key set has to be passed to the initialize function optionally with the values that are associated with them initially::

   from gecocoo.generator import generate_hashtable
   import numpy as np
   
   # An initialized hash table:
   table, utils = generate_hashtable(config)

   # Creates a key and value array:
   keys = np.array([1,2,3,4,5], dtpye=np.uint64)
   values = np.array([1,1,1,2,2], dtpye=np.uint64)

   # initializes the table:
   table.initialize(table.storage, keys, values)

Loading and Saving a Hash Table from/to a file
----------------------------------------------
As the data stored in a hash table is often times of use even after finishing the execution of the process it has been filled in the gecocoo package provides functions for serializing and deserializing hash tables that were created using it.

Saving a Hash Table
^^^^^^^^^^^^^^^^^^^
Saving a hash table can be done in a number of ways. 

The simplest is to just call the serialize function of the utils object with no file extension or a .npz one::

   from gecocoo.generator import generate_hashtable
   
   # An initialized hash table:
   table, utils = generate_hashtable(config)
   
   # Saves the table to the file table1.npz:
   utils.serialize("table1.npz")
   
   # Saves the table to the file table2.npz:
   utils.serialize("table2")

To save to a file with a different file extension the file has to be explicitly saved in a binary format like this::

   from gecocoo.generator import generate_hashtable
   
   # An initialized hash table:
   table, utils = generate_hashtable(config)
   
   # Saves the table to the file table1.gecocoo:
   with open("table1.gecocoo", "wb") as file:
      utils.serialize(file)

Finally the file can also be compressed when saving by setting the compress parameter to true::

   from gecocoo.generator import generate_hashtable
   
   # An initialized hash table:
   table, utils = generate_hashtable(config)
   
   # Saves the table to the file table1.npz:
   utils.serialize("table1.npz", compress=True)

Loading a Hash Table
^^^^^^^^^^^^^^^^^^^^
Loading a hash table from a file works similar to the way configuring one works. It solely requires the file name to be passed to the deserialize function and returns a named tuple representing the table and a utils object.
These can be used the same way as those of a generated hash table.
An example can be seen here::

   from gecocoo.table.serialize import deserialize
   
   # The two variables capture the basic and advanced interface for the hash table:
   table_deserialized, utils_deserialized = deserialize("mydata.gecocoo")

Plotting information about the Hash Table
-----------------------------------------
After filling a hash table with data it may be of interest to extract information about the way the data has been distributed amongst the buckets.
This is of particular interest because depending on the choice of hash functions a very different distribution may occur leading to different possible fillrates for similar key sets.
To retrieve and visualize this information the utils object created on generating or loading a hash table contains the method occupancy.

This method takes a resolution as an argument and returns an OccupancyData object that contains the information for resolution equaly sized segments on how many keys were assigned there using each hash function and how many keys occupy which slot index.
Passing this object to the plot_hash_usage and plot_slot_usage functions of the plotting file will visualize the distribution as a stacked bar chart.
This can be done like this::

   from gecocoo.table.serialize import deserialize
   from gecocoo.plotting import plot_hash_usage, plot_slot_usage
   
   # Loads a table from a file:
   table, utils = deserialize("mydata.gecocoo")
   
   # Generates the occupancy data for a resolution of 200:
   occupancy_data = utils.occupancy(100)

   # Plots both charts:
   plot_hash_usage(occupancy_data)
   plot_slot_usage(occupancy_data)
