"""The module contains helper functions for working with Numba and LLVM."""

from typing import Any, Callable

import numba
from llvmlite import ir


@numba.extending.intrinsic
def cmp_xchg_64(typingctx, address, compare, swap):
    """
    Compare and swap the given values at the given address.

    Segfaults if the address is not 64 bit aligned.

    :param typingctx: the Numba typing context
    :param address: the memory address
    :param compare: the value to compare with
    :param swap: the value to save
    :return: ``True if the swapping was successful``
    """

    if (
        isinstance(address, numba.types.Integer)
        and isinstance(compare, numba.types.Integer)
        and isinstance(swap, numba.types.Integer)
    ):

        def codegen(context, builder, sig, args):
            address, compare, swap = args
            int64_p = ir.IntType(64).as_pointer()
            ptr = builder.inttoptr(address, int64_p)
            ret = builder.cmpxchg(ptr, compare, swap, "seq_cst")
            return builder.extract_value(ret, 1)

        sig = numba.boolean(numba.types.uintp, numba.types.uint64, numba.types.uint64)
        return sig, codegen


@numba.extending.intrinsic
def cmp_xchg_128(typingctx, address, compare_lo, compare_hi, swap_lo, swap_hi):
    """
    Compare and swap the given values at the given address.

    Segfaults if the address is not 128 bit aligned.

    :param typingctx: the Numba typing context
    :param address: the memory address
    :param compare_lo: the 64 lower bits to compare with
    :param compare_hi: the 64 higher bits to compare with
    :param swap_lo: the 64 lower bits to set
    :param swap_hi: the 64 higher bits to set
    :return: ``True`` if the swapping was successful
    """

    if (
        isinstance(address, numba.types.Integer)
        and isinstance(compare_lo, numba.types.Integer)
        and isinstance(compare_hi, numba.types.Integer)
        and isinstance(swap_lo, numba.types.Integer)
        and isinstance(swap_hi, numba.types.Integer)
    ):

        def codegen(context, builder, sig, args):
            address, compare_lo, compare_hi, swap_lo, swap_hi = args
            int128_t = ir.IntType(128)
            int128_p = ir.IntType(128).as_pointer()
            long_comp1 = builder.zext(compare_lo, int128_t)
            long_comp2 = builder.zext(compare_hi, int128_t)
            long_comp2 = builder.shl(long_comp2, int128_t(64))
            long_comp = builder.or_(long_comp1, long_comp2)
            long_swap1 = builder.zext(swap_lo, int128_t)
            long_swap2 = builder.zext(swap_hi, int128_t)
            long_swap2 = builder.shl(long_swap2, int128_t(64))
            long_swap = builder.or_(long_swap1, long_swap2)
            ptr = builder.inttoptr(address, int128_p)
            ret = builder.cmpxchg(ptr, long_comp, long_swap, "seq_cst")
            return builder.extract_value(ret, 1)

        sig = numba.boolean(
            numba.types.uintp,
            numba.types.uint64,
            numba.types.uint64,
            numba.types.uint64,
            numba.types.uint64,
        )
        return sig, codegen


@numba.njit
def cmp_xchg_array_64(array, index, cmp, swap):
    """
    Compare and swap a 64 bit integer at the specified ``index`` in the ``array``.

    :param array: the array
    :param index: the index
    :param cmp: the value to compare with (``array[index]``)
    :param swap: the value to set (``array[index]``)
    :return: ``True`` if successful
    """

    address = array.ctypes.data + array.itemsize * index
    return cmp_xchg_64(
        address,
        cmp,
        swap,
    )


@numba.njit
def cmp_xchg_array_128(array, index, compare_lo, compare_hi, swap_lo, swap_hi):
    """
    Compare and swap two consecutive 64 bit integers at the specified ``index`` (and implicitly ``index + 1``) in the ``array``.

    :param array: the array
    :param index: the index
    :param compare_lo: the 64 lower bits to compare with (``array[index]``)
    :param compare_hi: the 64 higher bits to compare with (``array[index + 1]``)
    :param swap_lo: the 64 lower bits to set (``array[index]``)
    :param swap_hi: the 64 higher bits to set (``array[index + 1]``)
    :return: ``True`` if successful
    """

    address = array.ctypes.data + array.itemsize * index

    # TODO: Possibility to disable this check as it might cause a slight overhead
    if address % 16 != 0:
        raise Exception("cannot swap on a non 128 bit aligned address")

    return cmp_xchg_128(address, compare_lo, compare_hi, swap_lo, swap_hi)


def make_address_of_jitclass_attr(
    class_type: numba.types.ClassType, attribute: str
) -> Callable[[Any], int]:
    """
    Generate a function that retrieves the address of an attribute in a Numba ``@jitclass``.

    :param class_type: the jit class type
    :param attribute: the attributes name
    :raises TypeError: if `class_type` is not a jit class type
    :raises ValueError: if the class has no attribute named like value of ``attribute``
    :return: the generated function
    """

    if not isinstance(class_type, numba.types.ClassType):
        raise TypeError("class_type must be an instance of ClassType")

    if not attribute in class_type.struct.keys():
        raise ValueError(f'class {class_type} has not attribute named "{attribute}"')

    attribute_index = list(class_type.struct.keys()).index(attribute)

    @numba.extending.intrinsic
    def address_of_attr(typingctx, obj):
        if hasattr(obj, "class_type") and obj.class_type is class_type:

            def codegen(context, builder: ir.builder.IRBuilder, sig, args):
                # the type of obj is a LLVM literal struct with this layout: { i8*, { ... jit class fields ... }*}
                (obj,) = args
                # first we need to extract the pointer to the data struct
                ptr_data = builder.extract_value(obj, 1)
                # next we extract calculate a pointer to the attribute in the data struct
                ptr_attribute = numba.core.cgutils.gep_inbounds(
                    builder, ptr_data, 0, attribute_index
                )
                # convert the pointer to an integer for Numba
                return builder.ptrtoint(ptr_attribute, ir.IntType(64))

            sig = numba.intp(obj)
            return sig, codegen

    return address_of_attr


@numba.extending.intrinsic
def atomic_add_64(typingctx, address, value):
    """
    Atomically add the value to the ``value`` at the given ``address``.

    :param typingctx: the Numba typing context
    :param address: the memory address
    :param value: the value to add
    :return: the value before the modification
    """

    if address is numba.types.intp and isinstance(value, numba.types.Integer):

        def codegen(context, builder: ir.builder.IRBuilder, sig, args):
            address, value = args
            ptr = builder.inttoptr(address, ir.IntType(64).as_pointer())
            ret = builder.atomic_rmw("add", ptr, value, "seq_cst")
            return ret

        sig = numba.uint64(address, numba.uint64)
        return sig, codegen


@numba.extending.intrinsic
def atomic_sub_64(typingctx, address, value):
    """
    Atomically subtract the value from the ``value`` at the given ``address``.

    :param typingctx: the Numba typing context
    :param address: the memory address
    :param value: the value to subtract
    :return: the value before the modification
    """

    if address is numba.types.intp and isinstance(value, numba.types.Integer):

        def codegen(context, builder: ir.builder.IRBuilder, sig, args):
            address, value = args
            ptr = builder.inttoptr(address, ir.IntType(64).as_pointer())
            ret = builder.atomic_rmw("sub", ptr, value, "seq_cst")
            return ret

        sig = numba.uint64(address, numba.uint64)
        return sig, codegen


@numba.extending.intrinsic
def fence(typingctx):
    """Insert a fence with ``seq_cst``-ordering in the LLVM IR."""

    def codegen(context, builder: ir.builder.IRBuilder, sig, args):
        builder.fence("seq_cst")

    return numba.types.void(), codegen
