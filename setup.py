from setuptools import find_packages, setup

import gecocoo

setup(
    name="gecocoo",
    version=gecocoo.VERSION,
    packages=find_packages(),
    install_requires=["numba", "numpy<=1.19.5"],
    license="MIT",
)
