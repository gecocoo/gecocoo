"""The module provides functions for code generating access functions for entries with bit precise fields stored in consecutive memory."""

import math
from dataclasses import dataclass
from enum import Enum
from typing import Callable, List, NamedTuple, Tuple

import numba.types as ty
from numba import njit, uint64

from gecocoo.utils.codegen import FunctionGenerator
from gecocoo.utils.llvm import cmp_xchg_array_64, cmp_xchg_array_128, fence


@dataclass(frozen=True)
class Bitfield:
    """The class defines the properties of a single field in an entry."""

    name: str
    width: int


def _make_get_entry_sig(fields: List[Bitfield]):
    return ty.UniTuple(uint64, len(fields))(uint64[:], uint64, uint64)


def _make_set_entry_sig(fields: List[Bitfield]):
    return ty.void(uint64[:], uint64, uint64, *[uint64 for _ in range(len(fields))])


def _make_cmp_swap_entry_sig(fields: List[Bitfield]):
    return ty.boolean(
        uint64[:], uint64, uint64, *[uint64 for _ in range(2 * len(fields))]
    )


class _Lock(NamedTuple):
    acquire: Callable
    release: Callable


_NOOP_LOCK = _Lock(njit(lambda a, i: None), njit(lambda a, i: None))


def _make_striped_lock(start_index: int, lock_count: int) -> _Lock:
    from numba import njit

    @njit
    def acquire(array, index):
        lock_index = start_index + index % lock_count

        while not cmp_xchg_array_64(array, lock_index, 0, 1):
            pass

    @njit
    def release(array, index):
        lock_index = start_index + index % lock_count

        while not cmp_xchg_array_64(array, lock_index, 1, 0):
            pass

    return _Lock(acquire, release)


# region PACKED_BUCKETS and PACKED_LOCKED_BUCKETS
def _make_get_entry_packed(fields: List[Bitfield], bucket_size: int, lock: _Lock):
    entry_width = sum([f.width for f in fields])
    code = FunctionGenerator("get_entry", ["array", "index", "slot"], {"lock": lock})

    code += f"first_chunk_index = (index * {bucket_size} + slot) * {entry_width} // 64"
    code += f"first_field_offset = (index * {bucket_size} + slot) * {entry_width} % 64"
    code += "lock.acquire(array, index)"

    offset = 0
    for field in fields:
        mask = 2 ** field.width - 1
        code += f"# read {field.name}"
        code += f"chunk_offset = (first_field_offset + {offset}) // 64"
        code += f"field_offset = (first_field_offset + {offset}) % 64"
        code += f"{field.name} = (array[first_chunk_index + chunk_offset] >> field_offset) & 0x{mask:x}"
        code += f"rest = field_offset + {field.width} - 64"
        with code.if_("rest > 0"):
            code += f"{field.name} |= (array[first_chunk_index + chunk_offset + 1] << ({field.width} - rest)) & 0x{mask:x}"
        code += "\n"
        offset += field.width

    code += "lock.release(array, index)"
    code += "return (" + " ".join([f"{f.name}," for f in fields]) + ")"

    return code.generate_jit(_make_get_entry_sig(fields))


def _make_set_entry_packed(
    fields: List[Bitfield],
    bucket_size: int,
    lock: _Lock,
    atomic_write: bool,
):
    entry_width = sum([f.width for f in fields])
    code = FunctionGenerator(
        "set_entry",
        ["array", "index", "slot"] + [f.name for f in fields],
        {"cmp_xchg_array_64": cmp_xchg_array_64, "fence": fence, "lock": lock},
    )

    for field in fields:
        mask = 2 ** field.width - 1
        code += f"{field.name} = {field.name} & 0x{mask:x}"

    code += f"first_chunk_index = (index * {bucket_size} + slot) * {entry_width} // 64"
    code += f"first_field_offset = (index * {bucket_size} + slot) * {entry_width} % 64"
    code += "lock.acquire(array, index)"

    offset = 0
    for field in fields:
        all_ones = 2 ** 64 - 1
        mask = all_ones ^ (2 ** field.width - 1)
        code += f"# write {field.name}"
        code += f"chunk_offset = (first_field_offset + {offset}) // 64"
        code += f"field_offset = (first_field_offset + {offset}) % 64"
        code += f"mask = 0x{mask:x} << field_offset | (2 ** field_offset - 1)"

        # FIXME: Not atomic writing caused problems. Investigate the cause and potentially replace atomic swaps with normal memory writes.
        with code.while_("True"):
            code += "chunk_old = array[first_chunk_index + chunk_offset]"
            code += f"chunk_updated = (0"
            code += f"\t| chunk_old & mask"
            code += f"\t| ({field.name} << field_offset)"
            code += ")"
            with code.if_(
                "cmp_xchg_array_64(array, first_chunk_index + chunk_offset, chunk_old, chunk_updated)"
            ):
                code += "break"

        code += f"rest = field_offset + {field.width} - 64"
        with code.while_("rest > 0"):
            code += "chunk_old = array[first_chunk_index + chunk_offset + 1]"
            code += f"mask = 0x{all_ones:x} << rest"
            code += f"chunk_updated = (0"
            code += f"\t| chunk_old & mask"
            code += f"\t| {field.name} >> ({field.width} - rest)"
            code += ")"
            with code.if_(
                "cmp_xchg_array_64(array, first_chunk_index + chunk_offset + 1, chunk_old, chunk_updated)"
            ):
                code += "break"
        code += "\n"
        offset += field.width
    code += "lock.release(array, index)"

    return code.generate_jit(_make_set_entry_sig(fields))


def _make_cmp_swap_entry_packed(
    fields: List[Bitfield], bucket_size: int, lock: _Lock, atomic_write: bool
):
    get_entry = _make_get_entry_packed(fields, bucket_size, _NOOP_LOCK)
    set_entry = _make_set_entry_packed(fields, bucket_size, _NOOP_LOCK, atomic_write)

    code = FunctionGenerator(
        "cmp_and_swap_entry",
        ["array", "index", "slot"]
        + ["cmp_" + f.name for f in fields]
        + ["swap_" + f.name for f in fields],
        {"get_entry": get_entry, "set_entry": set_entry, "lock": lock},
    )

    code += "lock.acquire(array, index)"
    code += (
        " ".join([f"{f.name}," for f in fields]) + " = get_entry(array, index, slot)"
    )
    with code.if_(
        " and ".join([f"cmp_{f.name} == {f.name}" for f in fields if f.width > 0])
    ):
        code += (
            "set_entry(array, index, slot"
            + "".join([f", swap_{f.name}" for f in fields])
            + ")"
        )
        code += f"ret = True"
    with code.else_():
        code += "ret = False"

    code += "lock.release(array, index)"
    code += "return ret"

    return code.generate_jit(_make_cmp_swap_entry_sig(fields))


# endregion PACKED_BUCKETS and PACKED_LOCKED_BUCKETS

# region ALIGNED_BUCKETS and ALIGNED_LOCKED_BUCKETS
def _make_get_entry(fields: List[Bitfield], bucket_size: int, lock: _Lock):
    entry_width = sum([f.width for f in fields])
    bucket_chunk_count = math.ceil((entry_width * bucket_size) / 64)
    chunk_count = math.ceil(entry_width / 64)
    code = FunctionGenerator("get_entry", ["array", "index", "slot"], {"lock": lock})

    code += "lock.acquire(array, index)"
    for slot in range(bucket_size):
        with code.if_(f"slot == {slot}"):
            code += f"bucket_chunk_index = index * {bucket_chunk_count} + {math.floor(slot * entry_width / 64)}"
            for i in range(chunk_count):
                shift = slot * entry_width % 64
                if shift == 0:
                    code += f"chunk_{i} = array[bucket_chunk_index + {i}]"
                else:
                    code += f"chunk_{i} = array[bucket_chunk_index + {i}] >> {shift} | array[bucket_chunk_index + {i+1}] << {64 - shift}"
    code += "lock.release(array, index)"

    offset = 0
    current_chunk = 0
    for field in fields:
        if offset + field.width <= 64:
            mask = (2 ** field.width - 1) << offset
            code += f"{field.name} = (chunk_{current_chunk} & {mask}) >> {offset}"
        else:
            lower = 64 - offset
            higher = field.width - lower
            mask = 2 ** higher - 1
            code += f"{field.name} = (chunk_{current_chunk} >> {offset}) | (chunk_{current_chunk + 1} & {mask}) << {lower}"

        offset += field.width
        if offset >= 64:
            current_chunk += 1
        offset = offset % 64

    code += "return (" + "".join([f"{f.name}, " for f in fields]) + ")"

    return code.generate_jit(_make_get_entry_sig(fields))


def _make_set_entry(
    fields: List[Bitfield],
    bucket_size: int,
    lock: _Lock,
    atomic_write: bool,
):
    entry_width = sum([f.width for f in fields])
    bucket_chunk_count = math.ceil((entry_width * bucket_size) / 64)
    code = FunctionGenerator(
        "set_entry",
        ["array", "index", "slot"] + [f.name for f in fields],
        {"swap": cmp_xchg_array_64, "lock": lock},
    )

    for field in fields:
        mask = 2 ** field.width - 1
        code += f"{field.name} = {field.name} & {mask}"

    code += "lock.acquire(array, index)"
    for slot in range(bucket_size):
        with code.if_(f"slot == {slot}"):
            code += f"bucket_chunk_index = index * {bucket_chunk_count} + {math.floor(slot * entry_width / 64)}"
            shift = slot * entry_width % 64

            chunk = 0
            fields_todo = list(fields)
            field_bits_done = 0
            while fields_todo:
                chunk_offset = 0
                code += f"array[bucket_chunk_index + {chunk}] = (0"

                mask = 0
                if shift > 0 and chunk == 0:
                    chunk_offset += shift
                    mask |= 2 ** shift - 1

                while fields_todo and chunk_offset < 64:
                    field = fields_todo[0]
                    field_bits = min(64 - chunk_offset, field.width - field_bits_done)
                    if field_bits_done > 0:
                        code += f"\t| {field.name} >> {field_bits_done}"
                    else:
                        code += f"\t| {field.name} << {chunk_offset}"
                    chunk_offset += field_bits
                    field_bits_done += field_bits

                    if field_bits_done == field.width:
                        field_bits_done = 0
                        fields_todo.pop(0)

                if chunk_offset < 64:
                    mask |= (2 ** (64 - chunk_offset) - 1) << chunk_offset

                if (chunk == 0 or not fields_todo) and mask != 0:
                    code += f"\t| (array[bucket_chunk_index + {chunk}] & 0x{mask:x})"

                code += ")"
                chunk += 1
    code += "lock.release(array, index)"

    return code.generate_jit(_make_set_entry_sig(fields))


def _make_cmp_swap_entry(
    fields: List[Bitfield],
    bucket_size: int,
    lock: _Lock,
    atomic_write: bool,
):
    get_entry = _make_get_entry(fields, bucket_size, _NOOP_LOCK)
    set_entry = _make_set_entry(fields, bucket_size, _NOOP_LOCK, atomic_write)

    code = FunctionGenerator(
        "cmp_and_swap_entry",
        ["array", "index", "slot"]
        + ["cmp_" + f.name for f in fields]
        + ["swap_" + f.name for f in fields],
        {"get_entry": get_entry, "set_entry": set_entry, "lock": lock},
    )

    code += "lock.acquire(array, index)"
    code += (
        " ".join([f"{f.name}," for f in fields]) + " = get_entry(array, index, slot)"
    )
    with code.if_(
        " and ".join([f"cmp_{f.name} == {f.name}" for f in fields if f.width > 0])
    ):
        code += (
            "set_entry(array, index, slot"
            + "".join([f", swap_{f.name}" for f in fields])
            + ")"
        )
        code += f"ret = True"
    with code.else_():
        code += "ret = False"

    code += "lock.release(array, index)"
    code += "return ret"

    return code.generate_jit(_make_cmp_swap_entry_sig(fields))


# endregion ALIGNED_BUCKETS and ALIGNED_LOCKED_BUCKETS

# region ATOMIC_SLOTS (64 bit)
def _make_get_entry_64(fields: List[Bitfield], bucket_size: int):
    entry_width = sum([f.width for f in fields])
    if entry_width > 64:
        raise ValueError(
            f"entry width must not be more than 64 bits, but is {entry_width}"
        )
    entries_per_chunk = 64 // entry_width
    bucket_chunk_count = math.ceil(bucket_size / entries_per_chunk)
    code = FunctionGenerator("get_entry", ["array", "index", "slot"])

    code += f"chunk_index = index * {bucket_chunk_count} + slot // {entries_per_chunk}"
    code += f"chunk_slot = slot % {entries_per_chunk}"
    code += f"chunk = array[chunk_index]"
    for i in range(entries_per_chunk):
        shift = i * entry_width
        with code.if_(f"chunk_slot == {i}"):
            offset = shift
            for field in fields:
                code += (
                    f"{field.name} = (chunk >> {offset}) & 0x{2 ** field.width - 1:x}"
                )
                offset += field.width

    code += "return (" + "".join([f"{f.name}, " for f in fields]) + ")"

    return code.generate_jit(_make_get_entry_sig(fields))


def _make_set_entry_64(fields: List[Bitfield], bucket_size: int):
    entry_width = sum([f.width for f in fields])
    if entry_width > 64:
        raise ValueError(
            f"entry width must not be more than 64 bits, but is {entry_width}"
        )
    entries_per_chunk = 64 // entry_width
    bucket_chunk_count = math.ceil(bucket_size / entries_per_chunk)

    code = FunctionGenerator(
        "set_entry",
        ["array", "index", "slot"] + [f.name for f in fields],
        {"swap": cmp_xchg_array_64},
    )

    for field in fields:
        mask = 2 ** field.width - 1
        code += f"{field.name} = {field.name} & {mask}"

    code += f"chunk_index = index * {bucket_chunk_count} + slot // {entries_per_chunk}"
    code += f"chunk_slot = slot % {entries_per_chunk}"
    with code.while_("True"):
        for i in range(entries_per_chunk):
            shift = i * entry_width
            with code.if_(f"chunk_slot == {i}"):
                code += "cmp_chunk = array[chunk_index]"
                code += "swap_chunk = (0"
                offset = 0
                for field in fields:
                    code += f"\t| {field.name} << {shift + offset}"
                    offset += field.width

                mask = 2 ** shift - 1 | (2 ** (64 - shift - entry_width) - 1) << (
                    shift + entry_width
                )
                code += f"\t| cmp_chunk & 0x{mask:x}\n"
                code += ")"

        with code.if_("swap(array, chunk_index, cmp_chunk, swap_chunk)"):
            code += "break"

    return code.generate_jit(_make_set_entry_sig(fields))


def _make_cmp_swap_entry_64(fields: List[Bitfield], bucket_size: int):
    entry_width = sum([f.width for f in fields])
    if entry_width > 64:
        raise ValueError(
            f"entry width must not be more than 64 bits, but is {entry_width}"
        )
    entries_per_chunk = 64 // entry_width
    bucket_chunk_count = math.ceil(bucket_size / entries_per_chunk)

    code = FunctionGenerator(
        "set_cmp_and_swap_entry",
        ["array", "index", "slot"]
        + ["cmp_" + f.name for f in fields]
        + ["swap_" + f.name for f in fields],
        {"swap": cmp_xchg_array_64},
    )

    for field in fields:
        mask = 2 ** field.width - 1
        code += f"cmp_{field.name} = cmp_{field.name} & {mask}"
        code += f"swap_{field.name} = swap_{field.name} & {mask}"

    code += f"chunk_index = index * {bucket_chunk_count} + slot // {entries_per_chunk}"
    code += f"chunk_slot = slot % {entries_per_chunk}"
    for i in range(entries_per_chunk):
        shift = i * entry_width
        with code.if_(f"chunk_slot == {i}"):

            code_cmp = ""
            code_swap = ""

            offset = 0
            for field in fields:
                code_cmp += f"\t| cmp_{field.name} << {shift + offset}\n"
                code_swap += f"\t| swap_{field.name} << {shift + offset}\n"
                offset += field.width

            mask = 2 ** shift - 1 | (2 ** (64 - shift - entry_width) - 1) << (
                shift + entry_width
            )
            code_cmp += f"\t| array[chunk_index] & 0x{mask:x}\n"
            code_swap += f"\t| array[chunk_index] & 0x{mask:x}\n"

            code += "cmp_chunk = (0"
            code += code_cmp
            code += ")"

            code += "swap_chunk = (0"
            code += code_swap
            code += ")"

    code += "return swap(array, chunk_index, cmp_chunk, swap_chunk)"

    return code.generate_jit(_make_cmp_swap_entry_sig(fields))


# endregion ATOMIC_SLOTS (64 bit)

# region ATOMIC_SLOTS (128 bit)
def _make_get_entry_128(fields: List[Bitfield], bucket_size: int):
    entry_width = sum([f.width for f in fields])
    assert entry_width <= 128
    entries_per_chunk = 128 // entry_width
    bucket_chunk_count = math.ceil(bucket_size / entries_per_chunk)
    code = FunctionGenerator("get_entry", ["array", "index", "slot"])

    code += f"chunk_index = index * {bucket_chunk_count * 2} + (slot // {entries_per_chunk} * 2)"
    code += f"chunk_slot = slot % {entries_per_chunk}"
    code += f"chunk_lo = array[chunk_index]"
    code += f"chunk_hi = array[chunk_index + 1]"
    for i in range(entries_per_chunk):
        shift = i * entry_width
        with code.if_(f"chunk_slot == {i}"):
            offset = shift
            for field in fields:
                if offset < 64:
                    if offset + field.width > 64:
                        mask = 2 ** (field.width - 64 + offset) - 1
                        code += f"{field.name} = (chunk_lo >> {offset}) | ((chunk_hi & 0x{mask:x}) << {64 - offset} )"
                    else:
                        mask = 2 ** field.width - 1
                        code += f"{field.name} = (chunk_lo >> {offset}) & 0x{mask:x}"
                else:
                    mask = 2 ** field.width - 1
                    code += f"{field.name} = (chunk_hi >> {offset % 64}) & 0x{mask:x}"
                offset += field.width

    code += "return (" + "".join([f"{f.name}, " for f in fields]) + ")"

    return code.generate_jit(_make_get_entry_sig(fields))


def _make_set_entry_128(fields: List[Bitfield], bucket_size: int):
    entry_width = sum([f.width for f in fields])
    assert entry_width <= 128
    entries_per_chunk = 128 // entry_width
    bucket_chunk_count = math.ceil(bucket_size / entries_per_chunk)

    code = FunctionGenerator(
        "set_entry",
        ["array", "index", "slot"] + [f.name for f in fields],
        {"swap": cmp_xchg_array_128},
    )

    for field in fields:
        mask = 2 ** field.width - 1
        code += f"{field.name} = {field.name} & {mask}"

    code += f"chunk_index = index * {bucket_chunk_count * 2} + (slot // {entries_per_chunk} * 2)"
    code += f"chunk_slot = slot % {entries_per_chunk}"
    with code.while_("True"):
        for i in range(entries_per_chunk):
            slot_start = i * entry_width
            with code.if_(f"chunk_slot == {i}"):
                code += "cmp_chunk_lo = array[chunk_index]"
                code += "cmp_chunk_hi = array[chunk_index + 1]"

                code += "swap_chunk_lo = (0"
                offset = slot_start
                for field in fields:
                    if offset < 64:
                        code += f"\t| {field.name} << {offset}"
                    offset += field.width

                mask = (2 ** min(64, slot_start) - 1) | (
                    2 ** max(0, 64 - slot_start - entry_width) - 1
                ) << (slot_start + entry_width)
                code += f"\t| cmp_chunk_lo & 0x{mask:x}\n"
                code += ")"

                code += "swap_chunk_hi = (0"
                offset = slot_start
                for field in fields:
                    if offset < 64 and offset + field.width > 64:
                        code += f"\t| {field.name} >> {64 - offset}"
                    elif offset >= 64:
                        code += f"\t| {field.name} << {offset - 64}"
                    offset += field.width

                mask = (2 ** max(0, slot_start - 64) - 1) | (
                    2 ** min(64, 128 - slot_start - entry_width) - 1
                ) << max(0, slot_start + entry_width - 64)
                code += f"\t| cmp_chunk_hi & 0x{mask:x}\n"
                code += ")"

        with code.if_(
            "swap(array, chunk_index, cmp_chunk_lo, cmp_chunk_hi, swap_chunk_lo, swap_chunk_hi)"
        ):
            code += "break"

    return code.generate_jit(_make_set_entry_sig(fields))


def _make_cmp_swap_entry_128(fields: List[Bitfield], bucket_size: int):
    entry_width = sum([f.width for f in fields])
    assert entry_width <= 128
    entries_per_chunk = 128 // entry_width
    bucket_chunk_count = math.ceil(bucket_size / entries_per_chunk)

    code = FunctionGenerator(
        "cmp_and_swap_entry",
        ["array", "index", "slot"]
        + ["cmp_" + f.name for f in fields]
        + ["swap_" + f.name for f in fields],
        {"swap": cmp_xchg_array_128},
    )

    for field in fields:
        mask = 2 ** field.width - 1
        code += f"cmp_{field.name} = cmp_{field.name} & {mask}"
        code += f"swap_{field.name} = swap_{field.name} & {mask}"

    code += f"chunk_index = index * {bucket_chunk_count * 2} + (slot // {entries_per_chunk} * 2)"
    code += f"chunk_slot = slot % {entries_per_chunk}"
    code += "current_lo = array[chunk_index]"
    code += "current_hi = array[chunk_index + 1]"
    for i in range(entries_per_chunk):
        slot_start = i * entry_width
        with code.if_(f"chunk_slot == {i}"):
            code_cmp_lo = ""
            code_cmp_hi = ""
            code_swap_lo = ""
            code_swap_hi = ""

            offset = slot_start
            for field in fields:
                if offset < 64:
                    code_cmp_lo += f"\t| cmp_{field.name} << {offset}\n"
                    code_swap_lo += f"\t| swap_{field.name} << {offset}\n"

                    if offset + field.width > 64:
                        code_cmp_hi += f"\t| cmp_{field.name} >> {64 - offset}\n"
                        code_swap_hi += f"\t| swap_{field.name} >> {64 - offset}\n"
                else:
                    code_cmp_hi += f"\t| cmp_{field.name} << {offset - 64}\n"
                    code_swap_hi += f"\t| swap_{field.name} << {offset - 64}\n"
                offset += field.width

            mask = (2 ** min(64, slot_start) - 1) | (
                2 ** max(0, 64 - slot_start - entry_width) - 1
            ) << (slot_start + entry_width)
            keep_lo = f"\t| current_lo & 0x{mask:x}"

            mask = (2 ** max(0, slot_start - 64) - 1) | (
                2 ** min(64, 128 - slot_start - entry_width) - 1
            ) << max(0, slot_start + entry_width - 64)
            keep_hi = f"\t| current_hi & 0x{mask:x}"

            code += "cmp_chunk_lo = (0"
            code += code_cmp_lo
            code += keep_lo
            code += ")"
            code += "cmp_chunk_hi = (0"
            code += code_cmp_hi
            code += keep_hi
            code += ")"
            code += "swap_chunk_lo = (0"
            code += code_swap_lo
            code += keep_lo
            code += ")"
            code += "swap_chunk_hi = (0"
            code += code_swap_hi
            code += keep_hi
            code += ")"

    code += "return swap(array, chunk_index, cmp_chunk_lo, cmp_chunk_hi, swap_chunk_lo, swap_chunk_hi)"
    return code.generate_jit(_make_cmp_swap_entry_sig(fields))


# endregion ATOMIC_SLOTS (128 bit)


class Mode(Enum):
    """Modes supported by the bit fields array."""

    PACKED_BUCKETS = 0
    """Pack the buckets together as close as possible leaving no bits for padding and alignment."""

    PACKED_LOCKED_BUCKETS = 1
    """
    Pack the buckets together as close as possible leaving no bits for padding and alignment.
    In addition to :attr:`~Mode.PACKED_BUCKETS` this adds a lock bit to each bucket, enabling concurrency safe writes.
    """

    ALIGNED_BUCKETS = 2
    """Align the buckets to be on 64 bit memory addresses, leaving some bits unused in the process, but having better accessing performance."""

    ALIGNED_LOCKED_BUCKETS = 3
    """
    Align the buckets to be on 64 bit memory addresses, leaving some bits unused in the process, but having better accessing performance.
    In addition to :attr:`~Mode.ALIGNED_BUCKETS` this adds a lock bit to each bucket, enabling concurrency safe writes.
    """

    ATOMIC_SLOTS = 4
    """
    Align slots in the buckets in a way that it is possible to replace them atomically.
    This requires the entry size to be less than 128 bits, as they cannot otherwise be swapped atomically.
    """


class BitfieldsArray(NamedTuple):
    """The class bundles the generated access functions for a bit fields array."""

    size: int
    get_entry: Callable[[int, int], Tuple[int, ...]]
    set_entry: Callable[..., None]
    cmp_swap_entry: Callable[..., bool]


def make_bitfields_array(
    bucket_count: int,
    fields: List[Bitfield],
    bucket_size: int,
    mode: Mode = Mode.ALIGNED_BUCKETS,
) -> BitfieldsArray:
    """
    Generate the access functions for a bit fields array with the specified properties.

    :param bucket_count: the number of buckets
    :param fields: the fields of each entry
    :param bucket_size: the number of slots per bucket
    :param mode: the mode to use, defaults to :attr:`Mode.ALIGNED_BUCKETS`
    :raises ValueError: if an invalid parameter combination was provided
    :return: the generated access function as an object of :class:`BitfieldsArray`
    """

    entry_width = sum([f.width for f in fields])

    if mode == Mode.ATOMIC_SLOTS:
        if entry_width > 128:
            raise ValueError(
                f"entry width must not be more than 128 bits, but is {entry_width}"
            )

        # Pack as many slots as possible in each 64 bit or 128 bit chunk.
        # We chose the method that is more memory efficient.
        if entry_width <= 64 and 64 % entry_width <= 128 % entry_width:
            entries_per_chunk = 64 // entry_width
            bucket_chunk_count = math.ceil(bucket_size / entries_per_chunk)

            get_entry = _make_get_entry_64(fields, bucket_size)
            set_entry = _make_set_entry_64(fields, bucket_size)
            cmp_swap_entry = _make_cmp_swap_entry_64(fields, bucket_size)
        else:
            entries_per_chunk = 128 // entry_width
            bucket_chunk_count = math.ceil(bucket_size / entries_per_chunk) * 2

            get_entry = _make_get_entry_128(fields, bucket_size)
            set_entry = _make_set_entry_128(fields, bucket_size)
            cmp_swap_entry = _make_cmp_swap_entry_128(fields, bucket_size)

        size = int(bucket_count * bucket_chunk_count)

    elif mode == Mode.ALIGNED_BUCKETS or mode == Mode.ALIGNED_LOCKED_BUCKETS:
        locking = mode == Mode.ALIGNED_LOCKED_BUCKETS
        bucket_chunk_count = math.ceil(entry_width * bucket_size / 64)
        size = int(bucket_count * bucket_chunk_count)
        if locking:
            lock = _make_striped_lock(size + 10, 1000)
            size += 1010
        else:
            lock = _NOOP_LOCK

        get_entry = _make_get_entry(fields, bucket_size, lock)
        set_entry = _make_set_entry(fields, bucket_size, lock, locking)
        cmp_swap_entry = _make_cmp_swap_entry(fields, bucket_size, lock, locking)

    elif mode == Mode.PACKED_BUCKETS or mode == Mode.PACKED_LOCKED_BUCKETS:
        locking = mode == Mode.PACKED_LOCKED_BUCKETS
        size = math.ceil((bucket_count * bucket_size * entry_width) / 64) + 100
        if locking:
            lock = _make_striped_lock(size + 10, 1000)
            size += 1010
        else:
            lock = _NOOP_LOCK

        get_entry = _make_get_entry_packed(fields, bucket_size, lock)
        set_entry = _make_set_entry_packed(fields, bucket_size, lock, locking)
        cmp_swap_entry = _make_cmp_swap_entry_packed(fields, bucket_size, lock, locking)

    else:
        raise NotImplementedError(f"there is no implementation for mode {mode}")

    return BitfieldsArray(
        size=size,
        get_entry=get_entry,
        set_entry=set_entry,
        cmp_swap_entry=cmp_swap_entry,
    )
