"""The module provides the :func:`swaplinear` hash function."""

from math import floor, log2
from typing import List

import numpy as np
from numba import njit, uint64
from numba.types import UniTuple

from .utils import inverse_mult


def swaplinear(num_buckets: int, k: int, seed: int, parameters: List[int]):
    """
    Generate a reversible hash function for given parameters.

    The hash function works by first swapping the upper and lower half of the bits of the key,
    then it multiplying by a specific value.
    See: https://epubs.siam.org/doi/pdf/10.1137/1.9781611976007.15

    :param num_buckets: the number of buckets to address
    :param k: specifies the key space, where ``k`` means k-mers of size ``k``
    :param seed: a seed for a random hash function
    :param parameters: optional parameters for the hash function (currently unused)
    :return:
        a tuple consisting of

        - the forward function for hashing
        - the backward function for restoring a key
        - the number of bits the hash function requires for restoring keys
    """

    rng = np.random.RandomState(int(seed) % 32 ** 2)
    hash_seed = 0
    while hash_seed % 2 == 0:
        hash_seed = rng.randint(1, 2 ** 64 - 1, dtype=np.uint64)
    mask_2k_bits = uint64(2 ** (2 * k) - 1)
    inverse_a = uint64(inverse_mult(int(hash_seed), 4 ** k))
    keybits = max(0, 2 * k - floor(log2(num_buckets)))

    # Enforce types for numba
    k = uint64(k)
    num_buckets = uint64(num_buckets)
    hash_seed = uint64(hash_seed)

    @njit(
        UniTuple(uint64, 2)(uint64),
        locals=dict(
            rot=uint64,
            g=uint64,
            hash_index=uint64,
            quotient=uint64,
        ),
    )
    def forward(key: uint64):
        rot = (key << k ^ key >> k) & mask_2k_bits
        g = (hash_seed * rot) & mask_2k_bits
        hash_index = g % num_buckets
        quotient = g // num_buckets
        return hash_index, quotient

    @njit(
        uint64(uint64, uint64),
        locals=dict(
            g=uint64,
            rot=uint64,
            key=uint64,
        ),
    )
    def backward(hash_index: uint64, quotient: uint64):
        g = quotient * num_buckets + hash_index
        rot = (inverse_a * g) & mask_2k_bits
        key = (rot << k | rot >> k) & mask_2k_bits
        return key

    return forward, backward, keybits
