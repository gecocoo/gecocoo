# GeCoCoo Documentation

The latest build of the documentation from the `develop` branch can be found here: https://gecocoo.gitlab.io/gecocoo/

## Building

Use `./scripts.py docs-build` to build the documentation.
The output will land in `/docs/build/`.

If you are working on the documentation and want to have a fast feedback loop, consider using `./scripts.py docs-watch`.
This will start a small webserver locally that serves the documentation.
If you change project files the documentation will be rebuild and your browser page refreshed.
(Warning: In some cases this can result in slightly different different output that `./scripts.py docs-build`. But in most cases there should be no difference)
