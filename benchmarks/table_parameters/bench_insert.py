import benchutils
import pytest
from numba import njit

from gecocoo.config import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import generate_random_kmers, get_qualified_name
from gecocoo.valuemodules import keyset

ROUNDS = benchutils.apply_rounds_factor(10)
WARMUP_ROUNDS = benchutils.apply_rounds_factor(2, min=0, max=5)
SEED = 1234
KEY_COUNT = benchutils.apply_data_factor(10 ** 6, 10000)
BUCKET_SIZES = [4, 8, 16]
k = 31
STORAGE_MODES = [
    StorageMode.BUCKETS,
    StorageMode.PACKED,
    StorageMode.FAST,
]
NUM_THREADS = [1, 8, 16]
LOAD_FACTORS = [0.75, 0.95, 0.97]
NUM_HASH_FUNCTIONS = [2, 3]
NUM_HASH_FUNCTIONS_COST_OPTIMAL = [2, 3, 4]


@pytest.mark.parametrize("bucket_size", BUCKET_SIZES)
@pytest.mark.parametrize("load_factor", LOAD_FACTORS)
@pytest.mark.parametrize("thread_count", NUM_THREADS)
@pytest.mark.parametrize("storage_mode", STORAGE_MODES)
@pytest.mark.parametrize("num_hash_functions", NUM_HASH_FUNCTIONS)
def bench_insert_random_walk(
    bucket_size, load_factor, thread_count, storage_mode, num_hash_functions, benchmark
):
    benchmark.group = "table parameters insert random walk"
    benchmark.name = (
        f"insert_random_walk ("
        + f"bucket_size: {bucket_size}, "
        + f"load_factor: {load_factor}, "
        + f"threads: {thread_count}, "
        + f"storage_mode: {storage_mode}, "
        + f"num_hash_functions: {num_hash_functions}"
        + ")"
    )

    hash_functions = [
        HashFunction(get_qualified_name(xorlinear))
        for _ in range(0, num_hash_functions)
    ]

    config = Config(
        load_factor=load_factor,
        k=k,
        bucket_size=bucket_size,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=hash_functions,
        storage=StorageConfig(
            mode=storage_mode,
            concurrent=thread_count > 1,
            disable_quotienting=(storage_mode == StorageMode.FAST),
        ),
    )

    keys = generate_random_kmers(31, KEY_COUNT, SEED, None)
    (table, _) = generate_hashtable(config, KEY_COUNT)

    if thread_count == 1:

        def setup():
            table.clear(table.storage)

        @njit
        def work(table):
            for key in keys:
                table.insert_single(table.storage, key)

    else:
        from threading import Thread

        chunk_size = KEY_COUNT // thread_count

        def setup():
            table.clear(table.storage)

        def work(table):
            threads = list(
                [
                    Thread(target=insert_chunk, args=(table, chunk))
                    for chunk in range(thread_count)
                ]
            )
            for t in threads:
                t.start()
            for t in threads:
                t.join()

        @njit(nogil=True)
        def insert_chunk(table, chunk):
            for i in range(chunk * chunk_size, chunk * chunk_size + chunk_size):
                k = keys[i]
                table.insert_single(table.storage, k)

    benchmark.pedantic(
        work, args=(table,), setup=setup, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS
    )


@pytest.mark.parametrize("bucket_size", BUCKET_SIZES)
@pytest.mark.parametrize("load_factor", LOAD_FACTORS)
@pytest.mark.parametrize("storage_mode", STORAGE_MODES)
@pytest.mark.parametrize("num_hash_functions", NUM_HASH_FUNCTIONS_COST_OPTIMAL)
def bench_insert_cost_optimal(
    bucket_size, load_factor, storage_mode, num_hash_functions, benchmark
):
    benchmark.group = "table parameters insert cost optimal"
    benchmark.name = (
        f"insert_cost_optimal ("
        + f"bucket_size: {bucket_size}, "
        + f"load_factor: {load_factor}, "
        + f"storage_mode: {storage_mode}, "
        + f"num_hash_functions: {num_hash_functions}"
        + ")\n"
    )

    hash_functions = [
        HashFunction(get_qualified_name(xorlinear))
        for _ in range(0, num_hash_functions)
    ]

    config = Config(
        load_factor=load_factor,
        k=k,
        bucket_size=bucket_size,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=hash_functions,
        storage=StorageConfig(
            mode=storage_mode,
            concurrent=False,
            disable_quotienting=(storage_mode == StorageMode.FAST),
        ),
    )

    keys = generate_random_kmers(31, KEY_COUNT, SEED, None)
    (table, _) = generate_hashtable(config, KEY_COUNT)

    def setup():
        table.clear(table.storage)

    @njit
    def work(table, keys):
        table.initialize(table.storage, keys)

    benchmark.pedantic(
        work,
        args=(table, keys),
        setup=setup,
        rounds=ROUNDS,
        warmup_rounds=WARMUP_ROUNDS,
    )
