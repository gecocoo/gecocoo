"""The module defines the base class for value modules can implement."""

from abc import ABC, abstractmethod


class ValueModuleBase(ABC):
    """Abstract base class for defining value modules in classes."""

    @abstractmethod
    def make_first_insert(self):
        """Generate the function that will be called when a key is inserted for the first time."""

    @abstractmethod
    def make_update_value(self):
        """Generate the function that will be called when a key is inserted again."""

    @abstractmethod
    def make_map_value(self):
        """Generate the function that will be used to map a stored value for the result of searching of a key."""

    @property
    @abstractmethod
    def value_bits(self) -> int:
        """Return the number of bits used for storing values."""
