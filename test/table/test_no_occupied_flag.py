import numpy as np

from gecocoo.config import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.generator import HashtableInterface, generate_hashtable
from gecocoo.hashing import swaplinear, xorlinear
from gecocoo.helper import generate_random_kmers, get_qualified_name
from gecocoo.valuemodules import counting

SIZE = 16
SIZE_BIG = 10 ** 5

config_47 = Config(
    # Make sure it takes just 64 bit without the flag
    # 1b relocation, 1b hash_index, 32b quotient, 30b value
    # 4 per bucket, 8+1 buckets, 36 Fields
    # 1 field reserved for counter
    # 10 fields for as per utils.bitfieldsarray line 590
    load_factor=0.5,
    k=16,
    bucket_size=4,
    value_module=ValueModuleConfig(get_qualified_name(counting), [30]),
    hash_functions=[
        HashFunction(name=get_qualified_name(swaplinear)),
        HashFunction(name=get_qualified_name(xorlinear)),
    ],
    storage=StorageConfig(
        mode=StorageMode.BUCKETS,
        concurrent=True,
        disable_occupied_flag=True,
        disable_quotienting=False,
    ),
)


def test_array_size_expected():
    (table, _) = generate_hashtable(config_47, SIZE)

    config_bigger = Config(
        # Make sure it takes just 64 bit without the flag
        # 1b relocation, 1b hash_index, 32b quotient, 30b value
        # 4 per bucket, 8+1 buckets, 36 Fields
        # 1 field reserved for counter
        # 10 fields for as per utils.bitfieldsarray line 590
        load_factor=0.5,
        k=16,
        bucket_size=4,
        value_module=ValueModuleConfig(get_qualified_name(counting), [30]),
        hash_functions=[
            HashFunction(name=get_qualified_name(swaplinear)),
            HashFunction(name=get_qualified_name(xorlinear)),
        ],
        storage=StorageConfig(
            mode=StorageMode.BUCKETS,
            concurrent=True,
            disable_occupied_flag=False,
            disable_quotienting=True,
        ),
    )
    (table_bigger, _) = generate_hashtable(config_bigger, SIZE)

    # Check other config is larger
    assert table.storage.shape[0] < table_bigger.storage.shape[0]


def test_simple_ins_ser_rem():
    (table, _) = generate_hashtable(config_47, SIZE)

    # Check that no zeros are interpreted as entries
    assert table.search_single(table.storage, 0) == None
    assert np.all(table.storage == 0)
    # Check Reads are found
    table.insert_single(table.storage, 0)
    # need to check if bucket 17 contains anything
    # assert np.any(table.storage[8*4:9*4]>0)
    # check no other bucket contains anything
    # assert np.all(table.storage[0*4:8*4]==0)
    # does the reversing of the hash work?
    assert table.search_single(table.storage, 0) == 1
    table.remove_single(table.storage, 0)
    # check if removal successfull
    assert table.search_single(table.storage, 0) == None
    assert np.all(table.storage == 0)

    # swaplinear puts all the low keys to the same buckets, so this will cause relocation
    table.insert_single(table.storage, 0)
    table.insert_single(table.storage, 1)
    table.insert_single(table.storage, 2)
    table.insert_single(table.storage, 3)
    table.insert_single(table.storage, 4)
    assert table.search_single(table.storage, 0) == 1
    assert table.search_single(table.storage, 1) == 1
    assert table.search_single(table.storage, 2) == 1
    assert table.search_single(table.storage, 3) == 1
    assert table.search_single(table.storage, 4) == 1


config_big = Config(
    load_factor=0.95,
    k=31,
    bucket_size=4,
    value_module=ValueModuleConfig(get_qualified_name(counting), [64]),
    hash_functions=[
        HashFunction(name=get_qualified_name(swaplinear)),
        HashFunction(name=get_qualified_name(xorlinear)),
    ],
    storage=StorageConfig(
        mode=StorageMode.BUCKETS,
        concurrent=True,
        disable_occupied_flag=True,
        disable_quotienting=False,
    ),
)


def test_high_fillrates():
    # This test could hang if it turns out too many keys are mapped to 0.
    # This will happen with disable_quotienting because of lacking alternative positions.
    keys = generate_random_kmers(31, SIZE_BIG, 123)
    (table, _) = generate_hashtable(config_big, SIZE_BIG)

    table.clear(table.storage)

    for k in keys:
        table.insert_single(table.storage, k)

    for k in keys:
        assert table.search_single(table.storage, k) > 0
