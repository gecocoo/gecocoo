# GeCoCoo

An efficient and configurable hash table for genome processing using cuckoo hashing.

## Development

The repository contains some helper scripts for development.

To setup a new development environment call `./scripts.py setup-dev` and
then activate the virtual environment using `source .venv/bin/activate`.
You might also want to install the precommit hook for checking the formatting
of code files before commiting using `pre-commit install`.

The following scripts are available. Each of them is callable using `./scripts.py <name> [<args...>]`.

| name             | description                                                        |
| ---------------- | ------------------------------------------------------------------ |
| check-docstrings | Check for missing or wrong docstrings.                             |
| check-formatting | Check formatting (using black).                                    |
| check-imports    | Check imports order (using isort).                                 |
| docs-build       | Build the documentation.                                           |
| docs-watch       | Watch for changes and build documentation.                         |
| format           | Format all python code (using black and isort)                     |
| help             | Show this summary of the available scripts.                        |
| setup-dev        | Setup the development environment.                                 |
| test-ci-job      | Run a GitLab runner for the specific job locally (requires docker) |
| test-examples    | Test the scripts in the examples directory.                        |
| test-tests       | Run all tests in "test" directory with coverage reporting          |

## Benchmarking

See https://gitlab.com/gecocoo/gecocoo/-/blob/develop/benchmarks/README.md

## Documentation

The source files for the documentation are contained in the `/docs` directory.
The latest built version for the development branch is available at https://gecocoo.gitlab.io/gecocoo/.

To build the documentation locally use `./scripts.py docs-build`.
If you want to watch for changes to the documentation and it to be automatically be rebuild use `./scripts.py docs-watch`.