GeCoCoo
=======

GeCoCoo is an efficient and configurable hash table for genome processing using cuckoo hashing.

.. toctree::
   :maxdepth: 2
   :caption: Getting Started:
   :hidden:

   getting_started/installation
   getting_started/usage
   getting_started/interface

.. toctree::
   :maxdepth: 2
   :caption: Configuration:
   :hidden:

   configuration/options
   configuration/yaml

.. toctree::
   :maxdepth: 2
   :caption: Utilities:
   :hidden:

   utilities/serialization
   utilities/plotting

.. toctree::
   :maxdepth: 2
   :caption: Extending:
   :hidden:

   extending/valuemodules
   extending/hashfunctions

.. toctree::
   :maxdepth: 2
   :caption: API Reference:
   :hidden:

   api/index

.. toctree::
   :maxdepth: 2
   :caption: Project Information:
   :hidden:

   project/credits
   project/license
