import numpy as np
import pytest

from gecocoo.config import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.valuemodules import keyset

SEED = 1234
KEY_COUNT = 10 ** 6
LOAD_TEST_ITERATIONS = 100
LOAD_TEST_RECYCLE_COUNT = 10


def get_config(storage):
    return Config(
        load_factor=0.95,
        k=16,
        bucket_size=4,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
        storage=storage,
    )


@pytest.mark.parametrize(
    "storage",
    [
        pytest.param(StorageConfig(mode=StorageMode.PACKED, concurrent=False)),
        pytest.param(StorageConfig(mode=StorageMode.PACKED, concurrent=True)),
        pytest.param(StorageConfig(mode=StorageMode.BUCKETS, concurrent=False)),
        pytest.param(StorageConfig(mode=StorageMode.BUCKETS, concurrent=True)),
        pytest.param(
            StorageConfig(
                mode=StorageMode.FAST, concurrent=True, disable_quotienting=True
            )
        ),
        pytest.param(
            StorageConfig(
                mode=StorageMode.FAST, concurrent=False, disable_quotienting=True
            )
        ),
    ],
    ids=str,
)
def test_table_operations(storage: StorageConfig):
    config = get_config(storage)

    rng = np.random.RandomState(SEED)
    keys = rng.randint(0, 2 ** config.k - 1, KEY_COUNT, dtype=np.uint64)
    (table, _) = generate_hashtable(config, int(KEY_COUNT * 1.5))

    # insert some initial keys
    for k in keys:
        table.insert_single(table.storage, k)
        result = table.search_single(table.storage, k)
        assert result == True, f"expected key {k} to be in the table"

    # check for inserted keys
    for k in keys:
        result = table.search_single(table.storage, k)
        assert result == True, f"expected key {k} to be in the table"

    remove_keys = set(np.random.choice(keys, KEY_COUNT // 4))
    left_keys = set(keys) - remove_keys

    # remove a set of keys
    for k in remove_keys:
        result = table.remove_single(table.storage, k)
        assert result == True, f"expected key {k} to be successfully removed"

    # searching for removed keys returns None
    for k in remove_keys:
        result = table.search_single(table.storage, k)
        assert result is None, f"expected key {k} to not be in the table"

    # verify iterator yields the expected results
    key_value_pairs = list(table.key_value_iterator(table.storage))
    assert len(key_value_pairs) == len(left_keys)
    for k, v in key_value_pairs:
        assert k in left_keys
        assert v == True

    # clear table and check it is empty
    table.clear(table.storage)
    assert len(list(table.key_value_iterator(table.storage))) == 0


# TODO: Uncomment when fixed
@pytest.mark.skip
@pytest.mark.parametrize(
    "storage",
    [
        pytest.param(StorageConfig(mode=StorageMode.PACKED, concurrent=False)),
        pytest.param(StorageConfig(mode=StorageMode.PACKED, concurrent=True)),
        pytest.param(StorageConfig(mode=StorageMode.BUCKETS, concurrent=False)),
        pytest.param(StorageConfig(mode=StorageMode.BUCKETS, concurrent=True)),
        pytest.param(
            StorageConfig(
                mode=StorageMode.FAST, concurrent=True, disable_quotienting=True
            )
        ),
        pytest.param(
            StorageConfig(
                mode=StorageMode.FAST, concurrent=False, disable_quotienting=True
            )
        ),
    ],
    ids=str,
)
@pytest.mark.parametrize("insert_operation", ["random_walk", "cost_optimal"])
def test_on_load(storage: StorageConfig, insert_operation: str):
    """A load test for insertion operations with a well-filled table."""
    config = get_config(storage)

    rng = np.random.RandomState(SEED)
    keys = rng.randint(0, 2 ** config.k - 1, KEY_COUNT, dtype=np.uint64)
    table = generate_hashtable(config, int(KEY_COUNT * 1.5))

    if insert_operation == "random_walk":
        insert = table.insert_single
    else:
        insert = table.insert_cost_optimal

    # insert initial keys to generate well-filled table
    if insert_operation == "random_walk":
        for k in keys:
            insert(table.storage, k)
    else:
        insert(table.storage, keys)

    for k in keys:
        result = table.search_single(table.storage, k)
        assert (
            result == True
        ), f"{insert_operation}: expected key {k} to be in the table"

    # test remove and insert operations for random selected keys multiple times
    for i in range(LOAD_TEST_ITERATIONS):
        recycled_keys = set(np.random.choice(keys, LOAD_TEST_RECYCLE_COUNT))

        # remove a set of keys
        for k in recycled_keys:
            result = table.remove_single(table.storage, k)
            assert (
                result == True
            ), f"{insert_operation}: expected key {k} to be successfully removed"

        # searching for removed keys returns None
        for k in recycled_keys:
            result = table.search_single(table.storage, k)
            assert (
                result is None
            ), f"{insert_operation}: expected key {k} to not be in the table"

        # re-insert removed keys
        if insert_operation == "random_walk":
            for k in recycled_keys:
                insert(table.storage, k)
                result = table.search_single(table.storage, k)
                assert result == True, f"expected key {k} to be in the table"
        else:
            insert(table.storage, recycled_keys, values=recycled_keys)
