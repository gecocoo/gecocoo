from functools import lru_cache
from typing import Set

import hypothesis.strategies as st
from hypothesis.stateful import Bundle, RuleBasedStateMachine, initialize, rule

from gecocoo.config import Config, HashFunction, ValueModuleConfig
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import swaplinear, xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.valuemodules import keyset

K = 32
MAX_KEY = 2 ** (2 * K) - 1
SIZE = 10 ** 6

# Caches the generated table since the config stays the same.
# Before each test the table simply gets cleared and then reused.
# This greatly improves the test's execution time.
@lru_cache()
def get_table():
    config = Config(
        load_factor=0.95,
        k=K,
        bucket_size=4,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=[
            HashFunction(get_qualified_name(swaplinear)),
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
    )

    return generate_hashtable(config, SIZE)[0]


class CountingValueModuleComparsion(RuleBasedStateMachine):
    def __init__(self):
        super(CountingValueModuleComparsion, self).__init__()
        self.model: Set[int] = set()
        self.table = get_table()

    keys = Bundle("keys")

    @initialize()
    def init_table(self):
        self.table.clear(self.table.storage)

    @rule(target=keys, key=st.integers(min_value=0, max_value=MAX_KEY))
    def gen_key(self, key):
        return key

    @rule(key=keys)
    def insert(self, key):
        self.table.insert_single(self.table.storage, key)
        self.model.add(key)

    @rule(key=keys)
    def remove(self, key):
        self.table.remove_single(self.table.storage, key)
        self.model.discard(key)

    @rule(key=keys)
    def check_value_for_key(self, key):
        expected_value = key in self.model or None
        table_value = self.table.search_single(self.table.storage, key)
        assert expected_value == table_value

    @rule()
    def check_iterator(self):
        entries = list(self.table.key_value_iterator(self.table.storage))
        assert len(self.model) == len(entries)
        for k, v in entries:
            expected_value = k in self.model or None
            assert expected_value == v


TestCountingValueModule = CountingValueModuleComparsion.TestCase
