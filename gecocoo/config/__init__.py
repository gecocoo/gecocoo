"""The package contains the definition of GeCoCoo's configuration and functions for working with configuration objects."""

from .definition import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from .lowlevel import ConfigError
