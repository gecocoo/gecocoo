"""The module contains miscellaneous function for hash tables that do not require a complex implementation."""

from functools import lru_cache
from typing import Any, Callable

from numba import njit, uint64, void
from numba.types import Type

from ..config.lowlevel import LLConfig
from .storage import StorageAccess


@lru_cache(maxsize=5)
def define_clear(
    config: LLConfig, storage_type: Type, access: StorageAccess
) -> Callable[[Any], None]:
    """
    Generate a jit-compiled function that when called clears the hash table.

    :param config: the hash table configuration
    :param storage_type: the type of the hash table's storage object
    :param access: the access abstraction object for the hash table
    :return: the generated function
    """

    @njit(void(storage_type))
    def clear(storage):
        access.set_key_count(storage, 0)
        # TODO: add access abstraction for zeroing array
        storage.fill(0)

    return clear


@lru_cache(maxsize=5)
def define_get_key_count(
    config: LLConfig, storage_type: Type, access: StorageAccess
) -> Callable[[Any], int]:
    """
    Generate a jit-compiled function that when called returns teh count of unique keys in the table.

    :param config: the hash table configuration
    :param storage_type: the type of the hash table's storage object
    :param access: the access abstraction object for the hash table
    :return: the generated function
    """

    @njit(uint64(storage_type))
    def get_key_count(storage):
        return access.get_key_count(storage)

    return get_key_count
