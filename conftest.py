import pytest


@pytest.fixture
def mock_env(monkeypatch):
    monkeypatch.setenv("DEBUG", "2")
