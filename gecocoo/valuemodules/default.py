"""
The module provides the default value modules provided with GeCoCoo.

The value modules defined here should serve two purposes:

- Provide ready to use implementations of common hash table use case
- Act as examples for users implementing their own value modules
"""

from numba import njit, uint64


def counting(bits: int = 32, initial_count: int = 1, increment: int = 1):
    """
    Instantiate a value module for counting inserted keys.

    The table's insert function expects only the key, no further arguments should be provided.

    :param bits: the number of bits to use to store the current count, should depend on the expected maximum value
    :param initial_count: the number to use as the initial value for the first insertion of a key, defaults to ``1``
    :param increment: the number to increment the current count by when a key is inserted again, defaults to ``1``
    :return: the generated value module
    """

    @njit(uint64(uint64))
    def first_insert(key: uint64) -> uint64:
        return initial_count

    @njit(uint64(uint64, uint64))
    def update_value(key: uint64, stored_value: uint64) -> uint64:
        return stored_value + increment

    @njit
    def map_value(key: uint64, stored_value: uint64):
        return stored_value

    return (
        first_insert,
        update_value,
        map_value,
        bits,
    )


def store(bits: int = 64):
    """
    Instantiate a value module that simply stores the specified number of ``bits`` with each key.

    The value to store must be provided to the insert function.

    :param bits: the number of bits to store, defaults to ``64``
    :return: the generated value module
    """

    @njit(uint64(uint64, uint64))
    def first_insert(key: uint64, value: uint64) -> uint64:
        return value

    @njit(uint64(uint64, uint64, uint64))
    def update_value(key: uint64, old_value: uint64, value: uint64) -> uint64:
        return value

    @njit
    def map_value(key: uint64, stored_value: uint64):
        return stored_value

    return (
        first_insert,
        update_value,
        map_value,
        bits,
    )


def keyset():
    """
    Instantiate a value module that makes the hash table act as a simple set, only storing the keys without any associated values.

    Therefore only the keys have to be stored in the hash table and values require zero bits.
    Searching for a key always returns ``True`` when the key is in the table.
    """

    @njit(uint64(uint64))
    def first_insert(key):
        return 0

    @njit(uint64(uint64, uint64))
    def update_value(key, stored_value):
        return 0

    @njit
    def map_value(key, stored_value):
        return True

    return (
        first_insert,
        update_value,
        map_value,
        0,
    )
