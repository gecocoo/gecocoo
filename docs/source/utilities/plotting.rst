Plotting
========

The module :mod:`gecocoo.plotting` provides some utilities for plotting information of hash table instances.
Plotting works in two steps:

1. Calculate the data to plot by analyzing a hash table instance
2. Plot the calculated data

The plotting functions are implemented using `matplotlib <https://matplotlib.org/>`_.
The package has to be installed in order to use the functions.
The data calculation itself does not depend on matplotlib.

Currently the following plots are implemented:

- Calculate the occupancy information for a hash table instance using :func:`gecocoo.interface.HashtableUtilities.occupancy`.
  Then plot it using one of the following functions:

  - :func:`gecocoo.plotting.plot_hash_usage`
  - :func:`gecocoo.plotting.plot_hash_usage_total`
  - :func:`gecocoo.plotting.plot_slot_usage`
  - :func:`gecocoo.plotting.plot_slot_usage_total`

Example Code
------------

The following example demonstrates the plotting utilities.

File in the repository: :repo_url:`examples/plotting.py`

.. literalinclude:: ../../../examples/plotting.py
  :language: python
  :linenos:
  :emphasize-lines: 49-

Example Plots
-------------

Executing the example code above should produce plots similar to the following ones.

.. |plot_hash_usage| image:: plots/hash_usage.png
.. |plot_hash_usage_total| image:: plots/hash_usage_total.png
.. |plot_slot_usage| image:: plots/slot_usage.png
.. |plot_slot_usage_total| image:: plots/slot_usage_total.png

+------------------------------------------+-------------------------------------------+
| |plot_hash_usage|                        | |plot_hash_usage|                         |
|                                          |                                           |
| using                                    | using                                     |
| :func:`gecocoo.plotting.plot_hash_usage` | :func:`gecocoo.plotting.hash_usage_total` |
+------------------------------------------+-------------------------------------------+
| |plot_slot_usage|                        | |plot_slot_usage_total|                   |
|                                          |                                           |
| using                                    | using                                     |
| :func:`gecocoo.plotting.plot_hash_usage` | :func:`gecocoo.plotting.slot_usage_total` |
+------------------------------------------+-------------------------------------------+