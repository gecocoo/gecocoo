"""The module provides hash function for the use in GeCoCoo hash tables."""

from .swaplinear import swaplinear
from .xorlinear import xorlinear
