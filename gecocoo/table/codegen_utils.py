"""The module provides some functions for code generation used for generating hash tables."""

from typing import Callable, Iterable

from numba import njit, uint64
from numba.core.typing.templates import Signature


@njit(inline="always")
def get_tuple_field(tuple, index):  # noqa
    index = uint64(index)
    current_index = uint64(0)
    for field in tuple:
        if current_index == index:
            return field

        current_index += 1

    raise Exception("tuple access out of bounds")


def make_call_by_index(signature: Signature, fs: Iterable[Callable]) -> Callable:
    """
    Generate a jit-compiled function that takes an index and then calls the function at the index.

    :param signature: the signature of the functions to index
    :param fs: the functions to call by index
    :return:
        a jit-compiled function that must be called with an ``int`` index as its first argument,
        followed by the required arguments for the base functions.
    """

    my_globals = dict()
    my_locals = dict()

    # Generate code that calls the given functions based on the passed index
    arg_count = len(signature.args)
    args = ", ".join([f"a{i}" for i in range(arg_count)])
    code = f"def f(index, {args}):\n"
    for i, f in enumerate(fs):
        name = f"f{i}"
        my_globals[name] = f
        code += f"\tif index == {i}:\n"
        code += f"\t\treturn {name}({args})\n"
    code += """\traise Exception("function index out of bounds")\n"""

    # Evaluate the generated code and extract the function
    exec(code, my_globals, my_locals)
    call_by_index = my_locals["f"]

    # jit compile generated function with the correct signature
    index_signature = signature.return_type(uint64, *signature.args)
    jitted_call_by_index = njit(index_signature)(call_by_index)

    return jitted_call_by_index
