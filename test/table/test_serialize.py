import os
import pathlib
from typing import Any

import numpy as np
import pytest

from gecocoo.config import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import swaplinear, xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.table.serialize import deserialize
from gecocoo.valuemodules import store


@pytest.mark.parametrize(
    "storage",
    [
        pytest.param(StorageConfig(mode=StorageMode.PACKED, concurrent=False)),
        pytest.param(StorageConfig(mode=StorageMode.PACKED, concurrent=True)),
        pytest.param(StorageConfig(mode=StorageMode.BUCKETS, concurrent=False)),
        pytest.param(StorageConfig(mode=StorageMode.BUCKETS, concurrent=True)),
        pytest.param(
            StorageConfig(
                mode=StorageMode.FAST, concurrent=True, disable_quotienting=True
            )
        ),
        pytest.param(
            StorageConfig(
                mode=StorageMode.FAST, concurrent=False, disable_quotienting=True
            )
        ),
    ],
)
def test_content(tmp_path: pathlib.Path, storage: StorageConfig):
    config = Config(
        load_factor=0.5,
        k=14,
        bucket_size=4,
        value_module=ValueModuleConfig(get_qualified_name(store)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear), []),
            HashFunction(get_qualified_name(swaplinear), []),
        ],
        storage=storage,
        n=10000,
    )

    table, utils = generate_hashtable(config)
    rng = np.random.RandomState(6789)
    keys = rng.randint(0, 4 ** config.k, config.n, dtype=np.uint64)

    for key in keys:
        table.insert_single(table.storage, key, key * 3 - 45)
    utils.metadata = dict(message="Hello World!")

    file = tmp_path / "table.npz"
    utils.serialize(file)
    table_d, utils_d = deserialize(file)

    assert utils_d.metadata == utils.metadata

    actual_count = table_d.get_key_count(table_d.storage)
    expected_count = table.get_key_count(table.storage)
    assert actual_count == expected_count

    actual_content = list(table_d.debug_iterator(table_d.storage))
    expected_content = list(table.debug_iterator(table.storage))
    assert actual_content == expected_content


@pytest.mark.parametrize(
    "metadata",
    [
        pytest.param(None),
        pytest.param(
            {
                "test_string": "Hello äüö 🍰",
                "test_int": 1,
                "test_float": 13.37,
                "test_list": [1, 2.564, None, "test"],
                "test_empty": None,
                "test_dict": {"snake": "🐍"},
            }
        ),
    ],
)
def test_metadata(tmp_path: pathlib.Path, metadata: Any):
    config = Config(
        load_factor=0.95,
        k=14,
        bucket_size=4,
        value_module=ValueModuleConfig(get_qualified_name(store)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear), []),
            HashFunction(get_qualified_name(swaplinear), []),
        ],
        n=1000,
    )

    file = tmp_path / "table.npz"

    table, utils = generate_hashtable(config)
    utils.metadata = metadata

    utils.serialize(file)
    table_d, utils_d = deserialize(file)

    assert table is not table_d
    assert utils is not utils_d
    assert metadata is None or utils_d.metadata is not metadata
    assert utils_d.metadata == metadata


def test_compress(tmp_path: pathlib.Path):
    config = Config(
        load_factor=0.95,
        k=14,
        bucket_size=4,
        value_module=ValueModuleConfig(get_qualified_name(store)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear), []),
            HashFunction(get_qualified_name(swaplinear), []),
        ],
        n=1000000,
    )

    file_uncompressed = tmp_path / "table_uncompressed.npz"
    file_compressed = tmp_path / "table_compressed.npz"

    table, utils = generate_hashtable(config)
    utils.metadata = dict(message="An empty Table.")

    utils.serialize(file_uncompressed)
    utils.serialize(file_compressed, compress=True)

    # As we serialized an empty table, the compressed file should be much smaller
    size_uncompressed = file_uncompressed.stat().st_size
    size_compressed = file_compressed.stat().st_size
    assert size_uncompressed > size_compressed

    table_d, utils_d = deserialize(file_compressed)

    assert table is not table_d
    assert utils is not utils_d
    assert utils_d.metadata is not utils.metadata
    assert utils_d.metadata == utils.metadata
    assert table_d.get_key_count(table_d.storage) == 0


# file name -> config association
COMPATIBILITY_TESTS = {
    "test_compatibility_01.npz": Config(
        load_factor=0.95,
        k=14,
        bucket_size=4,
        value_module=ValueModuleConfig(get_qualified_name(store)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear), []),
            HashFunction(get_qualified_name(swaplinear), []),
        ],
        n=1000,
    )
}


@pytest.mark.parametrize("file", COMPATIBILITY_TESTS.keys())
def test_compatibility(file: str):
    """Load one serialized hash tables. If this fails we know we made a breaking change."""

    path = os.path.join(os.path.dirname(__file__), "data", file)
    table_d, utils_d = deserialize(path)

    value = table_d.search_single(table_d.storage, 200)

    assert value == 999
    assert utils_d.metadata == dict(test_meta="Hello World!")


def generate_test_compatibility_tables():
    """Generate the test tables for test_compatibility."""

    os.makedirs(os.path.join(os.path.dirname(__file__), "data"), exist_ok=True)

    for file, config in COMPATIBILITY_TESTS.items():
        print("Generate", file)

        table, utils = generate_hashtable(config)
        table.insert_single(table.storage, 200, 999)
        utils.metadata = dict(test_meta="Hello World!")

        path = os.path.join(os.path.dirname(__file__), "data", file)
        utils.serialize(path)


if __name__ == "__main__":
    # Regenerate the test table for test_compatibility.
    # This should not be required, unless a breaking change has been made somewhere
    # that effect serialization.
    generate_test_compatibility_tables()
