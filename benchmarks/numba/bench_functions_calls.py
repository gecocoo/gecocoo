from typing import Callable, NamedTuple

import benchutils
import hypothesis.strategies as st
import pytest
from hypothesis import given
from numba import njit, uint64

ROUNDS = benchutils.apply_rounds_factor(50)
WARMUP_ROUNDS = benchutils.apply_rounds_factor(5, 0, 10)
COUNT = 4
functions = list([njit(uint64(uint64))(lambda x: x * i) for i in range(1, COUNT + 1)])
f1, f2, f3, f4 = functions


@njit(uint64(uint64))
def call1(x):
    return f1(x)


@njit(uint64(uint64))
def call2(x):
    return f1(x) + f2(x)


@njit(uint64(uint64))
def call3(x):
    return f1(x) + f2(x) + f3(x)


@njit(uint64(uint64))
def call4(x):
    return f1(x) + f2(x) + f3(x) + f4(x)


def make_tuple_call(count):
    fs = tuple(functions[:count])

    @njit(uint64(uint64))
    def call(x):
        sum = uint64(0)
        for f in fs:
            sum += f(x)
        return sum

    return call


tuple_call1 = make_tuple_call(1)
tuple_call2 = make_tuple_call(2)
tuple_call3 = make_tuple_call(3)
tuple_call4 = make_tuple_call(4)


@njit(uint64(uint64, uint64))
def call_by_index(index, x):
    if index == 0:
        return f1(x)
    if index == 1:
        return f2(x)
    if index == 2:
        return f3(x)
    if index == 3:
        return f4(x)

    raise Exception("function index out of bounds")


def make_indexed_call(count):
    @njit(uint64(uint64))
    def call(x):
        sum = uint64(0)
        for i in range(count):
            sum += call_by_index(i, x)
        return sum

    return call


indexed_call1 = make_indexed_call(1)
indexed_call2 = make_indexed_call(2)
indexed_call3 = make_indexed_call(3)
indexed_call4 = make_indexed_call(4)


@njit((uint64,))
def get_function_by_index(index):
    if index == 0:
        return f1
    if index == 1:
        return f2
    if index == 2:
        return f3
    if index == 3:
        return f4

    raise Exception("function index out of bounds")


def make_get_indexed_call(count):
    @njit(uint64(uint64))
    def call(x):
        sum = uint64(0)
        for i in range(count):
            f = get_function_by_index(i)
            sum += f(x)
        return sum

    return call


get_indexed_call1 = make_get_indexed_call(1)
get_indexed_call2 = make_get_indexed_call(2)
get_indexed_call3 = make_get_indexed_call(3)
get_indexed_call4 = make_get_indexed_call(4)


def using_make_call_by_index(count):
    from gecocoo.table.codegen_utils import make_call_by_index

    f = make_call_by_index(uint64(uint64), functions[:count])

    @njit(uint64(uint64))
    def call(x):
        sum = uint64(0)
        for i in range(count):
            f(i, x)
        return sum

    return call


call_by_index1 = using_make_call_by_index(1)
call_by_index2 = using_make_call_by_index(2)
call_by_index3 = using_make_call_by_index(3)
call_by_index4 = using_make_call_by_index(4)


class Benchmark(NamedTuple):
    id: str
    call: Callable


BENCHMARKS = [
    Benchmark("call 1 function directly", call1),
    Benchmark("call 2 functions directly", call2),
    Benchmark("call 3 functions directly", call3),
    Benchmark("call 4 functions directly", call4),
    Benchmark("call 1 function using tuple", tuple_call1),
    Benchmark("call 2 functions using tuple", tuple_call2),
    Benchmark("call 3 functions using tuple", tuple_call3),
    Benchmark("call 4 functions using tuple", tuple_call4),
    Benchmark("call 1 function using indexes", indexed_call1),
    Benchmark("call 2 functions using indexes", indexed_call2),
    Benchmark("call 3 functions using indexes", indexed_call3),
    Benchmark("call 4 functions using indexes", indexed_call4),
    Benchmark("call 1 function get by indexes", get_indexed_call1),
    Benchmark("call 2 functions get by indexes", get_indexed_call2),
    Benchmark("call 3 functions get by indexes", get_indexed_call3),
    Benchmark("call 4 functions get by indexes", get_indexed_call4),
    Benchmark("call 1 function by index", call_by_index1),
    Benchmark("call 2 functions by index", call_by_index2),
    Benchmark("call 3 functions by index", call_by_index3),
    Benchmark("call 4 functions by index", call_by_index4),
]


@pytest.mark.parametrize(
    "call,name", [pytest.param(call, id, id=id) for id, call in BENCHMARKS]
)
def bench_calls(benchmark, call, name):
    benchmark.group = "function calls"
    benchmark.name = name

    @njit
    def work(x):
        sum = 0
        for i in range(x):
            sum += call(i)

        return sum

    benchmark.pedantic(work, args=(1000,), rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS)


@pytest.mark.parametrize(
    "fs",
    [
        (call1, tuple_call1, indexed_call1, get_indexed_call1),
        (call2, tuple_call2, indexed_call2, get_indexed_call2),
        (call3, tuple_call3, indexed_call3, get_indexed_call3),
        (call4, tuple_call4, indexed_call4, get_indexed_call4),
    ],
)
@given(x=st.integers(min_value=0, max_value=2 ** 64 - 1))
def test_equivalence(fs, x):
    ys = list(map(lambda f: f(x), fs))
    y1 = ys[0]
    expected_ys = list([y1 for _ in range(len(ys))])

    assert expected_ys == ys


if __name__ == "__main__":
    import os

    print("Dumping llvm ir and asm code...")
    target_dir = "dump/"
    os.makedirs(target_dir)

    for id, call in BENCHMARKS:
        with open(target_dir + id + ".ll", "w") as io:
            llvm = call.inspect_llvm(call.signatures[0])
            io.write(llvm)
        with open(target_dir + id + ".S", "w") as io:
            asm = call.inspect_asm(call.signatures[0])
            io.write(asm)
