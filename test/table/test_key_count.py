import numpy as np
import pytest
from numba import njit, prange

from gecocoo.config import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.valuemodules import keyset


@pytest.mark.parametrize(
    "storage",
    [
        pytest.param(StorageConfig(mode=StorageMode.PACKED, concurrent=False)),
        pytest.param(StorageConfig(mode=StorageMode.PACKED, concurrent=True)),
        pytest.param(StorageConfig(mode=StorageMode.BUCKETS, concurrent=False)),
        pytest.param(StorageConfig(mode=StorageMode.BUCKETS, concurrent=True)),
        pytest.param(
            StorageConfig(
                mode=StorageMode.FAST, concurrent=False, disable_quotienting=True
            )
        ),
        pytest.param(
            StorageConfig(
                mode=StorageMode.FAST, concurrent=True, disable_quotienting=True
            )
        ),
    ],
    ids=str,
)
def test_key_count(storage):
    config = Config(
        load_factor=0.95,
        k=32,
        bucket_size=4,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
        storage=storage,
    )
    (table, _) = generate_hashtable(config, 10 ** 5)

    keys = np.random.randint(0, 2 ** 64 - 1, 1000, dtype=np.uint64)
    unique_keys = np.unique(keys)

    # insert the keys, but do so in parallel when concurrent is True
    # FYI: prange acts as a normal range if parallel=False
    @njit(parallel=storage.concurrent)
    def insert(table):
        for i in prange(len(keys)):
            table.insert_single(table.storage, keys[i])

    insert(table)

    expected_key_count = len(unique_keys)
    actual_key_count = table.get_key_count(table.storage)
    assert (
        expected_key_count == actual_key_count
    ), "key count after inserting is incorrect"

    # take a part of the keys for removal
    remove_keys, remaining_keys = np.split(unique_keys, 2)

    # remove the keys (for parallel, see above)
    @njit(parallel=storage.concurrent)
    def remove(table):
        for i in prange(len(remove_keys)):
            table.remove_single(table.storage, remove_keys[i])

    remove(table)

    expected_key_count = len(remaining_keys)
    actual_key_count = table.get_key_count(table.storage)
    assert (
        expected_key_count == actual_key_count
    ), "key count after removing is incorrect"

    # clear the table
    table.clear(table.storage)
    actual_key_count = table.get_key_count(table.storage)
    assert actual_key_count == 0, "key count after clearing is incorrect"
