import benchutils
import numpy as np
import pytest
from numba import njit, uint64

from gecocoo.config import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import generate_random_kmers, get_qualified_name
from gecocoo.valuemodules import keyset

ROUNDS = benchutils.apply_rounds_factor(10)
WARMUP_ROUNDS = benchutils.apply_rounds_factor(2, 0, 5)
SEED = 1234
KEY_COUNT = benchutils.apply_data_factor(10 ** 4, 10 ** 4)
STORAGE_CONFIGS = list(
    [
        StorageConfig(
            mode=StorageMode.BUCKETS,
            concurrent=True,
            disable_occupied_flag=False,
            disable_quotienting=False,
        ),
        StorageConfig(
            mode=StorageMode.BUCKETS,
            concurrent=True,
            disable_occupied_flag=True,
            disable_quotienting=False,
        ),
    ]
)


@pytest.mark.parametrize("storage", STORAGE_CONFIGS)
def bench_storage_insert(benchmark, storage: StorageConfig):
    benchmark.group = "table occupied/quotienting (insert)"
    benchmark.name = f"mode = {storage.mode}, disable_occupied = {storage.disable_occupied_flag} disable_quotienting = {storage.disable_quotienting}"

    config = Config(
        load_factor=0.95,
        k=31,
        bucket_size=4,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
        storage=storage,
    )
    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 32 - 1, KEY_COUNT, dtype=np.uint64)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)
    (table, _) = generate_hashtable(config, KEY_COUNT)

    def setup():
        table.clear(table.storage)

    @njit
    def work(table, keys):
        for k in keys:
            table.insert_single(table.storage, k)

    benchmark.pedantic(
        work,
        args=(table, keys),
        setup=setup,
        rounds=ROUNDS,
        warmup_rounds=WARMUP_ROUNDS,
    )


@pytest.mark.parametrize("storage", STORAGE_CONFIGS)
def bench_storage_search(benchmark, storage: StorageConfig):
    benchmark.group = "table occupied/quotienting (search)"
    benchmark.name = f"mode = {storage.mode}, disable_occupied = {storage.disable_occupied_flag} disable_quotienting = {storage.disable_quotienting}"

    config = Config(
        load_factor=0.95,
        k=31,
        bucket_size=4,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
        storage=storage,
    )

    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 32 - 1, KEY_COUNT, dtype=np.uint64)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)
    (table, _) = generate_hashtable(config, KEY_COUNT)

    @njit
    def setup(table):
        for k in keys:
            table.insert_single(table.storage, k)

    # insert all keys once
    setup(table)

    @njit
    def work(table):
        sum = uint64(0)
        for k in keys:
            sum += table.search_single(table.storage, k)
        return sum

    benchmark.pedantic(work, args=(table,), rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS)
