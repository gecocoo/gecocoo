"""
The module contains the definition of GeCoCoo's configuration that the user deals with.

The internal configuration definition that the hash table generation works with can be found in :mod:`gecocoo.config.lowlevel`.
"""

from dataclasses import dataclass, field
from enum import Enum
from typing import Optional, Sequence


@dataclass(frozen=True)
class HashFunction:
    """
    The class allows defining a hash function to be used in the hash table.

    :param name:
        specifies hash function as a fully qualified name to the hash function's constructor
        (example: ``"gecocoo.hashing.swaplinear"``)
    :param parameters: optional parameters for creating an instance of the hash function
    """

    name: str
    parameters: Sequence[int] = ()


@dataclass(frozen=True)
class ValueModuleConfig:
    """
    The class allows defining a value module to be used for the hash table.

    The value modules defines what value can be stored in the table and how they are stored.
    Some basic value modules are defined in :mod:`gecocoo.valuemodules`, but it is possible
    to define own value modules and use them with the hash table.

    :param module_name:
        specifies value module as a fully qualified name to the value module's constructor
        (example: ``"gecocoo.valuemodules.counting"``)
    :param parameters: optional parameters for creating an instance of the value module
    """

    module_name: str
    parameters: Sequence[int] = ()


class StorageMode(Enum):
    """
    Enumeration of the supported storage modes.

    Each of the modes has its own benefits in the tradeoff between memory efficiency and accessing performance.
    """

    PACKED = 0
    """
    Storage mode that `packs` all the hash table entries in bit precise fields and therefore wasting as little space as possible.
    The memory efficiency comes with the cost of worse data alignment and more bit operation having to be performed to extract the values.
    """

    BUCKETS = 1
    """
    Storage mode that tries to balance memory efficiency and accessing performance by aligning each bucket on 64 bit addresses.
    This improves the accessing performance while still saving some memory with packing the slots tightly per bucket.
    """

    FAST = 2
    """
    Storage mode that takes favors performance over memory efficiency by having better alignment and performaning fewer bit operations.
    It also requires disabling any quotienting done by the chosen hash functions and thereby also skipping the key restoring other storage modes do.
    """


@dataclass(frozen=True)
class StorageConfig:
    """
    The class allows defining the backing storage to use for the hash table.

    :param mode:
        the basic storage mode, refer to :class:`StorageMode` for the possible options.
        The default mode is :attr:`StorageMode.BUCKETS`.
    :param concurrent:
        ``True`` to enable concurrency support on the hash table,
        ``False`` if only single threaded access is required (default).
    :param disable_quotienting:
        ``True`` disables any quotienting the chosen hash function might do, thus
        the full key has to be stored in the hash table (default ``False``).
    :param disable_occupied_flag:
        ``True`` to disable the one bit per entry which would mark a slot as used.
        Then encode the information as key_part=0 and store items with the actual key_part 0 in a seperate location (default ``False``).
        Currently this can only be used in BUCKETS StorageMode.
    """

    mode: StorageMode = StorageMode.BUCKETS
    concurrent: bool = False
    disable_quotienting: bool = False
    disable_occupied_flag: bool = False


@dataclass(frozen=True)
class Config:
    """
    The class describes the main configuration of the hash table.

    :param bucket_size: the bucket size of the hash table
    :param k: the k-mer size of the keys (a key is :math:`2^{2 k}` bits long)
    :param load_factor: the load factor the hash table should have when full
    :param value_module: the configuration for the value module to use for the hash table
    :param hash_functions: a list of hash functions to use
    :param storage: the configuration for the backing storage of the hash table
    :param n: the number of unique keys that the hash table should be able to hold
    """

    bucket_size: int
    k: int
    load_factor: float
    value_module: ValueModuleConfig
    hash_functions: Sequence[HashFunction]
    storage: Optional[StorageConfig] = None
    n: Optional[int] = None
