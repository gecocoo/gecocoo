"""The module contains the implementation for searching in the hash table."""

from functools import lru_cache
from typing import Any, Callable

from numba import njit, uint64
from numba.types import Type, UniTuple

from ..config.lowlevel import LLConfig
from .codegen_utils import make_call_by_index
from .storage import StorageAccess


@lru_cache(maxsize=5)
def define_search(
    config: LLConfig, storage_type: Type, access: StorageAccess
) -> Callable[..., Any]:
    """
    Generate a function that searches for a single key in the hash table and returns its associated value if found.

    :param config: the hash table configuration
    :param storage_type: the type of the hash table's storage object
    :param access: the access abstraction object for the hash table
    :return: the generated function
    """

    bucket_count = config.num_buckets
    bucket_size = config.bucket_size
    hash_forward = make_call_by_index(
        UniTuple(uint64, 2)(uint64), [h.forward for h in config.hash_functions]
    )
    hash_function_count = len(config.hash_functions)
    map_value = config.value_module.map_value

    @njit((storage_type, uint64))
    def search(storage, key):
        found_relocating = True
        while found_relocating:
            found_relocating = False
            for hash_index in range(hash_function_count):
                hash, quotient = hash_forward(hash_index, key)
                index = hash % bucket_count
                for slot in range(bucket_size):
                    match, relocating, value = access.compare_entry(
                        storage, index, slot, hash_index, quotient
                    )
                    found_relocating |= relocating
                    if match and not relocating:
                        return map_value(key, value)

        # value not found
        return None

    return search
