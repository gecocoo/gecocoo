from gecocoo.config import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.hashing import xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.valuemodules import store

# Create a configuration for the hashtable
config = Config(
    load_factor=0.95,
    k=14,  # k-mer size: 1-32
    bucket_size=4,
    value_module=ValueModuleConfig(get_qualified_name(store)),
    hash_functions=[
        HashFunction(get_qualified_name(xorlinear)),
        HashFunction(get_qualified_name(xorlinear)),
    ],
    storage=StorageConfig(
        # mode=StorageMode.PACKED, # Best for best memory efficiency, but worst performance
        # mode=StorageMode.FAST, # Best for best performance, but worst memory efficiency
        mode=StorageMode.BUCKETS,  # Good memory efficiency + good performance
        concurrent=False,  # True to enable conccurrency support
        disable_quotienting=False,  # must be True for FAST storage mode!
    ),  # storage=None for default configuration
)

# Generate hashtable from configuration and show usage
from gecocoo.generator import generate_hashtable

table, _ = generate_hashtable(config, 10 ** 6)

for key in range(500):
    table.insert_single(table.storage, key, key)
    assert table.search_single(table.storage, key) == key

for key in range(500):
    table.remove_single(table.storage, key)

for key in range(500):
    assert table.search_single(table.storage, key) is None
