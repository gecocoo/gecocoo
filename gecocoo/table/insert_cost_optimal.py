"""
The module contains the implementation for the cost optimal insertion strategy in cuckoo hash tables.

See: https://epubs.siam.org/doi/pdf/10.1137/1.9781611976007.15

Optimization with get_bits_fast and set_bits_fast based on https://gitlab.com/cuckopt/cuckopt/-/blob/master/cuckopt/iter_pages.py
"""

from functools import lru_cache
from typing import Callable

import numba
import numpy as np
from numba import boolean, njit, types, uint64, void
from numba.types import UniTuple

from ..config.lowlevel import LLConfig
from ..utils.codegen import FunctionGenerator
from .bitarray_functions import (
    calculate_array_size,
    get_bits,
    get_bits_fast,
    set_bits,
    set_bits_fast,
)
from .codegen_utils import make_call_by_index
from .storage import StorageAccess


@lru_cache(maxsize=5)
def define_insert_cost_optimal(
    config: LLConfig, storage_type: types.Type, access: StorageAccess
) -> Callable:
    """
    Generate a cost optimal initialization function a hash table.

    :param config: the hash table configuration
    :param storage_type: the type of the hash table's storage object
    :param access: the access abstraction object for the hash table
    :return: the generated function
    """

    int16_max = 65535

    storage_count = config.num_buckets
    bucket_size = config.bucket_size
    hashes_forward = make_call_by_index(
        types.UniTuple(uint64, 2)(uint64), [h.forward for h in config.hash_functions]
    )
    hash_count = len(config.hash_functions)

    # Build a function that calculates the value to insert into the table.
    # This provides an abstraction over the variable number of parameters value modules can take.
    code = FunctionGenerator(
        "calculate_value",
        ("key", "values", "index"),
        {"first_insert": config.value_module.first_insert},
    )
    code += (
        "return first_insert("
        + ", ".join(
            ["key"]
            + [
                f"values[{i}][index]"
                for i in range(config.value_module.parameter_count)
            ]
        )
        + ")"
    )
    calculate_value = code.generate_jit(
        uint64(
            uint64,
            UniTuple(uint64[:], config.value_module.parameter_count),
            uint64,
        )
    )

    @njit(
        void(
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            UniTuple(uint64[:], config.value_module.parameter_count),
            storage_type,
        ),
        locals=dict(
            current_assignment=numba.uint64,
            hash_value=numba.uint64,
            rest=numba.uint64,
            in_bucket_index=numba.uint64,
            bucket_index=numba.uint64,
            index=numba.uint64,
        ),
    )
    def set_table_content_from_assignment(
        insert_count, assignment_array, assignment_bits, keys, values, storage
    ):
        # Finally uses the assignements to fill the table:
        for index in range(insert_count):
            # Calculates the index:
            current_assignment = get_bits(assignment_array, assignment_bits, index)
            hash_value, rest = hashes_forward(current_assignment, keys[index])
            bucket_index = hash_value % storage_count
            value = calculate_value(keys[index], values, index)

            # Inserts in the first empty spot in the bucket:
            inserted = False
            for slot in range(bucket_size):
                occupied, _, _, _, _ = access.get_entry(storage, bucket_index, slot)

                if not occupied and access.set_empty_entry(
                    storage,
                    bucket_index,
                    slot,
                    False,  # relocating
                    current_assignment,
                    rest,
                    value,
                ):
                    inserted = True
                    break

            if not inserted:
                raise Exception("failed to find empty slot during table fill")

        # Finally sets the new key-count, now, that the keys are in the table:
        access.set_key_count(storage, insert_count)

    @njit(
        numba.int64(
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            uint64,
        ),
        locals=dict(
            failed_count=numba.int64,
            key=numba.uint64,
            fill_state=numba.uint64,
            bucket_h1=numba.uint64,
            bucket_h2=numba.uint64,
            index=numba.uint64,
        ),
    )
    def do_initialization_passes(
        insert_count,
        assignment_array,
        assignment_bits,
        bucket_fill_array,
        bucket_fill_bits,
        keys,
        invalid_assignment,
        invalid_element,
    ):
        failed_count = 0

        # Inserts the values into the table by using the initialization passes, if the table was empty before:
        # First initialization pass:
        index = 0
        for key in keys:
            bucket_h1 = hashes_forward(0, key)[0] % storage_count
            fill_state = get_bits(bucket_fill_array, bucket_fill_bits, bucket_h1)

            if fill_state < bucket_size:
                # Inserts the key and value and increments the bucket-fill:
                set_bits(assignment_array, assignment_bits, index, 0)
                set_bits(bucket_fill_array, bucket_fill_bits, bucket_h1, fill_state + 1)
            else:
                failed_count += 1
            index += 1

        # Second initialization pass, this time it iterates over the failed items from before:
        failed_count = 0
        index = 0

        for key in keys:
            if get_bits(assignment_array, assignment_bits, index) == invalid_assignment:
                bucket_h2 = hashes_forward(1, key)[0] % storage_count
                fill_state = get_bits(bucket_fill_array, bucket_fill_bits, bucket_h2)

                if fill_state < bucket_size:
                    # Inserts the key and value and increments the bucket-fill:
                    set_bits(assignment_array, assignment_bits, index, 1)
                    set_bits(
                        bucket_fill_array, bucket_fill_bits, bucket_h2, fill_state + 1
                    )
                else:
                    failed_count += 1
            index += 1

        # Returns the number of failed insertions:
        return failed_count

    @njit(
        void(
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            uint64,
        ),
        locals=dict(
            index=numba.uint64,
            key=numba.uint64,
            hash_index=numba.uint64,
            hash_value=numba.uint64,
            current_assignment=numba.uint64,
            new_outedge_count=numba.uint64,
            helper_value=numba.uint64,
            outedge_index=numba.uint64,
            next_start=numba.uint64,
            next_occurence=numba.uint64,
        ),
    )
    def setup_outedge_array(
        insert_count,
        assignment_array,
        assignment_bits,
        outedge_start_array,
        outedge_start_bits,
        outedge_element_array,
        outedge_element_bits,
        outedge_hash_array,
        outedge_hash_bits,
        keys,
        invalid_assignment,
        invalid_element,
    ):
        # Goes over the keys and calculates the buckets for all of them and then counts for each bucket how many edges lead to them:
        for index in range(insert_count):
            key = keys[index]

            for hash_index in range(hash_count):
                # Calculates the hash function:
                hash_value = hashes_forward(hash_index, key)[0] % storage_count

                # Only counts such edges if they actually go out of the bucket, uses the helper array to store the count:
                current_assignment = get_bits(assignment_array, assignment_bits, index)

                if (
                    current_assignment == invalid_assignment
                    or hashes_forward(current_assignment, key)[0] % storage_count
                    != hash_value
                ):
                    set_bits(
                        outedge_start_array,
                        outedge_start_bits,
                        hash_value,
                        get_bits(outedge_start_array, outedge_start_bits, hash_value)
                        + 1,
                    )  # Increment

        # Now determines the start indexes from the counts:
        next_start = 0
        next_occurence = 0

        for index in range(1, storage_count + 1):
            next_occurence = get_bits(
                outedge_start_array, outedge_start_bits, uint64(index - 1)
            )
            set_bits(
                outedge_start_array, outedge_start_bits, uint64(index - 1), next_start
            )

            next_start = next_start + next_occurence

        # Finally sets the last index:
        set_bits(
            outedge_start_array,
            outedge_start_bits,
            storage_count + 1,
            next_start + next_occurence,
        )

        # Goes over the keys and hashes again and this time stores the info at the spots in the now sufficiently indexed arrays:
        for index in range(insert_count):
            key = keys[index]
            for hash_index in range(hash_count):
                # Calculates the hash function:
                hash_value = hashes_forward(hash_index, key)[0] % storage_count

                # Only assignes such edges if they actually go out of the bucket:
                current_assignment = get_bits(assignment_array, assignment_bits, index)
                if (
                    current_assignment == invalid_assignment
                    or hashes_forward(current_assignment, key)[0] % storage_count
                    != hash_value
                ):
                    # Uses the start index array as index memory for the next position:
                    outedge_index = get_bits(
                        outedge_start_array, outedge_start_bits, hash_value
                    )

                    # Sets the values in the outedge-hash and element arrays:
                    set_bits(
                        outedge_element_array,
                        outedge_element_bits,
                        outedge_index,
                        index,
                    )
                    set_bits(
                        outedge_hash_array, outedge_hash_bits, outedge_index, hash_index
                    )

                    # Finally increases the next index where the items are going to be stored:
                    set_bits(
                        outedge_start_array,
                        outedge_start_bits,
                        hash_value,
                        outedge_index + 1,
                    )  # Increment

        # After the start index has been used for insertion it no longer holds the start indexes.
        # These now get recalculated from the current state of the array:
        for index in range(storage_count, 0, -1):
            set_bits(
                outedge_start_array,
                outedge_start_bits,
                index,
                get_bits(outedge_start_array, outedge_start_bits, uint64(index - 1)),
            )

        # Finally sets the first start index:
        set_bits(outedge_start_array, outedge_start_bits, 0, 0)

    @njit(
        void(
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            uint64,
        ),
        locals=dict(
            found_active=numba.boolean,
            index=numba.uint64,
            outedge_index=numba.uint64,
            first_prev=numba.uint64,
            end_prev=numba.uint64,
            prev_index=numba.uint64,
            prev_key=numba.uint64,
            prev_hash=numba.uint64,
            prev_assignment=numba.uint64,
            prev_assignment_bucket=numba.uint64,
            old_cost=numba.uint64,
            new_cost=numba.uint64,
        ),
    )
    def do_search_phase(
        insert_count,
        assignment_array,
        assignment_bits,
        bucket_cost_array,
        bucket_cost_bits,
        emba_array,
        emba_bits,
        outedge_start_array,
        outedge_start_bits,
        outedge_element_array,
        outedge_element_bits,
        outedge_hash_array,
        outedge_hash_bits,
        prev_element_array,
        prev_element_bits,
        prev_bucket_array,
        prev_bucket_bits,
        keys,
        invalid_assignment,
        invalid_element,
    ):
        # Now goes over the buckets periodically until none of them are active anymore and calculates the best movement paths for the elements:
        found_active = True

        while found_active == True:
            # No active buckets found thus far:
            found_active = False

            # Goes over all the buckets and only calculates new previous buckets for the active ones:
            for index in range(storage_count):
                if (
                    get_bits_fast(emba_array, emba_bits, index) == 1
                ):  # Is the bucket active 0=no, 1=yes
                    # First inactivates the current bucket:
                    set_bits_fast(emba_array, emba_bits, index, 0)

                    # Next, iterates over all the (key, hash_index) pairs that lead to this bucket:
                    first_prev = get_bits(
                        outedge_start_array, outedge_start_bits, index
                    )
                    end_prev = get_bits(
                        outedge_start_array, outedge_start_bits, index + 1
                    )  # Non inclusive

                    for outedge_index in range(first_prev, end_prev):
                        # Retrieves the index, key and hash of the predecessor:
                        prev_index = get_bits(
                            outedge_element_array,
                            outedge_element_bits,
                            outedge_index,
                        )

                        if prev_index != invalid_element:
                            prev_assignment = get_bits(
                                assignment_array, assignment_bits, prev_index
                            )

                            if prev_assignment != invalid_assignment:
                                # Only performs the backtracking for outgoing edges, whos elements already have an assignment:
                                prev_key = keys[prev_index]
                                prev_hash = get_bits(
                                    outedge_hash_array,
                                    outedge_hash_bits,
                                    outedge_index,
                                )

                                prev_assignment_bucket = (
                                    hashes_forward(prev_assignment, prev_key)[0]
                                    % storage_count
                                )

                                old_cost = get_bits(
                                    bucket_cost_array,
                                    bucket_cost_bits,
                                    prev_assignment_bucket,
                                )
                                new_cost = types.uint64(
                                    get_bits(bucket_cost_array, bucket_cost_bits, index)
                                    + prev_hash
                                    - prev_assignment
                                )

                                # Checks if the costs get better:
                                if old_cost > new_cost:
                                    # Updates the cost, the predecessors, sets the bucket active and sets the flag active that an active bucket has been found:
                                    set_bits(
                                        bucket_cost_array,
                                        bucket_cost_bits,
                                        prev_assignment_bucket,
                                        new_cost,
                                    )
                                    set_bits(
                                        prev_element_array,
                                        prev_element_bits,
                                        prev_assignment_bucket,
                                        prev_index,
                                    )
                                    set_bits(
                                        prev_bucket_array,
                                        prev_bucket_bits,
                                        prev_assignment_bucket,
                                        prev_hash,
                                    )
                                    set_bits_fast(
                                        emba_array,
                                        emba_bits,
                                        prev_assignment_bucket,
                                        1,
                                    )

                                    found_active = True

    @njit(
        numba.int64(
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            types.Array(numba.uint64, 1, "C"),
            uint64,
            uint64,
        ),
        locals=dict(
            failed_count=numba.int64,
            int16_max=numba.uint64,
            index=numba.uint64,
            best_hash_index=numba.uint64,
            best_cost=numba.uint64,
            key=numba.uint64,
            hash_index=numba.uint64,
            potential_bucket_index=numba.uint64,
            current_cost=numba.uint64,
            first_bucket=numba.uint64,
            current_bucket=numba.uint64,
            finished_backtracking=numba.uint64,
            current_prev_bucket_hash=numba.uint64,
            current_prev_element=numba.uint64,
            current_bucket_fill=numba.uint64,
            bt_current_bucket=numba.uint64,
            bt_next_element=numba.uint64,
            outedge_index=numba.uint64,
            bt_current_prev_bucket=numba.uint64,
            bt_current_prev_bucket_hash=numba.uint64,
            bt_current_prev_element=numba.uint64,
            prev_hash_for_outedge=numba.uint64,
        ),
    )
    def do_move_phase(
        insert_count,
        assignment_array,
        assignment_bits,
        bucket_cost_array,
        bucket_cost_bits,
        bucket_fill_array,
        bucket_fill_bits,
        emba_array,
        emba_bits,
        outedge_start_array,
        outedge_start_bits,
        outedge_element_array,
        outedge_element_bits,
        outedge_hash_array,
        outedge_hash_bits,
        prev_element_array,
        prev_element_bits,
        prev_bucket_array,
        prev_bucket_bits,
        keys,
        invalid_assignment,
        invalid_element,
    ):
        # Counts the number of failed move-procedures:
        failed_count = 0

        # Next, iterates over all the keys and only processes the unassigned ones further:
        for index in range(insert_count):
            if get_bits(assignment_array, assignment_bits, index) == invalid_assignment:
                # Finds the best starting point for the key and with that the best path if one exists:
                best_hash_index = invalid_assignment
                best_cost = int16_max

                key = keys[index]

                for hash_index in range(hash_count):
                    potential_bucket_index = (
                        hashes_forward(hash_index, key)[0] % storage_count
                    )
                    current_cost = hash_index + get_bits(
                        bucket_cost_array, bucket_cost_bits, potential_bucket_index
                    )

                    if current_cost < best_cost:
                        # Better starting point was found:
                        best_cost = current_cost
                        best_hash_index = hash_index

                if best_hash_index == invalid_assignment:
                    # If no starting point for the movement of the element has been found that means, that the element cannot be inserted therefore the function failed
                    # to insert at least this one key and returns False:
                    return -1
                else:
                    # If a starting point has been found the backtracking to an empty bucket starts:

                    # WARNING: Due to this algorithm in effect tracing the paths backward the terminology refers to
                    # elements/buckets which are closer to the open bucket as previous and those which are farther
                    # as next even though the movement in this part goes from an unassigned element to an open bucket.
                    first_bucket = (
                        hashes_forward(types.uint64(best_hash_index), key)[0]
                        % storage_count
                    )
                    current_bucket = first_bucket
                    finished_backtracking = False

                    while finished_backtracking == False:
                        current_prev_element = get_bits(
                            prev_element_array, prev_element_bits, current_bucket
                        )

                        # Checks if the current previous element does not exist and if the bucket is actually still open:
                        current_bucket_fill = get_bits(
                            bucket_fill_array, bucket_fill_bits, current_bucket
                        )

                        if (
                            current_prev_element == invalid_element
                            and current_bucket_fill < bucket_size
                        ):
                            # The bucket is open, therefore the assignements along the path get updated, as well as the element_moved flags and the bucket_fill:
                            finished_backtracking = True
                            set_bits(
                                bucket_fill_array,
                                bucket_fill_bits,
                                current_bucket,
                                current_bucket_fill + 1,
                            )

                            # First moves the unassigned element:
                            set_bits(
                                assignment_array,
                                assignment_bits,
                                index,
                                best_hash_index,
                            )
                            set_bits_fast(emba_array, emba_bits, index, 1)

                            # Then proceeds with the rest:
                            bt_current_bucket = first_bucket  # bt = backtrack
                            bt_next_element = index

                            while (
                                get_bits(
                                    prev_element_array,
                                    prev_element_bits,
                                    bt_current_bucket,
                                )
                                != invalid_element
                            ):
                                # Updates the key and hash for the outedge array:
                                outedge_index = get_bits(
                                    outedge_start_array,
                                    outedge_start_bits,
                                    bt_current_bucket,
                                )
                                bt_current_prev_element = get_bits(
                                    prev_element_array,
                                    prev_element_bits,
                                    bt_current_bucket,
                                )
                                prev_hash_for_outedge = get_bits(
                                    assignment_array,
                                    assignment_bits,
                                    bt_current_prev_element,
                                )

                                while (
                                    get_bits(
                                        outedge_element_array,
                                        outedge_element_bits,
                                        types.uint64(outedge_index),
                                    )
                                    != bt_next_element
                                ):
                                    # Finds the key in the outdeg array:
                                    outedge_index += 1

                                set_bits(
                                    outedge_element_array,
                                    outedge_element_bits,
                                    types.uint64(outedge_index),
                                    bt_current_prev_element,
                                )
                                set_bits(
                                    outedge_hash_array,
                                    outedge_hash_bits,
                                    types.uint64(outedge_index),
                                    prev_hash_for_outedge,
                                )

                                # Gets the next element, hash and bucket:
                                bt_current_prev_bucket_hash = get_bits(
                                    prev_bucket_array,
                                    prev_bucket_bits,
                                    bt_current_bucket,
                                )
                                bt_current_prev_bucket = (
                                    hashes_forward(
                                        bt_current_prev_bucket_hash,
                                        keys[bt_current_prev_element],
                                    )[0]
                                    % storage_count
                                )

                                # Updates the assignment and moved flag:
                                set_bits(
                                    assignment_array,
                                    assignment_bits,
                                    bt_current_prev_element,
                                    bt_current_prev_bucket_hash,
                                )
                                set_bits_fast(
                                    emba_array,
                                    emba_bits,
                                    bt_current_prev_element,
                                    1,
                                )

                                # Finally sets the next bucket as the current one:
                                bt_current_bucket = bt_current_prev_bucket
                                bt_next_element = bt_current_prev_element

                            # At the end it updates the outdeg data for the open bucket:
                            outedge_index = get_bits(
                                outedge_start_array,
                                outedge_start_bits,
                                bt_current_bucket,
                            )
                            bt_current_prev_element = get_bits(
                                prev_element_array,
                                prev_element_bits,
                                bt_current_bucket,
                            )
                            prev_hash_for_outedge = get_bits(
                                assignment_array,
                                assignment_bits,
                                bt_current_prev_element,
                            )

                            while (
                                get_bits(
                                    outedge_element_array,
                                    outedge_element_bits,
                                    types.uint64(outedge_index),
                                )
                                != bt_next_element
                            ):
                                # Finds the key in the outdeg array:
                                outedge_index += 1

                            set_bits(
                                outedge_element_array,
                                outedge_element_bits,
                                types.uint64(outedge_index),
                                bt_current_prev_element,
                            )
                            set_bits(
                                outedge_hash_array,
                                outedge_hash_bits,
                                types.uint64(outedge_index),
                                prev_hash_for_outedge,
                            )

                        else:
                            # The bucket is full, therefore the search continues:
                            if get_bits_fast(
                                emba_array, emba_bits, current_prev_element
                            ) == 1 or (
                                get_bits(
                                    prev_element_array,
                                    prev_element_bits,
                                    current_bucket,
                                )
                                == invalid_element
                                and current_bucket_fill >= bucket_size
                            ):
                                # If the element on the way of the key has been moved already this key could not be inserted in this pass and failed therefore:
                                finished_backtracking = True
                                failed_count += 1
                            else:
                                # If the element on the way of the key hasn't been moved the current_bucket gets updated and the search goes on:
                                current_prev_bucket_hash = get_bits(
                                    prev_bucket_array,
                                    prev_bucket_bits,
                                    current_bucket,
                                )
                                current_bucket = (
                                    hashes_forward(
                                        current_prev_bucket_hash,
                                        keys[current_prev_element],
                                    )[0]
                                    % storage_count
                                )

        # Returns the number of failed assignments for this pass:
        return failed_count

    @njit(
        boolean(
            storage_type,
            types.Array(numba.uint64, 1, "C"),
            UniTuple(uint64[:], config.value_module.parameter_count),
        ),
        locals=dict(
            int16_max=numba.uint64,
            invalid_assignment=numba.uint64,
            invalid_element=numba.uint64,
            storage_count=numba.uint64,
            insert_count=numba.uint64,
            bucket_size=numba.uint64,
            assignment_bits=numba.uint64,
            bucket_fill_bits=numba.uint64,
            outedge_start_bits=numba.uint64,
            outedge_element_bits=numba.uint64,
            outedge_hash_bits=numba.uint64,
            bucket_cost_bits=numba.uint64,
            prev_element_bits=numba.uint64,
            prev_bucket_bits=numba.uint64,
            emba_bits=numba.uint64,
            outedge_helper_bits=numba.uint64,
            failed_count=numba.int64,
            index=numba.uint64,
            hash_value=numba.uint64,
            rest=numba.uint64,
            key=numba.uint64,
            bucket_h1=numba.uint64,
            bucket_h2=numba.uint64,
            fill_state=numba.uint64,
            bucket_index=numba.uint64,
            in_bucket_index=numba.uint64,
            current_assignment=numba.uint64,
            first_prev=numba.uint64,
            end_prev=numba.uint64,
            prev_index=numba.uint64,
            prev_assignment=numba.uint64,
            prev_key=numba.uint64,
            prev_hash=numba.uint64,
            prev_assignment_bucket=numba.uint64,
            old_cost=numba.uint64,
            new_cost=numba.uint64,
            best_hash_index=numba.uint64,
            best_cost=numba.uint64,
            hash_index=numba.uint64,
            potential_bucket_index=numba.uint64,
            current_cost=numba.uint64,
            first_bucket=numba.uint64,
            current_bucket=numba.uint64,
            current_prev_element=numba.uint64,
            current_bucket_fill=numba.uint64,
            bt_current_bucket=numba.uint64,
            bt_next_element=numba.uint64,
            bt_current_prev_bucket_hash=numba.uint64,
            bt_current_prev_element=numba.uint64,
            bt_current_prev_bucket=numba.uint64,
            outedge_index=numba.uint64,
            prev_hash_for_outedge=numba.uint64,
        ),
    )
    def insert_cost_optimal(storage, keys, *values) -> bool:
        """
        Insert the given keys and values into the hash table.

        :param storage: the hash table's storage
        :param keys: the keys to insert
        :param values: the values for each keys
        :return: ``True`` if all keys could be inserted
        """

        # Gets the size of the keys and storage:
        insert_count = keys.shape[0]

        # Determines bit-length for those datatypes:
        assignment_bits = types.uint64(np.ceil(np.log2(hash_count + 1)))
        bucket_fill_bits = types.uint64(np.ceil(np.log2(bucket_size + 1)))
        outedge_start_bits = types.uint64(np.ceil(np.log2(hash_count * insert_count)))
        outedge_element_bits = types.uint64(np.ceil(np.log2(insert_count + 1)))
        outedge_hash_bits = types.uint64(np.ceil(np.log2(hash_count + 1)))
        bucket_cost_bits = 16
        prev_element_bits = types.uint64(np.ceil(np.log2(insert_count + 1)))
        prev_bucket_bits = types.uint64(np.ceil(np.log2(hash_count + 1)))
        emba_bits = 1

        # Reserves the memory for the helper arrays:
        assignment_array = np.zeros(
            calculate_array_size(assignment_bits, insert_count), dtype=np.uint64
        )
        bucket_fill_array = np.zeros(
            calculate_array_size(bucket_fill_bits, storage_count), dtype=np.uint64
        )
        outedge_start_array = np.zeros(
            calculate_array_size(outedge_start_bits, (storage_count + 1)),
            dtype=np.uint64,
        )
        outedge_element_array = np.zeros(
            calculate_array_size(outedge_element_bits, insert_count * hash_count),
            dtype=np.uint64,
        )
        outedge_hash_array = np.zeros(
            calculate_array_size(outedge_hash_bits, insert_count * hash_count),
            dtype=np.uint64,
        )
        bucket_cost_array = np.zeros(
            calculate_array_size(bucket_cost_bits, storage_count), dtype=np.uint64
        )
        prev_element_array = np.zeros(
            calculate_array_size(prev_element_bits, storage_count), dtype=np.uint64
        )
        prev_bucket_array = np.zeros(
            calculate_array_size(prev_bucket_bits, storage_count), dtype=np.uint64
        )
        emba_array = np.zeros(
            calculate_array_size(emba_bits, np.maximum(insert_count, storage_count)),
            dtype=np.uint64,
        )

        # 'Constants' that represent invalid numbers or indexes of some kind:
        invalid_assignment = types.uint64(
            hash_count
        )  # Uses the next number after the final hash-index as invalid
        invalid_element = types.uint64(
            insert_count
        )  # Same procedure as for the invalid-assignment except for the element

        # Initializes the assignements with invalid assignements:
        for index in range(insert_count):
            set_bits(
                assignment_array,
                assignment_bits,
                types.uint64(index),
                invalid_assignment,
            )

        # Initialisation passes:
        failed_count = do_initialization_passes(
            insert_count,
            assignment_array,
            assignment_bits,
            bucket_fill_array,
            bucket_fill_bits,
            keys,
            invalid_assignment,
            invalid_element,
        )

        # From now on every remaining key has to be inserted using the standard pass
        # so if none are remaining the function ends here:
        if failed_count == 0:
            # Finally uses the assignements to fill the table:
            set_table_content_from_assignment(
                insert_count, assignment_array, assignment_bits, keys, values, storage
            )

            return True

        # Otherwise it continues to attempt to insert the remaining keys:
        # For that it sets up the outedge arrays, 'abusing' the prev-element array as the helper array because it has the same structure as required for that job:
        setup_outedge_array(
            insert_count,
            assignment_array,
            assignment_bits,
            outedge_start_array,
            outedge_start_bits,
            outedge_element_array,
            outedge_element_bits,
            outedge_hash_array,
            outedge_hash_bits,
            keys,
            invalid_assignment,
            invalid_element,
        )

        # Now performs the standard passes until no insertions fail:
        while failed_count > 0:
            # Nothing failed in this pass thus far:
            failed_count = 0

            # Search phase:

            # First, sets all open buckets active and all remaining inactive as well as setting the active buckets to cost 0 and the inactive ones to cost int16 max:
            for index in range(storage_count):
                current_bucket_fill = get_bits(
                    bucket_fill_array, bucket_fill_bits, index
                )

                if current_bucket_fill < bucket_size:
                    set_bits_fast(emba_array, emba_bits, index, 1)
                    set_bits(bucket_cost_array, bucket_cost_bits, index, 0)
                else:
                    set_bits_fast(emba_array, emba_bits, index, 0)
                    set_bits(bucket_cost_array, bucket_cost_bits, index, int16_max)

            # Finally resets all the paths:
            for index in range(storage_count):
                set_bits(prev_element_array, prev_element_bits, index, invalid_element)
                set_bits(prev_bucket_array, prev_bucket_bits, index, invalid_assignment)

            # Now that the helper arrays are reset the search passes get executed:
            do_search_phase(
                insert_count,
                assignment_array,
                assignment_bits,
                bucket_cost_array,
                bucket_cost_bits,
                emba_array,
                emba_bits,
                outedge_start_array,
                outedge_start_bits,
                outedge_element_array,
                outedge_element_bits,
                outedge_hash_array,
                outedge_hash_bits,
                prev_element_array,
                prev_element_bits,
                prev_bucket_array,
                prev_bucket_bits,
                keys,
                invalid_assignment,
                invalid_element,
            )

            # Move phase:

            # First, the element moved array gets reset to False for everything:
            emba_array.fill(0)

            # Now moves the keys along the paths that were computed in the search phase:
            failed_count = do_move_phase(
                insert_count,
                assignment_array,
                assignment_bits,
                bucket_cost_array,
                bucket_cost_bits,
                bucket_fill_array,
                bucket_fill_bits,
                emba_array,
                emba_bits,
                outedge_start_array,
                outedge_start_bits,
                outedge_element_array,
                outedge_element_bits,
                outedge_hash_array,
                outedge_hash_bits,
                prev_element_array,
                prev_element_bits,
                prev_bucket_array,
                prev_bucket_bits,
                keys,
                invalid_assignment,
                invalid_element,
            )

            # If at least one of the keys did not have a single path the configuration does not allow for a valid assignment.
            # This is indicated by do-move-phase returning -1 and results in the returning of false from this method:
            if failed_count == -1:
                return False

        # Finally uses the assignements to fill the table:
        set_table_content_from_assignment(
            insert_count, assignment_array, assignment_bits, keys, values, storage
        )

        return True

    # Returns the compiled function:
    return insert_cost_optimal
