{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/1b750c41fc0cc7cef7992a150c05d11a15bd226a.tar.gz") {}, ...}:

let
  mach-nix = import (builtins.fetchGit {
    url = "https://github.com/DavHau/mach-nix/";
    ref = "refs/tags/3.1.1";
  }) {
    # optionally bring your own nixpkgs
    inherit pkgs;

    # optionally specify the python version
    python = "python38";

    # optionally update pypi data revision from https://github.com/DavHau/pypi-deps-db
    pypiDataRev = "f78b1f3d994e304ecb50316e996bf359c4eede19";
    pypiDataSha256 = "1pz7vg5vnrcpv30mjrcd0dqcgh2q3i4n8zqx2lnfq5ivcr4qc1cp";
  };
  dev-py = mach-nix.mkPython {  # replace with mkPythonShell if shell is wanted
    requirements = builtins.readFile ./requirements.txt;
    # setupttools-scm leads to an error when the stdist or wheel black is used
    _.black = pkgs.black;
    # providers = {
    #   _default = "wheel,sdist,nixpkgs";
    # };
  };
in
pkgs.mkShell {
  buildInputs = [
    pkgs.python38Packages.pip
    dev-py
  ];
  shellHook = ''
            export PIP_PREFIX="$(pwd)/.direnv/nix_pip"
            export PYTHONPATH="$(pwd)/.direnv/nix_pip/lib/python3.8/site-packages:$PYTHONPATH"
            export PATH="$PATH:$(pwd)/.direnv/nix_pip/bin"
            unset SOURCE_DATE_EPOCH
  '';
}