"""The module provides the main function for generating hash tables using GeCoCoo."""

from typing import Optional, Tuple, Union

import numpy as np

from .config import Config
from .config.lowlevel import LLConfig, resolve_config
from .interface import HashtableInterface, HashtableUtilities
from .table.insert_cost_optimal import define_insert_cost_optimal
from .table.insert_random_walk import define_insert_random_walk
from .table.iterator import define_debug_iterator, define_key_value_iterator
from .table.misc import define_clear, define_get_key_count
from .table.remove import define_remove
from .table.search import define_search
from .table.storage import initialize_storage


def generate_hashtable(
    config: Union[Config, LLConfig],
    override_n: Optional[int] = None,
    reuse_array: Optional[np.ndarray] = None,
) -> Tuple[HashtableInterface, HashtableUtilities]:
    """
    Generate a hash table based on the given configuration.

    :param config: the configuration to use
    :param override_n:
        override the number of keys specified in the configuration, defaults to None
    :param reuse_array: optionally an array to reuse for the hash table storage
    :return:
        a tuple consisting of

        - the hash table interface, usable in jit-compiled functions
        - the hash table utilities, **not** usable in jit-compiled functions
    """

    if isinstance(config, Config):
        # resolve the config to an LLConfig object
        llconfig = resolve_config(config, override_n)
    elif isinstance(config, LLConfig):
        # config is already an LLConfig object
        llconfig = config
    else:
        raise TypeError("type of config is invalid")

    storage, storage_type, access = initialize_storage(llconfig, reuse_array)
    insert_single = define_insert_random_walk(llconfig, storage_type, access)
    initialize = define_insert_cost_optimal(llconfig, storage_type, access)
    search_single = define_search(llconfig, storage_type, access)
    remove_single = define_remove(llconfig, storage_type, access)
    clear = define_clear(llconfig, storage_type, access)
    key_value_iterator = define_key_value_iterator(llconfig, storage_type, access)
    debug_iterator = define_debug_iterator(llconfig, storage_type, access)
    get_key_count = define_get_key_count(llconfig, storage_type, access)

    table = HashtableInterface(
        storage=storage,
        insert_single=insert_single,
        initialize=initialize,
        search_single=search_single,
        remove_single=remove_single,
        clear=clear,
        key_value_iterator=key_value_iterator,
        debug_iterator=debug_iterator,
        get_key_count=get_key_count,
    )
    utils = HashtableUtilities(llconfig, table)

    return table, utils
