import os
from io import StringIO

import pytest
from strictyaml import YAMLValidationError

from gecocoo.config.definition import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.config.file import load_from_yaml


@pytest.mark.parametrize(
    "input,config",
    [
        pytest.param(
            """
dimensions:
  k: 31
  bucket_size: 4
  load_factor: 0.98

values:
  module_name: "gecocoo.valuemodules.counting"
  parameters:
    - 32

hashfunctions:
  choices: 5
  default_hash: xor
  functions:
    - name: swaplinear
      parameters:
        - 23
        - 445
    - default
    - name: test3
      parameters:
    - name: xor
""",
            Config(
                bucket_size=4,
                k=31,
                load_factor=0.98,
                value_module=ValueModuleConfig(
                    module_name="gecocoo.valuemodules.counting", parameters=[32]
                ),
                hash_functions=[
                    HashFunction("swaplinear", [23, 445]),
                    HashFunction("xor", []),
                    HashFunction("test3", []),
                    HashFunction("xor", []),
                    HashFunction("xor", []),
                ],
            ),
            id="specific hash functions",
        ),
        (
            """
dimensions:
  k: 31
  bucket_size: 4
  load_factor: 0.98

values:
  module_name: "gecocoo.valuemodules.counting"

hashfunctions:
  choices: 2
  default_hash: xor
""",
            Config(
                bucket_size=4,
                k=31,
                load_factor=0.98,
                value_module=ValueModuleConfig(
                    module_name="gecocoo.valuemodules.counting", parameters=[]
                ),
                hash_functions=[
                    HashFunction("xor", []),
                    HashFunction("xor", []),
                ],
            ),
        ),
        pytest.param(
            """
dimensions:
  n: 12345678
  k: 31
  bucket_size: 4
  load_factor: 0.98

values:
  module_name: "gecocoo.valuemodules.counting"

hashfunctions:
  choices: 2
  default_hash: xor
""",
            Config(
                n=12345678,
                bucket_size=4,
                k=31,
                load_factor=0.98,
                value_module=ValueModuleConfig(
                    module_name="gecocoo.valuemodules.counting", parameters=[]
                ),
                hash_functions=[
                    HashFunction("xor", []),
                    HashFunction("xor", []),
                ],
            ),
            id="specific n",
        ),
        pytest.param(
            """
dimensions:
  n: 12345678
  k: 31
  bucket_size: 4
  load_factor: 0.98

values:
  module_name: "gecocoo.valuemodules.counting"

hashfunctions:
  choices: 2
  default_hash: xor

storage:
  concurrent: true
  mode: FAST
  disable_quotienting: true
""",
            Config(
                n=12345678,
                bucket_size=4,
                k=31,
                load_factor=0.98,
                value_module=ValueModuleConfig(
                    module_name="gecocoo.valuemodules.counting", parameters=[]
                ),
                hash_functions=[
                    HashFunction("xor", []),
                    HashFunction("xor", []),
                ],
                storage=StorageConfig(
                    mode=StorageMode.FAST, concurrent=True, disable_quotienting=True
                ),
            ),
            id="storage config",
        ),
    ],
)
def test_load_from_yaml(tmp_path, input: str, config: Config):
    with open(tmp_path / "config.yml", mode="w") as file:
        file.write(input)

    with open(tmp_path / "config.yml") as file:
        load_config = load_from_yaml(file)

    assert load_config == config


@pytest.mark.parametrize(
    "input,error_text",
    [
        pytest.param(
            """
dimensions:
  n: 12345678
  k: 25
  load_factor: 0.98

values:
  module_name: "gecocoo.valuemodules.counting"

hashfunctions:
  choices: 2
  default_hash: xor
""",
            "'bucket_size' not found",
            id="missing bucket size",
        ),
        pytest.param(
            """
dimensions:
  n: 12345678
  k: 25
  load_factor: 0.98
  bucket_size: 5

values:
  module_name: "gecocoo.valuemodules.counting"

hashfunctions:
  choices: 2
  default_hash: xor

storage:
  concurrent: true
  mode: INVALID
  disable_quotienting: true
""",
            "when expecting one of: PACKED, BUCKETS, FAST",
            id="invalid storage mode",
        ),
    ],
)
def test_load_from_yaml_fails(tmp_path, input: str, error_text: str):
    with open(tmp_path / "config.yml", mode="w") as file:
        file.write(input)

    with pytest.raises(YAMLValidationError, match=error_text) as error:
        with open(tmp_path / "config.yml") as file:
            load_from_yaml(file)
