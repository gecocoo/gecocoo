GeCoCoo API
===========

These pages document the entire API of GeCoCoo.
This also contains parts of the API surface that the user usually does not interact with.

.. toctree::
   :maxdepth: 5

   gecocoo/gecocoo