import matplotlib.pyplot as plt
import numpy as np
from matplotlib.testing.decorators import image_comparison

from gecocoo.interface import OccupancyData
from gecocoo.plotting import (
    plot_hash_usage,
    plot_hash_usage_total,
    plot_slot_usage,
    plot_slot_usage_total,
)

np.random.seed(1234)
SEGMENTS = 150
BUCKET_COUNT = 100000
BUCKET_SIZE = 4
HASH_COUNT = 5
DATA = OccupancyData(
    bucket_count=BUCKET_COUNT,
    bucket_size=BUCKET_SIZE,
    hash_count=HASH_COUNT,
    slot_usage=np.random.randint(
        0, BUCKET_COUNT // SEGMENTS, size=(SEGMENTS, BUCKET_SIZE), dtype=np.uint32
    ),
    hash_usage=np.random.randint(
        0, BUCKET_COUNT // SEGMENTS, size=(SEGMENTS, HASH_COUNT), dtype=np.uint32
    ),
)


@image_comparison(baseline_images=["slot_usage"], remove_text=True, extensions=["png"])
def test_plot_slot_usage():
    plot_slot_usage(DATA)


@image_comparison(
    baseline_images=["slot_usage_total"], remove_text=True, extensions=["png"]
)
def test_plot_slot_usage_total():
    plot_slot_usage_total(DATA)


@image_comparison(baseline_images=["hash_usage"], remove_text=True, extensions=["png"])
def test_plot_hash_usage():
    plot_hash_usage(DATA)


@image_comparison(
    baseline_images=["hash_usage_total"], remove_text=True, extensions=["png"]
)
def test_plot_hash_usage_total():
    plot_hash_usage_total(DATA)
