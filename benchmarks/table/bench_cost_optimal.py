import benchutils
import numpy as np
import pytest
from numba import njit

from gecocoo.config import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import generate_random_kmers, get_qualified_name
from gecocoo.valuemodules import keyset

ROUNDS = benchutils.apply_rounds_factor(10)
WARMUP_ROUNDS = benchutils.apply_rounds_factor(2, 0, 5)
SEED = 1234
KEY_COUNT = benchutils.apply_data_factor(10 ** 4, 1000)
STORAGE_CONFIGS = list(
    [
        StorageConfig(mode=StorageMode.PACKED),
        StorageConfig(mode=StorageMode.BUCKETS, concurrent=False),
        StorageConfig(
            mode=StorageMode.FAST, concurrent=False, disable_quotienting=True
        ),
    ]
)


@pytest.mark.parametrize("storage", STORAGE_CONFIGS)
def bench_insert_cost_optimal(benchmark, storage: StorageConfig):
    benchmark.group = "table storage modes (insert-cost-optimal)"
    benchmark.name = f"mode = {storage.mode}"

    config = Config(
        load_factor=0.95,
        k=32,
        bucket_size=4,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
        storage=storage,
    )

    (table, _) = generate_hashtable(config, KEY_COUNT)

    # Creates a copy of the key arrays for use in insert cost optimal:
    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 32 - 1, KEY_COUNT, dtype=np.uint64)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)

    def setup():
        table.clear(table.storage)

    @njit
    def work(table, keys):
        table.initialize(table.storage, keys)

    benchmark.pedantic(
        work,
        args=(table, keys),
        setup=setup,
        rounds=ROUNDS,
        warmup_rounds=WARMUP_ROUNDS,
    )
