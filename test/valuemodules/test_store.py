from functools import lru_cache
from typing import Dict

import hypothesis.strategies as st
from hypothesis.stateful import Bundle, RuleBasedStateMachine, initialize, rule

from gecocoo.config import Config, HashFunction, ValueModuleConfig
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.valuemodules import store

K = 32
MAX_KEY = 2 ** (2 * K) - 1
MAX_VALUE = 10 ** 5
SIZE = 10 ** 6

# Caches the generated table since the config stays the same.
# Before each test the table simply gets cleared and then reused.
# This greatly improves the test's execution time.
@lru_cache()
def get_table():
    config = Config(
        load_factor=0.95,
        k=K,
        bucket_size=4,
        value_module=ValueModuleConfig(module_name=get_qualified_name(store)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
    )

    return generate_hashtable(config, SIZE)[0]


class StoreValueModuleComparsion(RuleBasedStateMachine):
    def __init__(self):
        super(StoreValueModuleComparsion, self).__init__()
        self.model: Dict[int, int] = dict()
        self.table = get_table()

    keys = Bundle("keys")
    values = Bundle("values")

    @initialize()
    def init_table(self):
        self.table.clear(self.table.storage)

    @rule(target=keys, key=st.integers(min_value=0, max_value=MAX_KEY))
    def gen_key(self, key):
        return key

    @rule(target=values, value=st.integers(min_value=0, max_value=MAX_VALUE))
    def gen_value(self, value):
        return value

    @rule(key=keys, value=values)
    def insert(self, key, value):
        self.table.insert_single(self.table.storage, key, value)
        self.model[key] = value

    @rule(key=keys)
    def remove(self, key):
        self.table.remove_single(self.table.storage, key)
        self.model.pop(key, None)

    @rule(key=keys)
    def check_value_for_key(self, key):
        expected_value = self.model.get(key)
        table_value = self.table.search_single(self.table.storage, key)
        assert expected_value == table_value

    @rule()
    def check_iterator(self):
        entries = list(self.table.key_value_iterator(self.table.storage))
        assert len(self.model) == len(entries)
        for k, v in entries:
            expected_value = self.model.get(k, None)
            assert expected_value == v


TestStoreValueModule = StoreValueModuleComparsion.TestCase
