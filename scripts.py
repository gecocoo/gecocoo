#!/usr/bin/env python3

"""
Collection of useful scripts for development and testing.

Run from the command line with the script's name you want to run as the first argument.
"""

import inspect
import os
import subprocess
import sys
from typing import Callable, Dict

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
VENV_PYTHON = os.path.join(PROJECT_ROOT, ".venv", "bin", "python")


def _get_scripts() -> Dict[str, Callable]:
    return dict(
        [
            (name.replace("_", "-"), obj)
            for name, obj in inspect.getmembers(sys.modules[__name__])
            if (
                inspect.isfunction(obj)
                and not name.startswith("_")
                and obj.__module__ == __name__
            )
        ]
    )


def _venv_bin(name: str) -> str:
    return os.path.join(PROJECT_ROOT, ".venv", "bin", name)


def help():
    """Show this summary of the available scripts."""

    scripts = _get_scripts()
    max_len_name = max(map(len, scripts.keys()))
    print("Available scripts:\n")
    for name in sorted(scripts.keys()):
        function = scripts.get(name)
        docstring = getattr(function, "__doc__") or "(no description)"
        print(name.ljust(max_len_name), "-", docstring)


def format():
    """Format all python code (using black and isort)"""

    subprocess.run([_venv_bin("black"), "."], check=True)
    subprocess.run([_venv_bin("isort"), "."], check=True)


def docs_build(*args):
    """Build the documentation."""

    subprocess.run(
        [
            _venv_bin("sphinx-build"),
            "source",
            "build",
            "-E",  # clean build docs
            *args,
        ],
        cwd=os.path.join(PROJECT_ROOT, "docs"),
        check=True,
    )
    print("Open at", os.path.join(PROJECT_ROOT, "docs/build/index.html"))


def docs_watch():
    """Watch for changes and build documentation."""

    subprocess.run(
        [
            _venv_bin("sphinx-autobuild"),
            "source",
            "build",
            "--watch",
            os.path.join(PROJECT_ROOT, "gecocoo"),
        ],
        cwd=os.path.join(PROJECT_ROOT, "docs"),
        check=True,
    )


def setup_dev():
    """Setup the development environment."""

    import venv

    venv_dir = os.path.join(PROJECT_ROOT, ".venv")
    venv.main([venv_dir])
    subprocess.run(
        [
            VENV_PYTHON,
            "-m",
            "pip",
            "install",
            "-r",
            os.path.join(PROJECT_ROOT, "requirements.txt"),
            "--upgrade",
        ],
        check=True,
    )
    subprocess.run(
        [
            os.path.join(venv_dir, "bin/pip"),
            "install",
            "-e",
            PROJECT_ROOT,
        ],
        check=True,
    )

    print("Created virtual environment and installed packages")


def test_examples():
    """Test the scripts in the examples directory."""

    from timeit import default_timer as timer

    examples_path = os.path.join(PROJECT_ROOT, "examples")
    files = list(filter(lambda f: f.endswith(".py"), os.listdir(examples_path)))
    print(f"Found {len(files)} scripts in '{examples_path}'")

    failed = []

    for name in files:
        file = os.path.join(examples_path, name)
        print(f"Running {file}")

        start = timer()
        process = subprocess.run([VENV_PYTHON, file])
        end = timer()
        print(f"Took {end - start:.2f} seconds")

        if process.returncode != 0:
            print(f"Script failed with return code {process.returncode}")
            failed.append(file)

    if len(failed) == 0:
        print("All scripts ran successfully!")
    else:
        print(f"{len(failed)} script(s) failed:")
        for file in failed:
            print(f"- {file}")
        exit(1)


def test_ci_job(job_name: str):
    """Run a GitLab runner for the specific job locally (requires docker)"""

    cache_dir = os.path.join(PROJECT_ROOT, ".gitlab-cache")
    os.makedirs(cache_dir, exist_ok=True)
    print("Start GitLab runner for job", job_name)
    subprocess.run(
        [
            "docker",
            "run",
            "--rm",
            "-it",
            "--workdir",
            PROJECT_ROOT,
            "-v",
            "/var/run/docker.sock:/var/run/docker.sock",
            "-v",
            f"{PROJECT_ROOT}:{PROJECT_ROOT}",
            "gitlab/gitlab-runner:alpine",
            "exec",
            "docker",
            "--cache-dir=/tmp/gitlab-cache",
            "--docker-cache-dir=/tmp/gitlab-cache"
            f"--docker-volumes={cache_dir}:/tmp/gitlab-cache",
            "--docker-privileged",
            "--docker-pull-policy=if-not-present",
            "--docker-volumes=/var/run/docker.sock:/var/run/docker.sock",
            job_name,
        ],
        check=True,
    )


def test_tests(*args):
    """Run all tests in "test" directory with coverage reporting"""

    subprocess.run(
        [
            _venv_bin("pytest"),
            # Name the 20 slowest tests
            "--durations=20",
            "--benchmark-skip",
            "--junitxml=build/report.xml",
            "--workers=4",
            # Report coverage
            "--cov=gecocoo",
            "--cov-report=xml:build/coverage.xml",
            "--cov-report=html:build/coverage",
            # Append remaining arguments
            *args,
        ],
        check=True,
    )

    subprocess.run([_venv_bin("coverage"), "report"], check=True)


def check_docstrings():
    """Check for missing or wrong docstrings."""

    subprocess.run(
        [
            VENV_PYTHON,
            "-m",
            "pydocstyle",
            os.path.join(PROJECT_ROOT, "gecocoo"),
            "--count",
            "--ignore=D203,D212,D202",
        ],
        check=True,
    )


def check_formatting():
    """Check formatting (using black)."""

    subprocess.run(
        [
            VENV_PYTHON,
            "-m",
            "black",
            PROJECT_ROOT,
            "--check",
        ],
        check=True,
    )


def check_imports():
    """Check imports order (using isort)."""

    subprocess.run(
        [
            VENV_PYTHON,
            "-m",
            "isort",
            PROJECT_ROOT,
            "--check",
        ],
        check=True,
    )


if __name__ == "__main__":
    if len(sys.argv) < 2:
        help()
    else:
        scripts = _get_scripts()
        script_name = sys.argv[1]
        if script_name in scripts.keys():
            try:
                scripts.get(script_name)(*sys.argv[2:])
            except subprocess.CalledProcessError as e:
                command = " ".join(e.cmd)
                print(f"{command} exited with code {e.returncode}")
                exit(e.returncode)
        else:
            print(f"script {script_name} was not found")
            exit(1)
