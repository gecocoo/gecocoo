"""The package contains the base class for value modules using classes and provides some read to use value modules."""

from .baseclass import ValueModuleBase
from .default import counting, keyset, store
