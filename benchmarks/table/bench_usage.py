import benchutils
import numpy as np
import pytest
from numba import njit

from gecocoo.config import Config, HashFunction, ValueModuleConfig
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import generate_random_kmers, get_qualified_name
from gecocoo.valuemodules import keyset

ROUNDS = benchutils.apply_rounds_factor(10)
WARMUP_ROUNDS = benchutils.apply_rounds_factor(2, 0, 5)
SEED = 1234
KEY_COUNT = benchutils.apply_data_factor(10 ** 5, 1000)


def using_named_tuple(table, keys):
    """
    Use the hash table directly by using the attributes of the interface named tuple.
    """

    @njit
    def my_insert(table, key):
        table.insert_single(table.storage, key)

    @njit
    def work(table):
        for k in keys:
            my_insert(table, k)

    return lambda: work(table)


def using_variables(table, keys):
    """
    Use the hash table by destructing the interface in the named tuple.
    Functions are then called from variables instead of using an attribute of the tuple.
    """
    insert_single = table.insert_single

    @njit
    def my_insert(storage, key):
        insert_single(storage, key)

    @njit
    def work(storage):
        for k in keys:
            my_insert(storage, k)

    return lambda: work(table.storage)


@pytest.mark.parametrize(
    "usage_method, name",
    [
        pytest.param(using_named_tuple, "using named tuple"),
        pytest.param(using_variables, "using variables"),
    ],
)
def bench_bucket_size(benchmark, usage_method, name):
    """
    Benchmark different usage methods of the hash table to see which one is preferable for maximum performance.
    """
    benchmark.group = "usage methods"
    benchmark.name = name

    config = Config(
        load_factor=0.95,
        k=31,
        bucket_size=4,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
    )
    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 32 - 1, KEY_COUNT)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)
    (table, _) = generate_hashtable(config, int(KEY_COUNT * 1.5))
    work = usage_method(table, keys)

    def setup():
        table.clear(table.storage)

    benchmark.pedantic(work, setup=setup, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS)
