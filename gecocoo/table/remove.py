"""The module contains the hash table's removal function."""

from functools import lru_cache
from typing import Any, Callable

from numba import njit, uint64
from numba.types import Type, UniTuple

from ..config.lowlevel import LLConfig
from .codegen_utils import make_call_by_index
from .storage import StorageAccess


@lru_cache(maxsize=5)
def define_remove(
    config: LLConfig, storage_type: Type, access: StorageAccess
) -> Callable[[Any, int], Any]:
    """
    Generate a function that removes a single key with its associated value from the hash table.

    :param config: the hash table configuration
    :param storage_type: the type of the hash table's storage object
    :param access: the access abstraction object for the hash table
    :return: the generated function
    """

    bucket_count = config.num_buckets
    bucket_size = config.bucket_size
    hash_forward = make_call_by_index(
        UniTuple(uint64, 2)(uint64), [h.forward for h in config.hash_functions]
    )
    hash_function_count = len(config.hash_functions)
    map_value = config.value_module.map_value

    @njit((storage_type, uint64))
    def remove(storage, key: uint64):
        # Keep trying to remove the key as long as we locate the key
        # or fail to clear the entry.
        found_relocating = True
        while found_relocating:
            found_relocating = False
            for hash_index in range(hash_function_count):
                hash, quotient = hash_forward(hash_index, key)
                index = hash % bucket_count
                for slot in range(bucket_size):
                    match, relocating, value = access.compare_entry(
                        storage, index, slot, hash_index, quotient
                    )
                    found_relocating |= relocating

                    # Only attempt the removal if the key is not being relocated
                    if match and not relocating:
                        if access.clear_entry(
                            storage,
                            index,
                            slot,
                            False,  # relocating
                            hash_index,
                            quotient,
                            value,
                        ):
                            access.dec_key_count(storage)
                            return map_value(key, value)

        # key not found
        return None

    return remove
