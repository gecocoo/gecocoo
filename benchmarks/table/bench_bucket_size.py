import benchutils
import numpy as np
import pytest
from numba import njit, uint64

from gecocoo.config import Config, HashFunction, ValueModuleConfig
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import generate_random_kmers, get_qualified_name
from gecocoo.valuemodules import keyset

ROUNDS = benchutils.apply_rounds_factor(20)
WARMUP_ROUNDS = benchutils.apply_rounds_factor(5, 0, 10)
SEED = 1234
KEY_COUNT = benchutils.apply_data_factor(10 ** 6, 10000)
BUCKET_SIZES = [2, 4, 8]


@pytest.mark.parametrize("bucket_size", BUCKET_SIZES)
def bench_bucket_size_insert(bucket_size, benchmark):
    benchmark.group = "table bucket sizes (insert)"
    benchmark.name = f"size = {bucket_size}"

    config = Config(
        load_factor=0.95,
        k=31,
        bucket_size=bucket_size,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
    )
    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 31 - 1, KEY_COUNT)
    keys = generate_random_kmers(31, KEY_COUNT, SEED, None)
    (table, _) = generate_hashtable(config, int(KEY_COUNT * 1.5))

    def setup():
        table.clear(table.storage)

    @njit
    def work(table):
        for k in keys:
            table.insert_single(table.storage, k)

    benchmark.pedantic(
        work, args=(table,), setup=setup, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS
    )


@pytest.mark.parametrize("bucket_size", BUCKET_SIZES)
def bench_bucket_size_search(bucket_size, benchmark):
    benchmark.group = "table bucket sizes (search)"
    benchmark.name = f"size = {bucket_size}"

    config = Config(
        load_factor=0.95,
        k=31,
        bucket_size=bucket_size,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
    )
    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 31 - 1, KEY_COUNT)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)
    (table, _) = generate_hashtable(config, int(KEY_COUNT * 1.5))

    @njit
    def setup(table):
        for k in keys:
            table.insert_single(table.storage, k)

    # insert all keys once
    setup(table)

    @njit
    def work(table):
        sum = uint64(0)
        for k in keys:
            sum += table.search_single(table.storage, k)
        return sum

    benchmark.pedantic(work, args=(table,), rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS)
