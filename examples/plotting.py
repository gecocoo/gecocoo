import numpy as np

from gecocoo.config import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.valuemodules import counting

concurrent = True
k = 16
config = Config(
    load_factor=0.95,
    k=k,
    bucket_size=4,
    value_module=ValueModuleConfig(get_qualified_name(counting)),
    hash_functions=[
        HashFunction(get_qualified_name(xorlinear)),
        HashFunction(get_qualified_name(xorlinear)),
        HashFunction(get_qualified_name(xorlinear)),
        HashFunction(get_qualified_name(xorlinear)),
    ],
    storage=StorageConfig(
        mode=StorageMode.BUCKETS,
        concurrent=concurrent,
    ),
)

print("generating table")
keys = np.random.randint(0, 2 ** (2 * k), 100000)
table, utils = generate_hashtable(config, len(keys))

from numba import njit, prange


@njit(parallel=concurrent)
def insert(table):
    for i in prange(len(keys)):
        table.insert_single(table.storage, keys[i])


print("inserting keys")
insert(table)

# Split the table in 100 segments and count occupancies
occupancy = utils.occupancy(100)

# Plot the occupancy data
from gecocoo.plotting import (
    plot_hash_usage,
    plot_hash_usage_total,
    plot_slot_usage,
    plot_slot_usage_total,
)

fig_hash_usage = plot_hash_usage(occupancy)
fig_slot_usage = plot_slot_usage(occupancy)
fig_hash_usage_total = plot_hash_usage_total(occupancy)
fig_slot_usage_total = plot_slot_usage_total(occupancy)

# Save figures to files
fig_hash_usage.savefig("hash_usage.png", dpi=500)
fig_slot_usage.savefig("slot_usage.png", dpi=500)
fig_hash_usage_total.savefig("hash_usage_total.png", dpi=500)
fig_slot_usage_total.savefig("slot_usage_total.png", dpi=500)

# Uncomment to show plots in a window
# import matplotlib.pyplot as plt
# plt.show()
