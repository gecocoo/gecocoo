"""The module provides some helper for working with the cost optimal initialization of hash tables."""

import numpy as np


def define_extract_unique_key_value_pairs(value_policy):
    """
    Generate a function that takes key and value arrays as parameters and returns a tuple containing all those while eliminating any duplicate keys and merging their values together using the value policy.

    :param value_policy: A function that takes two integers as parameters which represent the two values of a pair of detected identical keys and returns a single integer that is going to be saved with the key instead of the two given values.
    :return: the generated function
    """

    def extract_unique_key_value_pairs(keys, values):
        """
        Take two equally sized key and value arrays as parameters and return a tuple containg the same keys while removing any duplicates and merging their values using the value_policy function.

        :param keys: The keys for which the duplicates are supposed to get removed from.
        :param values: The values that belong to the keys in keys.
        :return: A tuple containing the arrays with the unique keys and values.
        """
        # First uses the numpy argsort algorithm to determine the indices the keys would hav to be at to be sorted:
        index_array = np.argsort(keys)
        key_count = keys.shape[0]

        # Creates new key and value arrays which are sorted by the keys:
        sorted_keys = np.zeros(key_count, dtype=np.uint64)
        sorted_values = np.zeros(key_count, dtype=np.uint64)

        for index in range(key_count):
            sorted_index = index_array[index]

            sorted_keys[index] = keys[sorted_index]
            sorted_values[index] = values[sorted_index]

        # Extracts all the unique keys from the array and merges the values together using the value_policy function:

        # The first key definitley did not appear before:
        last_unique_index = 0

        # Iterates over the rest:
        for index in range(1, key_count):
            if sorted_keys[index] == sorted_keys[last_unique_index]:
                # A duplicate has been found, proceed with merge:
                sorted_values[last_unique_index] = value_policy(
                    sorted_values[last_unique_index], sorted_values[index]
                )
            else:
                # A new unique key has been found, update index and copy the current key and value there:
                last_unique_index += 1

                sorted_keys[last_unique_index] = sorted_keys[index]
                sorted_values[last_unique_index] = sorted_values[index]

        # There are last_unique_index + 1 unique keys:
        unique_count = last_unique_index + 1

        # Create arrays with the necessary size for the unique keys and values and copy them over:
        unique_keys = np.zeros(unique_count, dtype=np.uint64)
        unique_key_values = np.zeros(unique_count, dtype=np.uint64)

        for index in range(unique_count):
            unique_keys[index] = sorted_keys[index]
            unique_key_values[index] = sorted_values[index]

        # Returns the unique key value array tuple:
        return (unique_keys, unique_key_values)

    # Returns the generated function:
    return extract_unique_key_value_pairs
