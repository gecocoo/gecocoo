# Benchmarking GeCoCoo

Benchmarks are implemented using pytest and the plugin pytest-benchmark. This enables easiliy written benchmarks without much ceremonial code.
The plugin able to write nicely formatted results to stdout ans also able to create good looking plots as `.svg` files.

See:

- `pytest`: https://docs.pytest.org/en/stable/contents.html#toc
- `pytest-benchmark`: https://pytest-benchmark.readthedocs.io/en/latest/

## Running Benchmarks

- Run a specfic benchmark
  - all benchmarks in a file: e.g. `pytest benchmarks/numba/bench_function_calls.py`
  - only one benchmark function in a file: e.g. `pytest benchmarks/numba/bench_function_calls.py::bench_calls`
- Saving results
  - `pytest-benchmark` saves the benchmarks in a `.json` file in the directory `.benchmarks`
  - Running with auto save (e.g. `pytest benchmarks/numba --benchmark-autosave`) saves the results in a file named something like `xxxx_<commit-id>_yyyy.json`
  - Explicitly naming a file (e.g. `pytest benchmarks/numba --benchmark-save="before my change"`) saves the result in a file named something like `xxxx_before my change.json`
- Plotting results
  - Make sure to follow the installation instrcutions at https://pytest-benchmark.readthedocs.io/en/latest/comparing.html?highlight=plotting#plotting
  - Create a plot for a single benchmark by adding the option `--benchmark-histogram` like `pytest benchmarks/numba/bench_array.py --benchmark-histogram`.
  - By default the plotted graphs will be named something like `benchmark_<date>_<time>-<group>.svg`.
  - You can also specify a custom prefix using `--benchmark-histogram=myprefix`.
- Comparing results
  - The CLI provided by `pytest-benchmark` can be used to compare benchmarks. For this call `pytest-benchmark compare <results...>` and pass the benchmarks you want to compare.
    - specify benchmarks by prefix like `0003`
    - specify benchmarks by file name like `.benchmarks/Linux-CPython-3.8-64bit/0001_foo.json`
    - specify by glob using asterisk like `*before my change`
  - You can also plot the multiple results like this: `pytest-benchmark compare "*before" "*after" --histogram`

## Structure

- `numba`: benchmarks for testing how numba compiles specific things
- `table`: benchmarks for performance of the hash table testing different parameters and options
- `hashing`: benchmarks for hash functions

More (sub-)directories can be added as needed when neccessary.

## Writing Benchmarks

### Conventions

- File pattern: `bench_xxx.py`
- Place in correct directory (see [Structure](#Structure))
- Name functions with prefix `bench_`: `def bench_function_call(benchmark):`

### Parameters

- There are a number that parameters that can be specified for each benchmark
- Target function (required)
  - The actual function to be benchmarked
  - Make sure not to do any setup in here that should not be measured with the benchmark
- Setup function (optional)
  - called before each round
  - can be used to clear an array or reset other stateful variables
  - not counted towards measured time
- Rounds
  - number of times to repeat a function call to compute mean time and stddev
  - prefer a high number while still keeping benchmark runtime low
- Iterations
  - should mostly be set to `1` (the default)
  - prefer adding a simple loop to the actual function like `for _ in range(10000)` when benchmarking jitted code
- Warmup rounds
  - in some cases very important because of jitted functions
  - setting to `5` rounds should be a good value in most cases
- Names for the benchmark
  - Setting `benchmark.group` should be set for each benchmark to group each variation of the benchmark
  - Setting `benchmark.name` should be set for each varation as a description
- Use `@pytest.mark.parametrize` to supply multiple benchmark variations to the benchmark function

### Generating data

- Generate random date using NumPy for quick access in jitted functions
- Use a fixed seed to make each benchmark reproducible and comparable.
- Example:
  ```python
  rng = np.random.RandomState(SEED)
  indexes = rng.randint(0, CAPACITY, COUNT)
  ```

### Optional

- Additional tests inside the benchmark file
  - Test the benchmarked implementations are actually equivalent from the outside
  - Nice use case for property based testing (Hypothesis)
- A main block inside a benchmark file can be used for some useful purposes:
  - Use for printing out information regarding the benchmark
  - Dumping LLVM code
  - Print memory usage

### Example

```python
import numpy as np
import pytest

SEED = 1234
COUNT = 10 ** 5

@pytest.mark.parametrize(
    "function,name", [
      pytest.param(function1, "function 1"),
      pytest.param(function2, "function 2"),
      pytest.param(function3, "function 3"),
    ]
)
def bench_my_function(benchmark, function, name):
    # set group and name for benchmark
    benchmark.group = "different function implementations"
    benchmark.name = name

    # generate some data
    rng = np.random.RandomState(SEED)
    data = rng.randint(0, 99999, COUNT)

    # setup data structure once
    array = np.zeros(COUNT, dtype=np.uint64)

    # reset some data structure before each run
    def setup():
        array.fill(0)

    # do something
    @njit
    def work(array):
        for x in data:
            function(array, x)

    benchmark.pedantic(work, 
      args=(array,), # pass some arguments to the work function
      setup=setup, 
      rounds=20, 
      warmup_rounds=5)
```
