import os

from gecocoo.config.file import load_from_yaml
from gecocoo.generator import generate_hashtable

config_path = os.path.join(os.path.dirname(__file__), "example_config.yaml")

if __name__ == "__main__":
    with open(config_path) as file:
        config = load_from_yaml(file)

    table, utils = generate_hashtable(config, 100000)

    table.insert_single(table.storage, 200, 1234)
    assert table.search_single(table.storage, 200) == 1234
