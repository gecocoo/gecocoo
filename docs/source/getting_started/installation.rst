Installation
============
Currently there is only one way to install the Package.

Manually using pip
------------------
Download/Clone the source and install it using:
::
    pip install /path/to/source
