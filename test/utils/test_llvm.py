import numpy as np
import pytest

from gecocoo.interface import OccupancyData
from gecocoo.utils.llvm import (
    cmp_xchg_array_64,
    cmp_xchg_array_128,
    make_address_of_jitclass_attr,
)


def test_cmp_xchg_array_64():
    arr = np.zeros(10, dtype=np.uint64)
    arr[7] = 3435
    assert cmp_xchg_array_64(arr, 7, 3435, 111) == True
    assert arr[7] == 111


def test_cmp_xchg_array_128():
    arr = np.zeros(10, dtype=np.uint64)
    arr[6] = 28346934
    arr[7] = 3435
    assert cmp_xchg_array_128(arr, 6, 28346934, 3435, 111, 222) == True
    assert arr[6] == 111
    assert arr[7] == 222


def test_cmp_xchg_array_128_unaliggned():
    arr = np.zeros(10, dtype=np.uint64)

    with pytest.raises(Exception):
        cmp_xchg_array_128(arr, 7, 3435, 0, 111, 0)


import numba


@numba.experimental.jitclass(
    [("test1", numba.types.uint64), ("test2", numba.types.uint32)]
)
class JitClassTest:
    def __init__(self):
        self.test1 = 1234
        self.test2 = 5678


def test_make_address_of_jitclass_attr():
    address_of = make_address_of_jitclass_attr(JitClassTest.class_type, "test2")
    instance = JitClassTest()

    @numba.njit
    def test(x):
        return address_of(x)

    assert test(instance) > 0


def test_make_address_of_jitclass_attr_requires_jitclass():
    with pytest.raises(TypeError):
        make_address_of_jitclass_attr(OccupancyData, "bucket_count")


def test_make_address_of_jitclass_attr_requires_valid_attribute():
    with pytest.raises(ValueError):
        make_address_of_jitclass_attr(JitClassTest.class_type, "foo")
