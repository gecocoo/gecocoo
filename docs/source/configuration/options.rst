Options
=======

The following list shows all options that allow the hash table to be adapted to the needs of the respective application.

:attr:`~gecocoo.config.definition.Config.n`
    The number of unique keys that the hash table should be able to hold.

:attr:`~gecocoo.config.definition.Config.load_factor`
    The load factor the hash table should have when full :attr:`0.0 < load < 1.0`.
    The resulting capacity will be **N = ceil(n/load)** and the
    resulting number of buckets will be **>= ceil(N/bucketsize)**.

:attr:`~gecocoo.config.definition.Config.bucket_size`
    The bucket size of the hash table :attr:`1 <= bucketsize <= 65535`.

:attr:`~gecocoo.config.definition.Config.k`
    The k-mer size of the key :attr:`1 <= k <= 32`.
    The resulting key size will be  **2*k bits**.

:attr:`~gecocoo.config.definition.Config.value_module`
    List of value modules to be used in the hash table.

    :attr:`~gecocoo.config.definition.ValueModuleConfig.module_name`
        Specifies value module as a fully qualified name to the value module's constructor.
    :attr:`~gecocoo.config.definition.ValueModuleConfig.parameters`
        Optional parameters for creating an instance of the value module.

:attr:`~gecocoo.config.definition.Config.hash_functions`
    List of hash functions to be used in the hash table.

    :attr:`~gecocoo.config.definition.HashFunction.name`
        Specifies hash function as a fully qualified name to the hash function's constructor
        (example: ``"gecocoo.hashing.swaplinear"``).
    :attr:`~gecocoo.config.definition.HashFunction.parameters`
        Optional parameters for creating an instance of the hash function.

:attr:`~gecocoo.config.definition.Config.storage`
    :attr:`~gecocoo.config.definition.StorageConfig.mode`
        The basic storage mode. The default mode is :attr:`BUCKETS`.

        :attr:`PACKED` Storage mode that `packs` all the hash table entries in bit precise fields and therefore wasting as little space as possible.
        The memory efficiency comes with the cost of worse data alignment and more bit operation having to be performed to extract the values.

        :attr:`BUCKETS` Storage mode that tries to balance memory efficiency and acessing performance by aligning each bucket on 64 bit addresses.
        This improves the acessing performance while still saving some memory with packing the slots tightly per bucket.

        :attr:`FAST`  Storage mode that takes favors performance over memory efficiency by having better alignment and performaning fewer bit operations.
        It also requires disabling any quotienting done by the chosen hash functions and thereby also skipping the key restoring other storage modes do.

    :attr:`~gecocoo.config.definition.StorageConfig.concurrent`
        Concurrency support on the hash table.
        Should be disabled if only single threaded access is required (default :attr:`False`).

        :attr:`True` concurrency support enabled.

        :attr:`False` concurrency support disabled.

    :attr:`~gecocoo.config.definition.StorageConfig.disable_quotienting`
        Disables any quotienting the chosen hash function might do, thus
        the full key has to be stored in the hash table (default :attr:`False`).

        :attr:`True` quotienting disabled.

        :attr:`False` quotienting enabled.

    :attr:`~gecocoo.config.definition.StorageConfig.disable_occupied_flag`
        :attr:`True` to disable the one bit per entry which would mark a slot as used.
        Then encode the information as key_part=0 and store items with the actual key_part 0 in a seperat location (default :attr:`False`).
        Currently this can only be used in BUCKETS StorageMode.
