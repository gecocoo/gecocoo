from numba import byte, njit, uint64
from numba.types import UniTuple

from gecocoo.config import Config, HashFunction, ValueModuleConfig
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.valuemodules import ValueModuleBase

# There are two ways to define value modules: using functions or using classes
# Both have the same features, it's just a matter of preference.

# The following defines a value module for counting keys.
# Each time a key is inserted, its counter is incremented.
# Parameters for the constructor function are taken from the config.
def counting(bits=32, initial_count=1, increment=1):
    # first_insert is called when a key is first inserted.
    # Note how we only take the key as an argument, as for counting we don't need anymore information.
    @njit(uint64(uint64))
    def first_insert(key):
        return initial_count

    # update_value is called when a key is inserted again.
    # The difference to first_insert is that it also given the currently stored value.
    # In this case we simply increment the stored value.
    @njit(uint64(uint64, uint64))
    def update_value(key, stored_value):
        return stored_value + increment

    # map_value is called to map the stored value for the result of searching in the table.
    # In many cases it can probably just return the stored value. You can find an example of a more advanced usage below.
    @njit
    def map_value(key, stored_value):
        return stored_value

    return (
        first_insert,
        update_value,
        map_value,
        bits,
    )


def show_counting():

    # For the table's config we need the value module's name.
    # The function gecocoo.helper.get_qualified_name(...) can be used for that purpose.
    # You could also specify the module name directly as string like "gecocoo.valuemodules.counting"
    module_name = get_qualified_name(counting)

    # define a config for the table
    config = Config(
        load_factor=0.95,
        k=32,
        bucket_size=4,
        value_module=ValueModuleConfig(module_name=module_name),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
    )

    (table, _) = generate_hashtable(config, 10 ** 6)

    # insert key for the first time
    table.insert_single(table.storage, 1234)
    assert table.search_single(table.storage, 1234) == 1

    # insert key four more times
    table.insert_single(table.storage, 1234)
    table.insert_single(table.storage, 1234)
    table.insert_single(table.storage, 1234)
    table.insert_single(table.storage, 1234)
    assert table.search_single(table.storage, 1234) == 5

    # we haven't inserted this key
    assert table.search_single(table.storage, 999) is None


# The following shows how to use the hash table as a key value store.
# For this purpose we define a new value module as a class.
class StoreValueModule(ValueModuleBase):
    def __init__(self, bits=32):
        self.bits = bits

    def make_first_insert(self):
        @njit(uint64(uint64, uint64))
        def first_insert(key, value):
            print("inserting", key, value)
            return value

        return first_insert

    def make_update_value(self):
        @njit(uint64(uint64, uint64, uint64))
        def update_value(key, stored_value, value):
            print("updating", key, stored_value, value)
            return value

        return update_value

    def make_map_value(self):
        return njit(lambda k, v: v)

    @property
    def value_bits(self):
        return self.bits


def show_store():

    module_name = get_qualified_name(StoreValueModule)

    # define a config for the table
    config = Config(
        load_factor=0.95,
        k=32,
        bucket_size=4,
        value_module=ValueModuleConfig(module_name=module_name),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
    )

    (table, _) = generate_hashtable(config, 10 ** 6)

    # insert key and its value
    table.insert_single(table.storage, 1234, 5678)
    assert table.search_single(table.storage, 1234) == 5678

    # update the key's value
    table.insert_single(table.storage, 1234, 4321)
    assert table.search_single(table.storage, 1234) == 4321

    # calling insert_single with too few arguments results in a TypeError
    try:
        table.insert_single(table.storage, 543221)
    except TypeError:
        pass
    else:
        assert False, "expected exception not thrown"

    # so does calling with too many
    try:
        table.insert_single(table.storage, 54321, 1, 2, 3)
    except TypeError:
        pass
    else:
        assert False, "expected exception not thrown"

    # we haven't inserted this key
    assert table.search_single(table.storage, 999) is None


# Custom value module that allows storing three  bits per key.
# In this example the bits are simply used as flags for no special purpose.
# In this implementation once a flag is set, it can never be reset (excepts when the key is removed)
def flags():
    @njit(uint64(uint64, byte, byte, byte))
    def first_insert(key, f1, f2, f3):
        # Construct a single value from the flags by bitshiftig
        return (f1 & 1) | (f2 & 1) << 1 | (f3 & 1) << 2

    @njit(uint64(uint64, uint64, byte, byte, byte))
    def update_value(key, stored_value, f1, f2, f3):
        # same as above, but bit-or with the stored value
        return stored_value | (f1 & 1) | (f2 & 1) << 1 | (f3 & 1) << 2

    @njit(UniTuple(byte, 3)(uint64, uint64))
    def map_value(key, stored_value):
        # restore three seperate flags from the stored value
        return stored_value & 1, (stored_value >> 1) & 1, (stored_value >> 2) & 1

    return (
        first_insert,
        update_value,
        map_value,
        3,  # we want to store exactly three bits
    )


def show_flags():
    config = Config(
        load_factor=0.95,
        k=32,
        bucket_size=4,
        value_module=ValueModuleConfig(module_name=get_qualified_name(flags)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
    )

    (table, _) = generate_hashtable(config, 10 ** 6)

    # store flags for a key
    table.insert_single(table.storage, 1234, 1, 0, 1)
    assert table.search_single(table.storage, 1234) == (1, 0, 1)

    # update the flags, or-ing with the existing flags
    table.insert_single(table.storage, 1234, 0, 1, 0)
    assert table.search_single(table.storage, 1234) == (1, 1, 1)

    # we haven't stored anything for this key
    assert table.search_single(table.storage, 999) is None


if __name__ == "__main__":
    show_counting()
    show_store()
    show_flags()
