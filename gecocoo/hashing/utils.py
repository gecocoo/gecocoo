"""The module provides some utilities for hash function implementations."""


def inverse_mult(a: int, b: int):
    """
    Calculate the inverse multiple of ``a`` with respect to modulo ``b``. ``a`` and ``b`` are assumed to be coprime.

    See: https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm

    :param a: the value
    :param b: the modulo
    :raises ValueError: when values ``a`` and ``b`` are not coprime
    :return: the inverse multiple
    """

    a = int(a)
    b = int(b)
    old_r, r = b, a
    old_t, t = 0, 1

    while r != 0:
        quotient = old_r // r
        (old_r, r) = (r, old_r - quotient * r)
        (old_t, t) = (t, old_t - quotient * t)

    if old_r != 1:
        raise ValueError(
            f"Modular inverse not found for {a} with mod {b}. Make sure to choose coprime values."
        )

    return old_t % b
