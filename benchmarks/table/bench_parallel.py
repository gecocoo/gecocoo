import benchutils
import numpy as np
import pytest
from numba import njit, prange

from gecocoo.config import Config, HashFunction, StorageConfig, ValueModuleConfig
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import generate_random_kmers, get_qualified_name
from gecocoo.valuemodules import keyset

ROUNDS = benchutils.apply_rounds_factor(10)
WARMUP_ROUNDS = benchutils.apply_rounds_factor(2, 0, 5)
SEED = 1234
KEY_COUNT = benchutils.apply_data_factor(10 ** 6, 10000)
THREAD_COUNTS = [4, 8, 16]


def bench_insert_single_threaded(benchmark):
    benchmark.group = "table parallel inserts"
    benchmark.name = f"single thread"

    config = Config(
        load_factor=0.95,
        k=31,
        bucket_size=2,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
    )
    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 32 - 1, KEY_COUNT)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)
    (table, _) = generate_hashtable(config, int(KEY_COUNT * 1.5))

    def setup():
        table.clear(table.storage)

    @njit
    def work(table):
        for k in keys:
            table.insert_single(table.storage, k)

    benchmark.pedantic(
        work, args=(table,), setup=setup, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS
    )


def bench_insert_prange(benchmark):
    benchmark.group = "table parallel inserts"
    benchmark.name = f"prange"

    config = Config(
        load_factor=0.95,
        k=31,
        bucket_size=2,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
        storage=StorageConfig(concurrent=True),
    )
    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 32 - 1, KEY_COUNT)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)
    (table, _) = generate_hashtable(config, int(KEY_COUNT * 1.5))

    def setup():
        table.clear(table.storage)

    @njit(parallel=True)
    def work(table):
        for i in prange(KEY_COUNT):
            k = keys[i]
            table.insert_single(table.storage, k)

    benchmark.pedantic(
        work, args=(table,), setup=setup, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS
    )


@pytest.mark.parametrize("thread_count", THREAD_COUNTS)
def bench_insert_threadpool(benchmark, thread_count):
    benchmark.group = "table parallel inserts"
    benchmark.name = f"threadpool ({thread_count} workers)"

    config = Config(
        load_factor=0.95,
        k=31,
        bucket_size=2,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
        storage=StorageConfig(concurrent=True),
    )
    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 32 - 1, KEY_COUNT)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)
    (table, _) = generate_hashtable(config, int(KEY_COUNT * 1.5))

    chunk_size = KEY_COUNT // thread_count

    def setup():
        table.clear(table.storage)

    def work():
        from concurrent.futures import ThreadPoolExecutor

        with ThreadPoolExecutor(max_workers=thread_count) as e:
            for chunk in range(thread_count):
                e.submit(insert_chunk, table, chunk)

    @njit(nogil=True)
    def insert_chunk(table, chunk):
        for i in range(chunk * chunk_size, chunk * chunk_size + chunk_size):
            k = keys[i]
            table.insert_single(table.storage, k)

    benchmark.pedantic(work, setup=setup, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS)


@pytest.mark.parametrize("thread_count", THREAD_COUNTS)
def bench_insert_threading(benchmark, thread_count):
    from threading import Thread

    benchmark.group = "table parallel inserts"
    benchmark.name = f"threading ({thread_count} threads)"

    config = Config(
        load_factor=0.95,
        k=31,
        bucket_size=2,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
        storage=StorageConfig(concurrent=True),
    )
    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 32 - 1, KEY_COUNT)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)
    (table, _) = generate_hashtable(config, int(KEY_COUNT * 1.5))

    chunk_size = KEY_COUNT // thread_count

    def setup():
        table.clear(table.storage)

    def work():
        threads = list(
            [
                Thread(target=insert_chunk, args=(table, chunk))
                for chunk in range(thread_count)
            ]
        )
        for t in threads:
            t.start()
        for t in threads:
            t.join()

    @njit(nogil=True)
    def insert_chunk(table, chunk):
        for i in range(chunk * chunk_size, chunk * chunk_size + chunk_size):
            k = keys[i]
            table.insert_single(table.storage, k)

    benchmark.pedantic(work, setup=setup, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS)
