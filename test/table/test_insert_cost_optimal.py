import numpy as np
import pulp as pl

from gecocoo.config import Config, HashFunction, ValueModuleConfig
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.valuemodules import store

# Tests use custom generator and interface to get access to the internal structure of the table in order to
# be able to determine the cost of the produced assignment.


def test_insert():
    # Creates the configuration:
    k = 20
    bucket_size = 2
    value_module = ValueModuleConfig(get_qualified_name(store))
    hash_functions = [
        HashFunction(get_qualified_name(xorlinear), []),
        HashFunction(get_qualified_name(xorlinear), []),
        HashFunction(get_qualified_name(xorlinear), []),
        HashFunction(get_qualified_name(xorlinear), []),
        HashFunction(get_qualified_name(xorlinear), []),
    ]

    config = Config(
        load_factor=0.95,
        k=k,
        bucket_size=bucket_size,
        value_module=value_module,
        hash_functions=hash_functions,
    )

    # Goes on to generate the hashtable:
    table, utilities = generate_hashtable(config, (10 ** 4))

    # Retrieves the required elements from the hashtable:
    storage = table.storage

    search_single = table.search_single
    initialize = table.initialize

    # Generates a couple of keys and values:
    in_count = int((10 ** 4) * 0.9)

    keys = np.zeros(in_count, dtype=np.uint64)
    values = np.zeros(in_count, dtype=np.uint64)

    for index in range(in_count):
        keys[index] = index + 1
        values[index] = index + 1

    # Inserts the keys and values using the cost optimal insertion strategy:
    assert initialize(storage, keys, values) == True

    # Checks whether the keys are actually in the table:
    for index in range(in_count):
        assert search_single(storage, index + 1) == (index + 1)


def test_optimal():
    # Creates the configuration:
    k = 20
    bucket_size = 2
    value_module = ValueModuleConfig(get_qualified_name(store))
    hash_functions = [
        HashFunction(get_qualified_name(xorlinear), []),
        HashFunction(get_qualified_name(xorlinear), []),
        HashFunction(get_qualified_name(xorlinear), []),
        HashFunction(get_qualified_name(xorlinear), []),
    ]
    hash_count = len(hash_functions)

    bucket_count = 10 ** 4
    in_count = (10 ** 3) * 9

    config = Config(
        load_factor=0.95,
        k=k,
        bucket_size=bucket_size,
        value_module=value_module,
        hash_functions=hash_functions,
    )

    # Goes on to generate the hashtable:
    table, utilities = generate_hashtable(config, bucket_count)

    # Retrieves the required elements from the hashtable:
    storage = table.storage
    storage_array = storage

    search_single = table.search_single
    initialize = table.initialize

    debug_iterator = table.debug_iterator(storage)

    # Generates a couple of keys and values:
    keys = np.zeros(in_count, dtype=np.uint64)
    values = np.zeros(in_count, dtype=np.uint64)

    for index in range(in_count):
        keys[index] = index + 1
        values[index] = (index + 1) * 2

    # Inserts the keys and values using the cost optimal insertion strategy:
    success = initialize(storage, keys, values)

    # Determines the cost of the assignments:
    actual_cost = 0

    # Gets the hash-functions:
    hashes_forward = tuple([h.forward for h in utilities.llconfig.hash_functions])

    for key, value, hash_index, index, slot in debug_iterator:
        actual_cost += hash_index

    # Calculates the cost optimal assignment cost:
    optimal_cost = 0

    # Defines the problem:
    problem = pl.LpProblem("Assignment", pl.LpMinimize)

    # Creates the datastructures for easily generating the bucket-fill constraints:
    possible_assignments = np.empty(bucket_count, dtype=object)

    for bucket_index in range(bucket_count):
        possible_assignments[bucket_index] = []

    for in_index in range(in_count):
        for hash_index in range(hash_count):
            # Determines the bucket:
            current_bucket = (
                hashes_forward[hash_index](keys[in_index])[0] % bucket_count
            )

            # Appends the new element to the list:
            possible_assignments[current_bucket].append((in_index, hash_index))

    # Defines the variables:
    hash_idxs = []
    in_idxs = []

    for index in range(hash_count):
        hash_idxs.append(str(index))

    for index in range(in_count):
        in_idxs.append(str(index))

    choice = pl.LpVariable.dicts("Choice", (in_idxs, hash_idxs), 0, 1, pl.LpInteger)

    # Creates the objective function and adds it to the problem:
    problem += (
        pl.lpSum(
            [
                (index % hash_count)
                * choice[in_idxs[index // hash_count]][hash_idxs[index % hash_count]]
                for index in range(hash_count * in_count)
            ]
        ),
        "objective",
    )

    # Creates and adds the constraints that ensure that only one hash-function is chosen per in-key:
    for in_index in range(in_count):
        problem += (
            pl.lpSum(
                [
                    choice[in_idxs[in_index]][hash_idxs[hash_index]]
                    for hash_index in range(hash_count)
                ]
            )
            == 1,
            "",
        )

    # Creates and adds the constraints that ensure that no bucket gets overfilled:
    for bucket_index in range(bucket_count):
        problem += (
            pl.lpSum(
                [
                    choice[in_idxs[possible_assignments[bucket_index][index][0]]][
                        hash_idxs[possible_assignments[bucket_index][index][1]]
                    ]
                    for index in range(len(possible_assignments[bucket_index]))
                ]
            )
            <= bucket_size,
            "",
        )

    # Solves the problem and retrieves the optimal cost:
    status = problem.solve()

    if success:
        # If the insert succeded the cost gets compared:
        assert pl.LpStatus[status] == "Optimal"

        optimal_cost = int(pl.value(problem.objective))
        assert actual_cost == optimal_cost
    else:
        # If the insert-strategy failed then the lp is expected to also have failed in finding a solution:
        assert pl.LpStatus[status] != "Optimal"
