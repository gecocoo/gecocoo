.. _CustomHashFunctions:

Custom Hash Functions
=====================
In addition to the predefined hash functions, the user is free to specify his own custom hash functions in the same way as for the :ref:`CustomValueModules`.
In order to be able to load entries inserted into the hash table, the hash functions to be used must be reversible.

Therefore a hash function must implement the functions **forward** and **backward** and return them with the **number of bits** needed to store the key.
As parameters, the hash function receives the **number of buckets**, the **k-mer size** used, a **seed** for randomization, and other **hash function specific parameters as a list** for the calculation of the hash.

:attr:`forward(key)`
  The function computes the corresponding hash from a given key, where the calculation of the hash is an implementation detail.
  As qoutienting is an option in the hashtable configuration the forward function has to return the quotient in addition to the hash_index.
:attr:`backward(hash_index, quotient)`
  Accordingly to the forward function it computes the corresponding key from a given hash_index or the quotient.