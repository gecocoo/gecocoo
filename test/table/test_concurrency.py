import threading

import numpy as np
import pytest
from numba import njit, prange

from gecocoo.config import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.generator import HashtableInterface, generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.valuemodules import counting

SIZE = 10 ** 4


# This test can fail when the CPU is really busy and
# the code does not get executed concurrently.
# For this reason the test is marked with xfail to not
# fail the CI when this happens.
@pytest.mark.xfail(strict=False)
def test_non_concurrent_fails():
    config = Config(
        load_factor=0.95,
        k=16,
        bucket_size=2,
        value_module=ValueModuleConfig(get_qualified_name(counting), [32]),
        hash_functions=[
            HashFunction(name=get_qualified_name(xorlinear)),
            HashFunction(name=get_qualified_name(xorlinear)),
        ],
        storage=StorageConfig(mode=StorageMode.BUCKETS, concurrent=False),
    )
    (table, _) = generate_hashtable(config, SIZE)

    # Use a small number of unique keys to enforce duplicates
    random_keys = np.random.randint(0, 500, 10 ** 4, dtype=np.uint64)

    # Insert keys in parallel
    # TODO: This can potentially timeout
    @njit(parallel=True)
    def insert(table: HashtableInterface):
        for i in prange(len(random_keys)):
            table.insert_single(table.storage, random_keys[i])

    insert(table)

    unique_keys, counts = np.unique(random_keys, return_counts=True)
    key_counts = dict(zip(unique_keys, counts))

    # Expect some count to be wrong
    with pytest.raises(AssertionError):
        for key, actual_count in table.key_value_iterator(table.storage):
            expected_count = key_counts[key]
            assert expected_count == actual_count


@pytest.mark.parametrize(
    "concurrent",
    [
        pytest.param(True),
        pytest.param(False),
    ],
)
def test_concurrent_insert(concurrent):
    config = Config(
        load_factor=0.95,
        k=32,
        bucket_size=2,
        value_module=ValueModuleConfig(get_qualified_name(counting), [64]),
        hash_functions=[
            HashFunction(name=get_qualified_name(xorlinear)),
            HashFunction(name=get_qualified_name(xorlinear)),
        ],
        storage=StorageConfig(
            mode=StorageMode.BUCKETS, concurrent=concurrent, disable_quotienting=True
        ),
    )
    (table, _) = generate_hashtable(config, SIZE)

    steps = 10 ** 5
    keys_repeat = np.array(range(int(SIZE * 0.1)), dtype=np.uint64)
    keys_random = np.random.randint(len(keys_repeat) + 1, SIZE * 0.7, steps)

    # repeatedly insert the same keys to provoke getting duplicates
    @njit(parallel=concurrent, nogil=True)
    def repeat(table: HashtableInterface):
        for i in prange(len(keys_repeat) * steps):
            table.insert_single(table.storage, keys_repeat[i % len(keys_repeat)])

    # insert some random keys to provoke other keys to be moved
    @njit(parallel=concurrent, nogil=True)
    def random(table: HashtableInterface):
        for i in prange(len(keys_random)):
            table.insert_single(table.storage, keys_random[i])

    threads = list(
        [
            threading.Thread(target=task, args=(table,))
            for task in [
                repeat,
                random,
            ]
        ]
    )

    for t in threads:
        t.start()
        if not concurrent:
            t.join()

    for t in threads:
        t.join()

    # check for duplicated keys in the table
    contents = list(table.key_value_iterator(table.storage))
    contents_keys_repeat = list(filter(lambda kv: kv[0] < len(keys_repeat), contents))

    assert len(contents_keys_repeat) == len(keys_repeat)
