"""The module contains a basic implementation of the key count access functions that can be shared across different storage modes."""

from typing import Callable, Tuple

from numba import njit
from numba.types import Type

from .interface import get_storage_access_signatures


def make_key_count_accessors_array(
    storage_type: Type, index: int
) -> Tuple[Callable, Callable, Callable, Callable]:
    """
    Generate the access functions for a key count stored in an array at the specified index.

    :param storage_type: the storage object's type
    :param index: the index the key count is stored at
    :return: a tuple containing the four access functions
    """
    from gecocoo.utils.llvm import atomic_add_64, atomic_sub_64

    signatures = get_storage_access_signatures(storage_type)

    @njit(signatures.get_key_count)
    def get_key_count(storage):
        return storage[index]

    @njit(signatures.set_key_count)
    def set_key_count(storage, count):
        storage[index] = count

    @njit(signatures.inc_key_count)
    def inc_key_count(storage):
        address = storage.ctypes.data + storage.itemsize * index
        atomic_add_64(address, 1)

    @njit(signatures.dec_key_count)
    def dec_key_count(storage):
        address = storage.ctypes.data + storage.itemsize * index
        atomic_sub_64(address, 1)

    return get_key_count, set_key_count, inc_key_count, dec_key_count
