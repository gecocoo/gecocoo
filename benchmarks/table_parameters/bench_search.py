import benchutils
import pytest
from numba import njit, uint64

from gecocoo.config import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import generate_random_kmers, get_qualified_name
from gecocoo.valuemodules import keyset

ROUNDS = benchutils.apply_rounds_factor(10)
WARMUP_ROUNDS = benchutils.apply_rounds_factor(2, 0, 5)
SEED = 1234
KEY_COUNT = benchutils.apply_data_factor(10 ** 6, 10000)
BUCKET_SIZES = [4]
k = 31
STORAGE_MODES = [
    StorageMode.BUCKETS,
    StorageMode.PACKED,
    StorageMode.FAST,
]
NUM_THREADS = [1, 8, 16]
LOAD_FACTORS = [0.75, 0.95, 0.97]
NUM_HASH_FUNCTIONS = [2, 3, 4]
NUM_SEARCHES = [10 ** 6]
COST_OPTIMAL = [True, False]


@pytest.mark.parametrize("bucket_size", BUCKET_SIZES)
@pytest.mark.parametrize("load_factor", LOAD_FACTORS)
@pytest.mark.parametrize("thread_count", NUM_THREADS)
@pytest.mark.parametrize("storage_mode", STORAGE_MODES)
@pytest.mark.parametrize("num_hash_functions", NUM_HASH_FUNCTIONS)
@pytest.mark.parametrize("num_searches", NUM_SEARCHES)
@pytest.mark.parametrize("use_cost_optimal", COST_OPTIMAL)
def bench_search_random_walk(
    bucket_size,
    load_factor,
    thread_count,
    storage_mode,
    num_hash_functions,
    num_searches,
    use_cost_optimal,
    benchmark,
):
    benchmark.group = "table parameters search"
    benchmark.name = (
        f"search ("
        + f"threads: {thread_count}, "
        + f"use_cost_optimal: {use_cost_optimal}, "
        + f"storage_mode: {storage_mode}, "
        + f"bucket_size: {bucket_size}, "
        + f"load_factor: {load_factor}, "
        + f"num_hash_functions: {num_hash_functions}"
        + ")"
    )

    hash_functions = [
        HashFunction(get_qualified_name(xorlinear))
        for _ in range(0, num_hash_functions)
    ]

    config = Config(
        load_factor=load_factor,
        k=k,
        bucket_size=bucket_size,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=hash_functions,
        storage=StorageConfig(
            mode=storage_mode,
            concurrent=thread_count > 1,
            disable_quotienting=(storage_mode == StorageMode.FAST),
        ),
    )

    keys = generate_random_kmers(31, KEY_COUNT, SEED, None)
    (table, _) = generate_hashtable(config, KEY_COUNT)

    if use_cost_optimal:

        def setup():
            table.clear(table.storage)
            table.initialize(table.storage, keys)

    else:

        def setup():
            table.clear(table.storage)
            for key in keys:
                table.insert_single(table.storage, key)

    if thread_count == 1:

        @njit
        def work(table):
            for i in range(0, num_searches):
                key = keys[i % KEY_COUNT]
                table.search_single(table.storage, key)

    else:
        from threading import Thread

        chunk_size = num_searches // thread_count

        def work(table):
            threads = list(
                [
                    Thread(target=insert_chunk, args=(table, chunk))
                    for chunk in range(thread_count)
                ]
            )
            for t in threads:
                t.start()
            for t in threads:
                t.join()

        @njit(nogil=True)
        def insert_chunk(table, chunk):
            for i in range(chunk * chunk_size, chunk * chunk_size + chunk_size):
                key = keys[i % KEY_COUNT]
                table.search_single(table.storage, key)

    benchmark.pedantic(
        work, args=(table,), setup=setup, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS
    )
