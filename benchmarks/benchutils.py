import os
import warnings


def _get_env_var(name, ctor, default):
    value = os.environ.get(name)
    if value is None:
        return default

    try:
        return ctor(value)
    except Exception:
        warnings.warn(f"Failed to parse environment variable {name}", RuntimeWarning)
        return default


def apply_rounds_factor(rounds, min=1, max=None):
    factor = _get_env_var("ROUNDS_FACTOR", float, 1.0)
    rounds = int(rounds * factor)

    if rounds < min:
        return min

    if max is not None and rounds > max:
        return max

    return rounds


def apply_data_factor(count, min=1, max=None):
    factor = _get_env_var("DATA_FACTOR", float, 1.0)
    count = int(count * factor)

    if count < min:
        return min

    if max is not None and count > max:
        return max

    return count
