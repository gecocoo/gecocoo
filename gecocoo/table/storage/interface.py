"""The module defines the interface that every storage mode must provide."""

from typing import Any, Callable, NamedTuple, Tuple, Type, get_type_hints

from numba.core.typing import Signature


class StorageAccess(NamedTuple):
    """The class contains the functions for accessing the hash table's backing storage."""

    compare_entry: Callable[
        [
            Any,
            int,  # index
            int,  # slot
            int,  # hash_index
            int,  # key_part
        ],
        Tuple[
            bool,  # matched
            bool,  # relocating
            int,  # value
        ],
    ]
    cmp_swap_entry: Callable[
        [
            Any,
            int,  # index
            int,  # slot
            int,  # compare hash_index
            int,  # comapre key_part
            int,  # compare value
            int,  # swap hash_index
            int,  # swap key_part
            int,  # swap value
        ],
        bool,  # success
    ]
    set_empty_entry: Callable[
        [
            Any,
            int,  # index
            int,  # slot
            bool,  # relocating
            int,  # hash_index
            int,  # key_part
            int,  # value
        ],
        bool,  # success
    ]
    clear_entry: Callable[
        [
            Any,
            int,  # index
            int,  # slot
            bool,  # compare relocating
            int,  # compare hash_index
            int,  # compare key_part
            int,  # compare value
        ],
        bool,  # success
    ]
    get_entry: Callable[
        [
            Any,
            int,  # index
            int,  # slot
        ],
        Tuple[
            bool,  # occupied
            bool,  # relocating
            int,  # hash_index
            int,  # key_part
            int,  # value
        ],
    ]
    mark_relocating_entry: Callable[
        [
            Any,
            int,  # index
            int,  # slot
            int,  # hash_index
            int,  # key_part
            int,  # value
            bool,  # set relocating
        ],
        bool,  # success
    ]

    get_key_count: Callable[[Any], int]
    set_key_count: Callable[[Any, int], None]
    inc_key_count: Callable[[Any], None]
    dec_key_count: Callable[[Any], None]


StorageAccess.compare_entry.__doc__ = """
    Compare the arguments with the identifying attributes of an entry.
    
    :param storage: the storage object
    :param index: the bucket index
    :param slot: the slot index in the bucket
    :param hash_index: the index of the used hash function
    :param key_part: the expected key to be stored for the entry
    :return: 
        a tuple consisting of three values:

        - ``True`` if the entry matched, ``False`` otherwise
        - ``True`` if the entry is possibly being  relocated, ``False`` otherwise
        - the stored value
    """
StorageAccess.cmp_swap_entry.__doc__ = """
    Set an entry at the specified position to the specified attributes after comparing all attributes to the expected values.
    
    :param storage: the storage object
    :param index: the bucket index
    :param slot: the slot index in the bucket
    :param cmp_hash_index: the expected index of the used hash function
    :param cmp_key_part: the expected key to be stored for the entry
    :param cmp_value: the expected value to be stored for the entry
    :param set_hash_index: the new index of the used hash function
    :param set_key_part: the new key to be stored for the entry
    :param set_value: the new value to be stored for the entry
    :return: ``True`` if all attributes matched the expected values and the entry was updated, ``False`` otherwise
    """
StorageAccess.set_empty_entry.__doc__ = """
    Set an entry at the specified position to the given attributes after confirming the slot to be empty.
    
    :param storage: the storage object
    :param index: the bucket index
    :param slot: the slot index in the bucket
    :param relocating: the relocating state of the entry
    :param hash_index: the index of the used hash function
    :param key_part: the key to be stored for the entry
    :param value: the value to be stored for the entry
    :return: ``True`` if an empty entry was replaced with the given attributes, ``False`` otherwise
    """
StorageAccess.clear_entry.__doc__ = """
    Clear an entry at the specified position after comparing all attributes.
    
    :param storage: the storage object
    :param index: the bucket index
    :param slot: the slot index in the bucket
    :param relocating: the expected relocating state of the entry
    :param hash_index: the expected index of the used hash function
    :param key_part: the expected key to be stored for the entry
    :param value: the expected value to be stored for the entry
    :return: ``True`` if all attributes matched and the entry was cleared, ``False`` otherwise
    """
StorageAccess.get_entry.__doc__ = """
    Get the stored attributes for the specified entry.
    
    :param storage: the storage object
    :param index: the bucket index
    :param slot: the slot index in the bucket
    :return:
        a tuple of the stored attributes:

        - ``True`` if the specified slot is occupied
        - ``True`` if the entry at the specified position is possibly relocating
        - the entry's hash index
        - the entry's stored key part
        - the entry's stored value
"""
StorageAccess.mark_relocating_entry.__doc__ = """
    Mark the entry as being relocated.
    
    :param storage: the storage object
    :param index: the bucket index
    :param slot: the slot index in the bucket
    :param hash_index: the expected index of the used hash function
    :param key_part: the expected key to be stored in the entry
    :param value: the expected stored value
    :return: ``True`` if the flag was successfully set
    """
StorageAccess.get_key_count.__doc__ = """
    Get the stored key count.
    
    :param storage: the storage object
    :return: the count
    :rtype: ``int``
    """
StorageAccess.set_key_count.__doc__ = """
    Set the stored key count to the specified value.
    
    :param storage: the storage object
    :param count: the new count
    """
StorageAccess.inc_key_count.__doc__ = """
    Increment the stored key count by one. Must work atomically when used concurrently.
    
    :param storage: the storage object
    """
StorageAccess.dec_key_count.__doc__ = """
    Decrement the stored key count by one. Must work atomically when used concurrently.

    :param storage: the storage object
    """


class StorageAccessSignatures(NamedTuple):
    """The class bundles Numba function signatures for all the storage access functions defined in :class:`StorageAccess`."""

    compare_entry: Signature
    cmp_swap_entry: Signature
    set_empty_entry: Signature
    clear_entry: Signature
    get_entry: Signature
    mark_relocating_entry: Signature
    get_key_count: Signature
    set_key_count: Signature
    inc_key_count: Signature
    dec_key_count: Signature


# Generate docstrings with references to the respective functions
for attr in get_type_hints(StorageAccessSignatures):
    if not attr.startswith("_"):
        getattr(
            StorageAccessSignatures, attr
        ).__doc__ = f"Signature for :meth:`StorageAccess.{attr}`"


def get_storage_access_signatures(storage_type: Type) -> StorageAccessSignatures:
    """
    Get the storage access signatures when the given storage object type is used.

    :param storage_type: the storage object's type
    :return: the signatures
    """

    from numba.types import Tuple, boolean, uint64, void

    return StorageAccessSignatures(
        compare_entry=Tuple(
            [
                boolean,  # matched
                boolean,  # relocating
                uint64,  # value
            ]
        )(
            storage_type,
            uint64,  # index
            uint64,  # slot
            uint64,  # hash_index
            uint64,  # key_part
        ),
        cmp_swap_entry=boolean(
            storage_type,
            uint64,  # index
            uint64,  # slot
            uint64,  # compare hash_index
            uint64,  # compare key_part
            uint64,  # compare value
            uint64,  # swap hash_index
            uint64,  # swap key_part
            uint64,  # swap value
        ),
        set_empty_entry=boolean(
            storage_type,
            uint64,  # index
            uint64,  # slot
            boolean,  # relocating
            uint64,  # hash_index
            uint64,  # key_part
            uint64,  # value
        ),
        clear_entry=boolean(
            storage_type,
            uint64,  # index
            uint64,  # slot
            boolean,  # relocating
            uint64,  # hash_index
            uint64,  # key_part
            uint64,  # value
        ),
        get_entry=Tuple(
            [
                boolean,  # occupied
                boolean,  # relocating
                uint64,  # hash_index
                uint64,  # key_part
                uint64,  # value
            ]
        )(
            storage_type,
            uint64,  # index
            uint64,  # slot
        ),
        mark_relocating_entry=boolean(
            storage_type,
            uint64,  # index
            uint64,  # slot
            uint64,  # hash_index
            uint64,  # key_part
            uint64,  # value
            boolean,  # set relocating
        ),
        get_key_count=uint64(storage_type),
        set_key_count=void(
            storage_type,
            uint64,
        ),
        inc_key_count=void(storage_type),
        dec_key_count=void(storage_type),
    )
