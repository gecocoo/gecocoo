"""The module provides the internally used representation of the :class:`~.definition.Config` object provided by the user."""

import inspect
from dataclasses import dataclass
from enum import Enum
from importlib import import_module
from json import JSONEncoder
from math import ceil
from typing import Any, Callable, Dict, List, Optional, Tuple

import numpy as np
from numba import njit
from numba.types import UniTuple, uint64

from ..valuemodules import ValueModuleBase
from .definition import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)


class ConfigError(Exception):
    """
    The exception is thrown if there exists an issue with the provided configuration.

    :param attribute: the name of the configuration's attribute that is invalid
    :param message: a description of the problem
    :param value: possibly the problematic value of the attribute or ``None``
    """

    def __init__(self, attribute, message, value=None):
        """Construct an instance."""
        self.attribute = attribute
        self.value = value
        self.message = message

    def __str__(self):
        """Turn the error into a nice string."""
        if self.value is not None:
            return f"{self.attribute}: {self.message}, but is: {self.value}"
        else:
            return f"{self.attribute}: {self.message}"


@dataclass(frozen=True)
class LLHashFunction:
    """The class provides the internally used representation of a hash function."""

    name: str
    seed: int
    parameters: Tuple[int]
    forward: Callable[[uint64], uint64]
    backward: Callable[[uint64], Tuple[uint64, uint64]]
    keybits: int


@dataclass(frozen=True)
class LLValueFunctions:
    """The class provides the internally used representation of a value module."""

    name: str
    parameter_count: int
    first_insert: Callable
    update_value: Callable
    map_value: Callable
    parameters: Tuple[int]


@dataclass(frozen=True)
class LLConfig:
    """The class provides the internally used representation of a user provided :class:`.definition.Config`."""

    k: int
    key_bits: int
    value_bits: int
    num_buckets: int
    bucket_size: int
    value_module: LLValueFunctions
    hash_functions: Tuple[LLHashFunction]
    storage: StorageConfig


class _ResolveType(Enum):
    VALUE_MODULE = 0
    HASH_FUNCTION = 1


def _resolve_qualified_name(qname: str, resolve_type: _ResolveType):
    components = qname.split(".")
    module_name = ".".join(components[:-1])
    short_name = components[-1]

    if module_name == "":
        if resolve_type == _ResolveType.VALUE_MODULE:
            module_name = "gecocoo.valuemodules"
        elif resolve_type == _ResolveType.HASH_FUNCTION:
            module_name = "gecocoo.hashing"

    module = import_module(module_name)
    instance = getattr(module, short_name)

    return instance


def _resolve_value_module(
    config: ValueModuleConfig,
) -> Tuple[LLValueFunctions, int]:
    value_module = _resolve_qualified_name(
        config.module_name, _ResolveType.VALUE_MODULE
    )

    # check if the given module is a class or a function
    if inspect.isclass(value_module):
        # create instance of class
        assert issubclass(
            value_module, ValueModuleBase
        ), f"class {value_module} must be a subclass of ValueModuleBase"
        instance: ValueModuleBase = value_module(*config.parameters)

        first_insert = instance.make_first_insert()
        update_value = instance.make_update_value()
        map_value = instance.make_map_value()
        bits = instance.value_bits
    else:
        # call constructor as a function
        first_insert, update_value, map_value, bits = value_module(*config.parameters)

    assert callable(first_insert), "first_insert must be a function"
    assert callable(update_value), "update_value must be a function"

    first_insert_signature = inspect.signature(first_insert)
    update_value_signature = inspect.signature(update_value)

    assert len(first_insert_signature.parameters) + 1 == len(
        update_value_signature.parameters
    ), "first_insert and update_value must accept the same number of arguments"

    return (
        LLValueFunctions(
            name=config.module_name,
            parameter_count=len(first_insert_signature.parameters) - 1,
            first_insert=first_insert,
            update_value=update_value,
            map_value=map_value,
            parameters=tuple(config.parameters),
        ),
        bits,
    )


def _disable_quotienting_wrapper(forward, backward):
    @njit(UniTuple(uint64, 2)(uint64))
    def new_forward(key: uint64):
        return forward(key)[0], key

    @njit(uint64(uint64, uint64))
    def new_backward(hash_index: uint64, quotient: uint64):
        return quotient

    return new_forward, new_backward


def _no_occupied_flag_wrapper(forward, backward, zero_bucket):
    # This Wrapper will map all keys which have a quotient of 0 to a special bucket

    @njit(UniTuple(uint64, 2)(uint64))
    def new_forward(key: uint64):
        ret = forward(key)
        if ret[1] != 0:
            return ret
        else:
            return (
                zero_bucket,
                ret[0] + 1,
            )  # This +1 ensures that if both hash_index and quotient are 0, this will still work

    @njit(uint64(uint64, uint64))
    def new_backward(hash_index: uint64, quotient: uint64):
        if hash_index != zero_bucket:
            return backward(hash_index, quotient)
        else:
            return backward(quotient - 1, 0)

    return new_forward, new_backward


def _resolve_hash_function(
    num_buckets: int,
    k: int,
    disable_quotienting: bool,
    disable_occupied_flag: bool,
    hash_function: HashFunction,
    seed: Optional[int] = None,
):
    if disable_occupied_flag:
        # decrease the number of buckets to the limit where the normal hashes are supposed to go
        num_buckets -= 1

    hash_constructor = _resolve_qualified_name(
        hash_function.name, _ResolveType.HASH_FUNCTION
    )
    assert callable(
        hash_constructor
    ), f"cannot use {hash_function.name} as hash function constructor"
    seed = (
        seed if seed is not None else np.random.randint(0, 2 ** 64 - 1, dtype=np.uint64)
    )
    forward, backward, keybits = hash_constructor(
        num_buckets, k, seed, hash_function.parameters
    )
    if disable_quotienting:
        forward, backward = _disable_quotienting_wrapper(forward, backward)
        keybits = k * 2

    # If used the empty flag wrapper needs to be applied after the quotienting wrapper
    if disable_occupied_flag:
        forward, backward = _no_occupied_flag_wrapper(forward, backward, num_buckets)

    return LLHashFunction(
        hash_function.name,
        seed,
        tuple(hash_function.parameters),
        forward,
        backward,
        keybits,
    )


def resolve_config(config: Config, override_n: Optional[int] = None) -> LLConfig:
    """
    Resolve a :class:`~.definition.Config` object to an :class:`LLConfig` object.

    The hash functions and the value module that are referenced by name in the configuration
    are resolved and instantiated. Some important values like bucket count and the required key
    bits to be saved are calculated and saved for later use during generating the hash table.

    :param config: the configuration to resolve
    :param override_n:
        an optional value for the number unique keys the table should hold, that will override the one provided.
        At least one of :attr:`.definition.Config.n` and ``override_n`` must be set, while ``override_n`` takes precedence.
    :raises ConfigError: if the configuration is invalid
    :return: the resolved :class:`LLConfig` object
    """

    n = override_n or config.n
    if n is None:
        raise ConfigError("n", "number of entries must be specified in config")

    if n <= 0:
        raise ConfigError("n", "number of entries must be larger than zero")

    storage = config.storage
    if storage is None:
        # Create storage configuration with all options on default values
        storage = StorageConfig()

    if storage.mode == StorageMode.FAST and not storage.disable_quotienting:
        raise ConfigError(
            "storage.disable_quotienting",
            "fast storage requires disabling quotienting",
            storage.disable_quotienting,
        )

    # Validate bucket_size
    if not isinstance(config.bucket_size, int) or not 1 <= config.bucket_size:
        raise ConfigError(
            "bucket_size", "value must be greater or equal to 1", config.bucket_size
        )

    # Validate k
    if not isinstance(config.k, int) or not (config.k >= 1 and config.k <= 32):
        raise ConfigError("k", "value must be between 1 and 32 (inclusive)", config.k)

    # Validate load_factor
    if not isinstance(config.load_factor, float) or not 0.0 < config.load_factor <= 1.0:
        raise ConfigError(
            "load_factor", "value must be between 0 and 1", config.load_factor
        )

    # Aiming for: load_factor = n / capacity <=> capacity = n / load_factor
    # num_buckets = capacity / bucket_size
    # The number of buckets must be >=1
    num_buckets = ceil(ceil(n / config.load_factor) / config.bucket_size)
    value_module, value_bits = _resolve_value_module(config.value_module)

    # if empty bit is not used the quotient=0 elements will be put in a seperat bucket
    if storage.disable_occupied_flag:
        if not storage.mode == StorageMode.BUCKETS:
            raise ConfigError(
                "disable_occupied_flag", "can only be used with StorageMode BUCKETS"
            )
        if storage.disable_quotienting:
            raise ConfigError(
                "disable_occupied_flag", "can not be used with disable_quotienting"
            )
        num_buckets += 1

    # Validate hash_functions
    if len(config.hash_functions) == 0:
        raise ConfigError(
            "hash_functions", "at least one hash function must be specified"
        )

    hash_functions = tuple(
        map(
            lambda h: _resolve_hash_function(
                num_buckets,
                config.k,
                storage.disable_quotienting,
                storage.disable_occupied_flag,
                h,
            ),
            config.hash_functions,
        )
    )
    key_bits = int(max(map(lambda h: h.keybits, hash_functions)))

    return LLConfig(
        k=config.k,
        key_bits=key_bits,
        value_bits=value_bits,
        num_buckets=num_buckets,
        bucket_size=config.bucket_size,
        value_module=value_module,
        hash_functions=hash_functions,
        storage=storage,
    )


def from_json(dct: Dict) -> Any:
    """
    Restore a :class:`LLConfig` object from a json object.

    This function is meant to be used as the ``object_hook`` argument of :func:`json.load`.

    :param dct: the object to de-serialize
    :return: the de-serialized object
    """

    if "__llconfig__" in dct:
        k: int = dct["k"]
        bucket_count: int = dct["bucket_count"]
        storage: StorageConfig = dct["storage"]
        hash_functions = tuple(
            map(
                lambda hs: _resolve_hash_function(
                    bucket_count,
                    k,
                    storage.disable_quotienting,
                    storage.disable_occupied_flag,
                    hs[0],
                    hs[1],
                ),
                dct["hash_functions"],
            )
        )
        value_module: LLValueFunctions = dct["value_module"]

        return LLConfig(
            k=k,
            key_bits=dct["key_bits"],
            value_bits=dct["value_bits"],
            num_buckets=bucket_count,
            bucket_size=dct["bucket_size"],
            value_module=value_module,
            hash_functions=hash_functions,
            storage=storage,
        )

    if "__llvaluefunctions__" in dct:
        module = dct["module"]
        parameters = tuple(dct["parameters"])
        value_functions, _ = _resolve_value_module(
            ValueModuleConfig(module_name=module, parameters=parameters)
        )
        return value_functions

    if "__llhashfunction__" in dct:
        name = dct["name"]
        seed = dct["seed"]
        parameters = tuple(dct["parameters"])
        return (HashFunction(name, parameters), seed)

    if "__llstorageconfig__" in dct:
        mode = StorageMode[dct["mode"]]
        concurrent = dct["concurrent"]
        disable_quotienting = dct["disable_quotienting"]
        return StorageConfig(
            mode=mode, concurrent=concurrent, disable_quotienting=disable_quotienting
        )

    return dict


class LLConfigJSONEncoder(JSONEncoder):
    """Implementation of :class:`json.JSONEncoder` that supports serializing :class:`LLConfig` objects."""

    def default(self, obj: Any):
        """
        Serialize ``obj`` to a json object if possible.

        This implements the serialization of :class:`LLConfig` objects.

        :param obj: the object to serialize
        :return: the serialized object
        """

        if isinstance(obj, LLConfig):
            config: LLConfig = obj
            return dict(
                __llconfig__=True,
                k=config.k,
                key_bits=config.key_bits,
                value_bits=config.value_bits,
                bucket_count=config.num_buckets,
                bucket_size=config.bucket_size,
                value_module=config.value_module,
                hash_functions=config.hash_functions,
                storage=config.storage,
            )

        if isinstance(obj, LLValueFunctions):
            value_functions: LLValueFunctions = obj
            return dict(
                __llvaluefunctions__=True,
                module=value_functions.name,
                parameters=value_functions.parameters,
            )

        if isinstance(obj, LLHashFunction):
            hash_function: LLHashFunction = obj
            return dict(
                __llhashfunction__=True,
                name=hash_function.name,
                seed=int(hash_function.seed),
                parameters=hash_function.parameters,
            )

        if isinstance(obj, StorageConfig):
            storage: StorageConfig = obj
            return dict(
                __llstorageconfig__=True,
                mode=storage.mode.name,
                concurrent=storage.concurrent,
                disable_quotienting=storage.disable_quotienting,
            )

        # serialize default types or raise TypeError
        return JSONEncoder.default(self, obj)
