from dataclasses import dataclass
from typing import List

import hypothesis.strategies as st
import pytest
from hypothesis import given, settings

from gecocoo.hashing import swaplinear, xorlinear

hash_functions = [
    swaplinear,
    xorlinear,
]


@dataclass
class HashTestCase:
    size: int
    k: int
    seed: int
    keys: List[int]


@st.composite
def hash_test_case(draw):
    size = draw(st.integers(min_value=20, max_value=10 ** 8))
    k = draw(st.integers(min_value=1, max_value=32))
    seed = draw(st.integers(min_value=0, max_value=2 ** 64))
    keys = draw(
        st.lists(st.integers(min_value=0, max_value=2 ** (2 * k) - 1), min_size=1)
    )

    return HashTestCase(size, k, seed, keys)


@pytest.mark.parametrize("hash_function", hash_functions)
@given(case=hash_test_case())
@settings(deadline=None, max_examples=500)
def test_hash_function(hash_function, case: HashTestCase):
    """
    Verfiy that applying the hash function to key and then the reverse function to the result gives the input key again.
    """
    forward, backward, _ = hash_function(case.size, case.k, case.seed, [])

    for expected_key in case.keys:
        index, quotient = forward(expected_key)
        key = backward(index, quotient)
        assert key == expected_key
