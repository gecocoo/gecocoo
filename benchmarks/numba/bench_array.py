from typing import Any, Callable, NamedTuple

import benchutils
import hypothesis.strategies as st
import numpy as np
import pytest
from hypothesis import given
from numba import from_dtype, njit, uint64, void

from gecocoo.utils.bitfieldsarray import Bitfield, Mode, make_bitfields_array

ROUNDS = benchutils.apply_rounds_factor(20)
WARMUP_ROUNDS = benchutils.apply_rounds_factor(3, 0, 5)
SEED = 1234
CAPACITY = 10 ** 7
COUNT = benchutils.apply_data_factor(10 ** 7, 100)


def bitfields_buckets(mode: Mode):
    """
    Create an array of bit field using buckets with better memory efficiency, but slightly worse accessing performance.
    """

    fields = [
        Bitfield("empty", 1),
        Bitfield("hash_index", 2),
        Bitfield("key_part", 20),
        Bitfield("value", 32),
    ]
    size, get_entry, set_entry, _ = make_bitfields_array(
        CAPACITY / 4, fields, 4, Mode(mode)
    )
    array = np.zeros(size, dtype=np.uint64)

    @njit(void(uint64[:], uint64, uint64, uint64, uint64, uint64))
    def set_entry(array, index, empty, hash_index, key_part, value):
        set_entry(array, index // 4, index % 4, empty, hash_index, key_part, value)

    @njit(uint64(uint64[:], uint64))
    def get_entry(array, index):
        empty, hash_index, key_part, value = get_entry(array, index // 4, index % 4)
        return empty + hash_index + key_part + value

    return array, set_entry, get_entry


def structured_array(align: bool):
    """
    Use a structured array that NumPy will add padding/alignment to and it therefore takes more memory
    """
    dtype = np.dtype(
        [
            ("empty", np.uint8),
            ("hash_index", np.uint8),
            ("key_part", np.uint32),
            ("value", np.uint32),
        ],
        align=align,
    )
    array = np.zeros(CAPACITY, dtype)
    array_type = from_dtype(dtype)

    @njit(void(array_type[:], uint64, uint64, uint64, uint64, uint64))
    def set_entry(array, index, empty, hash_index, key_part, value):
        entry = array[index]
        entry.empty = empty
        entry.hash_index = hash_index
        entry.key_part = key_part
        entry.value = value

    @njit(uint64(array_type[:], uint64))
    def get_entry(array, index):
        entry = array[index]
        return entry.empty + entry.hash_index + entry.key_part + entry.value

    return array, set_entry, get_entry


def record_array(align: bool):
    """
    Again use a structured array but interpret it as an array of records, which seems to improve Numbas performance.
    """
    dtype = np.dtype(
        [
            ("empty", np.uint8),
            ("hash_index", np.uint8),
            ("key_part", np.uint32),
            ("value", np.uint32),
        ],
        align=align,
    )
    array = np.rec.array(np.zeros(CAPACITY, dtype))
    array_type = from_dtype(dtype)

    @njit(void(array_type[:], uint64, uint64, uint64, uint64, uint64))
    def set_entry(array, index, empty, hash_index, key_part, value):
        entry = array[index]
        entry.empty = empty
        entry.hash_index = hash_index
        entry.key_part = key_part
        entry.value = value

    @njit(uint64(array_type[:], uint64))
    def get_entry(array, index):
        entry = array[index]
        return entry.empty + entry.hash_index + entry.key_part + entry.value

    return array, set_entry, get_entry


class Benchmark(NamedTuple):
    id: str
    array: Any
    set_entry: Callable
    get_entry: Callable


BENCHMARKS = [
    *[
        Benchmark(f"bitfields array ({mode})", *bitfields_buckets(mode))
        for mode in Mode
    ],
    Benchmark("structured array", *structured_array(False)),
    Benchmark("structured array (aligned)", *structured_array(True)),
    Benchmark("record array", *record_array(False)),
    Benchmark("record array (aligned)", *record_array(True)),
]


@pytest.mark.parametrize(
    "array,set_entry,name",
    [
        pytest.param(array, set_entry, id, id=id)
        for id, array, set_entry, _ in BENCHMARKS
    ],
)
def bench_array_writes(benchmark, array, set_entry, name):
    benchmark.group = "array writes"
    benchmark.name = name

    rng = np.random.RandomState(SEED)
    indexes = rng.randint(0, CAPACITY, COUNT)

    def setup():
        array.fill(0)

    @njit
    def work(array):
        for i in indexes:
            set_entry(array, i, 1, 3, 4321, 123456)

    benchmark.pedantic(
        work, args=(array,), setup=setup, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS
    )


@pytest.mark.parametrize(
    "array,get_entry,name",
    [
        pytest.param(array, get_entry, id, id=id)
        for id, array, _, get_entry in BENCHMARKS
    ],
)
def bench_array_reads(benchmark, array, get_entry, name):
    benchmark.group = "array reads"
    benchmark.name = name

    rng = np.random.RandomState(SEED)
    indexes = rng.randint(0, CAPACITY, COUNT)

    @njit
    def work(array):
        sum = uint64(0)
        for i in indexes:
            sum += get_entry(array, i)
        return sum

    benchmark.pedantic(work, args=(array,), rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS)


@pytest.mark.parametrize(
    "array,get_entry,set_entry",
    [
        pytest.param(array, get_entry, set_entry, id=id)
        for id, array, set_entry, get_entry in BENCHMARKS
    ],
)
@given(index=st.integers(min_value=0, max_value=CAPACITY))
def test_read_write(array, get_entry, set_entry, index):
    """
    Very simple test to verify reading and writing works as expected
    """
    set_entry(array, index, 1, 2, 3, 4)
    assert get_entry(array, index) == 10


if __name__ == "__main__":
    factor = 1024 ** 2
    unit = "MiB"

    min_nbytes = min([array.nbytes for _, array, _, _ in BENCHMARKS])

    # Print array sizes in bytes
    for id, array, _, _ in BENCHMARKS:
        nbytes = array.nbytes
        print(f"{id:<25}: {nbytes / factor:.4f} {unit} ({nbytes / min_nbytes:.2f})")
