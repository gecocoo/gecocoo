Credits
=======

The project is the result of project group "GeCoCoo: Generierung von Code für schnelle Cuckoo-Hashing-Varianten in Genomanalysen" (PG 633)
at the TU Dortmund University.
A project group is a mandatory part of the Master of Science in Computer Science at the TU Dortmund University.

Authors
-------

- Dennis Bednarek
- Alwin Berger
- Thomas Buse
- Sven Deichsel
- Niklas Domagalla
- Leon Motzet
- Erik Rieping
- Fabian Sieber
- Jakob Vogt
- Felix Wiegand
- Jannik Wolff