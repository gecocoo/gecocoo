import benchutils
import numpy as np
import pytest
from numba import njit, uint64

from gecocoo.config import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import xorlinear
from gecocoo.helper import generate_random_kmers, get_qualified_name
from gecocoo.valuemodules import counting, keyset

ROUNDS = benchutils.apply_rounds_factor(10)
WARMUP_ROUNDS = benchutils.apply_rounds_factor(2, 0, 5)
SEED = 1234
KEY_COUNT = benchutils.apply_data_factor(10 ** 6 * 2, 10000)


@pytest.mark.parametrize("storage_mode", StorageMode)
def bench_keyset(benchmark, storage_mode: StorageMode):
    benchmark.group = "gecocoo vs. python"
    benchmark.name = f"gecocoo set ({storage_mode})"

    config = Config(
        load_factor=0.95,
        k=31,
        bucket_size=4,
        value_module=ValueModuleConfig(module_name=get_qualified_name(keyset)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
        storage=StorageConfig(
            mode=storage_mode, disable_quotienting=storage_mode == StorageMode.FAST
        ),
    )
    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 32 - 1, KEY_COUNT, dtype=np.uint64)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)
    (table, _) = generate_hashtable(config, int(KEY_COUNT * 1.5))

    def setup():
        table.clear(table.storage)

    @njit
    def work(table):
        for k in keys:
            table.insert_single(table.storage, k)

    benchmark.pedantic(
        work, args=(table,), setup=setup, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS
    )


def bench_numba_set(benchmark):
    benchmark.group = "gecocoo vs. python"
    benchmark.name = f"numba set"

    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 32 - 1, KEY_COUNT, dtype=np.uint64)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)

    @njit
    def work():
        table = set()
        for k in keys:
            table.add(k)
        return table

    benchmark.pedantic(work, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS)


def bench_python_set(benchmark):
    benchmark.group = "gecocoo vs. python"
    benchmark.name = f"python set"

    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 32 - 1, KEY_COUNT, dtype=np.uint64)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)

    def work():
        table = set()
        for k in keys:
            table.add(k)
        return table

    benchmark.pedantic(work, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS)


@pytest.mark.parametrize("storage_mode", StorageMode)
def bench_counting(benchmark, storage_mode: StorageMode):
    benchmark.group = "gecocoo vs. python"
    benchmark.name = f"gecocoo counting ({storage_mode})"

    config = Config(
        load_factor=0.95,
        k=31,
        bucket_size=4,
        value_module=ValueModuleConfig(module_name=get_qualified_name(counting)),
        hash_functions=[
            HashFunction(get_qualified_name(xorlinear)),
            HashFunction(get_qualified_name(xorlinear)),
        ],
        storage=StorageConfig(
            mode=storage_mode, disable_quotienting=storage_mode == StorageMode.FAST
        ),
    )
    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 32 - 1, KEY_COUNT, dtype=np.uint64)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)
    (table, _) = generate_hashtable(config, int(KEY_COUNT * 1.5))

    def setup():
        table.clear(table.storage)

    @njit
    def work(table):
        for k in keys:
            table.insert_single(table.storage, k)

    benchmark.pedantic(
        work, args=(table,), setup=setup, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS
    )


def bench_numba_counting(benchmark):
    from numba.typed import Dict

    benchmark.group = "gecocoo vs. python"
    benchmark.name = f"numba counting"

    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 32 - 1, KEY_COUNT, dtype=np.uint64)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)

    @njit
    def work():
        table = Dict.empty(
            key_type=uint64,
            value_type=uint64,
        )
        for k in keys:
            table[k] = table.get(k, uint64(0)) + uint64(1)
        return table

    benchmark.pedantic(work, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS)


def bench_python_counting(benchmark):
    benchmark.group = "gecocoo vs. python"
    benchmark.name = f"python counting"

    # rng = np.random.RandomState(SEED)
    # keys = rng.randint(0, 2 ** 32 - 1, KEY_COUNT, dtype=np.uint64)
    keys = generate_random_kmers(31, KEY_COUNT, SEED)

    def work():
        table = dict()
        for k in keys:
            table[k] = table.get(k, 0) + 1
        return table

    benchmark.pedantic(work, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS)
