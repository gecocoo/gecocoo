"""The module provides some helper functions for users of GeCoCoo."""

from typing import Callable, Optional, Type, Union

import numpy as np
from numba import njit, uint64


def get_qualified_name(x: Union[Type, Callable]) -> str:
    """
    Return a fully qualified name of the given class or function.

    :param x: type of a class or a function
    :return: a fully qualified name like :code:`"gecocoo.valuemodules.store.store"`
    """
    return f"{x.__module__}.{x.__qualname__}"


def generate_random_kmers(
    k: int,
    count: int,
    seed: Optional[int] = None,
    kmers_per_sequence: Optional[int] = None,
) -> np.ndarray:
    """
    Return an array filled with sequential k-mers.

    :param k: the k for the k-mer, must be between 1 and 32
    :param count: the number of k-mers the returned array should contain
    :param seed: the seed for the random number generation
    :param kmers_per_sequence: if not ``None``, the returned sequence of k-mers will be reset after the specified number of k-mers
    :return: the array filled with random k-mers of type :attr:`np.uint64`
    """

    if not (0 < k <= 32):
        raise ValueError(f"k is not in the range from 1 to 32")

    kmer_array = np.zeros(count, dtype=np.uint64)
    mask = uint64(4 ** (k - 1) - 1)
    sequence_length = uint64(
        kmers_per_sequence if kmers_per_sequence is not None else count
    )

    @njit
    def fill_array(array):
        if seed is not None:
            np.random.seed(seed)
        kmer = uint64(0)
        for i in range(0, count):
            if i % sequence_length == 0:
                kmer = uint64(np.random.randint(0, mask))
            kmer = uint64((kmer & mask) * 4) + uint64(np.random.randint(0, 4))
            array[i] = kmer
        return array

    return fill_array(kmer_array)
