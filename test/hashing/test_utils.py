import math

import hypothesis.strategies as st
import pytest
from hypothesis import assume, example, given, settings

from gecocoo.hashing.utils import inverse_mult


@given(x=st.integers(min_value=0), mod=st.integers(min_value=2))
@example(x=238172381943, mod=2 ** 64)
@example(x=1359472127461665, mod=18014398509481984)
@settings(max_examples=2000)
def test_inverse_mult(x, mod):
    """
    Verify that the calculated inverse multiple is the actual inverse multiple of a given number.
    """
    # assume x and mod are coprime
    assume(math.gcd(x, mod) == 1)

    assert (x * inverse_mult(x, mod)) % mod == 1


@pytest.mark.parametrize(
    "x,mod",
    [
        (2, 10),
        (7, 28),
        (2342, 782),
    ],
)
def test_inverse_mult_fails_when_not_coprime(x, mod):
    with pytest.raises(ValueError):
        inverse_mult(x, mod)
