import math
from typing import Callable, NamedTuple

import benchutils
import numpy as np
import pytest
from numba import njit, uint64

from gecocoo.hashing import swaplinear, xorlinear

ROUNDS = benchutils.apply_rounds_factor(20)
WARMUP_ROUNDS = benchutils.apply_rounds_factor(3, 0, 5)
HASH_FUNCTIONS = [
    swaplinear,
    xorlinear,
    # add more hash functions here...
]
KS = [18, 25, 32]
SEED = 123456
COUNT = benchutils.apply_data_factor(10 ** 8, 100)


class Benchmark(NamedTuple):
    name: str
    hash_function: Callable
    k: int


BENCHMARKS = list(
    [
        Benchmark(f"{hash_function.__name__} k={k}", hash_function, k)
        for hash_function in HASH_FUNCTIONS
        for k in KS
    ]
)


@pytest.mark.parametrize(
    "hash_function,k,name",
    [
        pytest.param(hash_function, k, name, id=name)
        for name, hash_function, k in BENCHMARKS
    ],
)
def bench_hash_function_forward(benchmark, hash_function, k, name):
    """
    Benchmark forward hash function
    """

    benchmark.group = f"hashing {COUNT} keys (forward)"
    benchmark.name = name

    num_buckets = int(math.sqrt(2 ** (2 * k) * 2))
    forward, _, _ = hash_function(num_buckets, k, SEED, [])

    rng = np.random.RandomState(SEED)
    keys = rng.randint(0, 2 ** (2 * k) - 1, COUNT, dtype=np.uint64)

    @njit
    def work():
        sum = uint64(0)
        for key in keys:
            index, rest = forward(key)
            sum += index + rest

        return sum

    benchmark.pedantic(work, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS)


@pytest.mark.parametrize(
    "hash_function,k,name",
    [
        pytest.param(hash_function, k, name, id=name)
        for name, hash_function, k in BENCHMARKS
    ],
)
def bench_hash_function_backward(benchmark, hash_function, k, name):
    """
    Benchmark backward hash function
    """

    benchmark.group = f"hashing {COUNT} keys (backward)"
    benchmark.name = name

    num_buckets = int(math.sqrt(2 ** (2 * k) * 2))
    forward, backward, _ = hash_function(num_buckets, k, SEED, [])

    rng = np.random.RandomState(SEED)
    keys = rng.randint(0, 2 ** (2 * k) - 1, COUNT, dtype=np.uint64)

    @njit
    def calculate_index_and_rest(keys):
        indexes = np.zeros(COUNT, dtype=np.uint64)
        rests = np.zeros(COUNT, dtype=np.uint64)

        for i, k in enumerate(keys):
            index, rest = forward(k)
            indexes[i] = index
            rests[i] = rest

        return indexes, rests

    indexes, rests = calculate_index_and_rest(keys)

    @njit
    def work():
        sum = uint64(0)
        for index, rest in zip(indexes, rests):
            key = backward(index, rest)
            sum += key

        return sum

    benchmark.pedantic(work, rounds=ROUNDS, warmup_rounds=WARMUP_ROUNDS)
