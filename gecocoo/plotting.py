"""The module provides some tools for plotting data related to hash tables like a hash table's occupancy distribution."""

import matplotlib.pyplot as plt
from matplotlib.figure import Figure

from .interface import OccupancyData


def plot_hash_usage(data: OccupancyData) -> Figure:
    """
    Plot the hash function usage in each segment.

    :param data: the occupancy data
    :return: the created matplotlib :code:`Figure`
    """

    fig, ax = plt.subplots()

    ax.set_title("Hash function usage per segment")
    ax.set_xlabel("Segment index")
    ax.set_ylabel("Hash function count")

    for hash_index in range(data.hash_count):
        ax.bar(
            range(data.hash_usage.shape[0]),
            data.hash_usage[:, hash_index],
            bottom=data.hash_usage[:, 0:hash_index].sum(axis=1),
        )

    ax.legend(
        labels=[f"hash {hash_index}" for hash_index in range(data.hash_count)],
        loc="lower right",
    )

    return fig


def plot_slot_usage(data: OccupancyData):
    """
    Plot the slot usage in the buckets for each segment.

    :param data: the occupancy data
    :return: the created matplotlib :code:`Figure`
    """

    fig, ax = plt.subplots()

    ax.set_title("Slot usage per segment")
    ax.set_xlabel("Segment index")
    ax.set_ylabel("Slot count")

    for slot in range(data.bucket_size):
        ax.bar(
            range(data.slot_usage.shape[0]),
            data.slot_usage[:, slot],
            bottom=data.slot_usage[:, 0:slot].sum(axis=1),
        )

    ax.legend(
        labels=[f"slot {slot}" for slot in range(data.bucket_size)], loc="lower right"
    )

    return fig


def plot_hash_usage_total(data: OccupancyData):
    """
    Plot the hash function usage in the whole table.

    :param data: the occupancy data
    :return: the created matplotlib :code:`Figure`
    """

    fig, ax = plt.subplots()

    ax.set_title("Hash function usage")
    ax.set_xlabel("Hash function")
    ax.set_ylabel("Count")

    for hash_index in range(data.hash_count):
        ax.bar(
            f"hash {hash_index}",
            data.hash_usage[:, hash_index].sum(),
        )

    ax.legend(
        labels=[f"hash {hash_index}" for hash_index in range(data.hash_count)],
        loc="upper right",
    )

    return fig


def plot_slot_usage_total(data: OccupancyData):
    """
    Plot the slot usage in the buckets in the whole table.

    :param data: the occupancy data
    :return: the created matplotlib :code:`Figure`
    """

    fig, ax = plt.subplots()

    ax.set_title("Slot usage")
    ax.set_xlabel("Slot")
    ax.set_ylabel("Count")

    for slot in range(data.bucket_size):
        ax.bar(
            f"slot {slot}",
            data.slot_usage[:, slot].sum(),
        )

    ax.legend(
        labels=[f"slot {slot}" for slot in range(data.bucket_size)], loc="upper right"
    )

    return fig
