import io
import re
from typing import Callable, Iterable

import hypothesis.strategies as st
from hypothesis import Phase, assume, given, reproduce_failure, settings
from numba import njit
from numba.extending import is_jitted

from gecocoo.config import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.config.lowlevel import resolve_config
from gecocoo.generator import generate_hashtable
from gecocoo.hashing import swaplinear, xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.valuemodules import store


def inspect_recursive(f: Callable) -> Iterable[Callable]:
    if not is_jitted(f):
        raise ValueError("function must be jit compiled")

    globals = f.py_func.__globals__
    free_vars = dict(
        zip(
            f.func_code.co_freevars,
            [c.cell_contents for c in f.py_func.__closure__ or ()],
        )
    )

    for name in [*f.func_code.co_names, *f.func_code.co_freevars]:
        referenced_function = globals.get(name, None) or free_vars.get(name, None)

        if referenced_function is not None and is_jitted(referenced_function):
            yield from inspect_recursive(referenced_function)

    yield f


def check_blacklisted_types(functions):
    blacklist = [r":: float\d*$"]

    for f in functions:
        with io.StringIO() as buffer:
            f.inspect_types(file=buffer)
            code = buffer.getvalue()

            for pattern in blacklist:
                match = re.search(pattern, code, flags=re.MULTILINE)
                assert match is None, f"{code} contains blacklisted type `{match[0]}`"


@st.composite
def valid_storage_configs(draw):
    mode = draw(
        st.sampled_from([StorageMode.PACKED, StorageMode.BUCKETS, StorageMode.FAST])
    )

    if mode == StorageMode.PACKED:
        concurrent = False
    else:
        concurrent = draw(st.booleans())

    if mode == StorageMode.FAST:
        disable_quotienting = True
    else:
        disable_quotienting = draw(st.booleans())

    return StorageConfig(
        mode=mode,
        concurrent=concurrent,
        disable_quotienting=disable_quotienting,
    )


@st.composite
def valid_configs(draw):
    k = draw(st.integers(min_value=12, max_value=32))
    bucket_size = draw(st.integers(min_value=2, max_value=8))
    value_module = ValueModuleConfig(get_qualified_name(store))
    hash_functions = draw(
        st.lists(
            elements=st.sampled_from(
                [
                    HashFunction(get_qualified_name(xorlinear), []),
                    HashFunction(get_qualified_name(swaplinear), []),
                ]
            ),
            min_size=2,
            max_size=5,
        )
    )
    storage = draw(st.one_of([st.just(None), valid_storage_configs()]))
    n = draw(st.integers(10 ** 5, 10 ** 6))

    return Config(
        load_factor=0.95,
        k=k,
        bucket_size=bucket_size,
        value_module=value_module,
        hash_functions=hash_functions,
        storage=storage,
        n=n,
    )


@given(config=valid_configs())
@settings(
    deadline=None,
    max_examples=10,
    phases=(Phase.generate,),
)
def test_valid_configs(config: Config):
    llconfig = resolve_config(config)

    # Assume that each entry is smaller than 128 bit when using storage mode FAST
    assume(
        llconfig.storage.mode != StorageMode.FAST
        or llconfig.key_bits
        + llconfig.value_bits
        + 1
        + int(llconfig.storage.concurrent)
        <= 128
    )

    (table, _) = generate_hashtable(llconfig)

    functions = [
        *inspect_recursive(table.insert_single),
        *inspect_recursive(table.remove_single),
        *inspect_recursive(table.search_single),
        # TODO: Some bit fields functions use floats, so this has to be excluded here
        # *inspect_recursive(table.initialize),
    ]
    check_blacklisted_types(functions)

    # execute some testing operations
    for key in range(500):
        table.insert_single(table.storage, key, key)
        assert table.search_single(table.storage, key) == key

    for key in range(500):
        table.remove_single(table.storage, key)

    for key in range(500):
        assert table.search_single(table.storage, key) == None

    table.clear(table.storage)

    # test table using jit compiled function
    @njit
    def test(table):
        # execute some testing operations
        for key in range(500):
            table.insert_single(table.storage, key, key + 1)
            result1 = table.search_single(table.storage, key)

            # poor man's assert as assert trips up numba here
            if result1 is None or result1 != key + 1:
                raise Exception("searching after inserting failed")

        for key in range(500):
            table.remove_single(table.storage, key)

        for key in range(500):
            result2 = table.search_single(table.storage, key)

            # see above
            if result2 is not None:
                raise Exception("searching after removing failed")

    test(table)
