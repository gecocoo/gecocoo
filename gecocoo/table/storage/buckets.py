"""The module contains the implementation for the `BUCKETS` storage mode."""

from functools import lru_cache
from math import ceil, log2
from typing import Any, Optional, Tuple

import numpy as np
from numba import njit, uint64
from numba.core.types.abstract import Type
from numba.core.types.npytypes import Array

from gecocoo.config.definition import StorageMode
from gecocoo.config.lowlevel import LLConfig
from gecocoo.utils.bitfieldsarray import (
    Bitfield,
    BitfieldsArray,
    Mode,
    make_bitfields_array,
)

from .interface import StorageAccess, get_storage_access_signatures
from .keycount import make_key_count_accessors_array


@lru_cache(maxsize=5)
def _get_storage_access(config: LLConfig):
    fields = [
        Bitfield("occupied", 1 if not config.storage.disable_occupied_flag else 0),
        # relocating is only needed for concurrent operations
        Bitfield("relocating", int(config.storage.concurrent)),
        Bitfield("hash_index", ceil(log2(len(config.hash_functions)))),
        Bitfield("key_part", config.key_bits),
        Bitfield("value", config.value_bits),
    ]
    entry_width = sum([f.width for f in fields])
    if config.storage.mode == StorageMode.PACKED:
        if config.storage.concurrent:
            mode = Mode.PACKED_LOCKED_BUCKETS
        else:
            mode = Mode.PACKED_BUCKETS
    else:
        if not config.storage.concurrent:
            mode = Mode.ALIGNED_BUCKETS
        elif entry_width <= 128:
            mode = Mode.ATOMIC_SLOTS
        else:
            mode = Mode.ALIGNED_LOCKED_BUCKETS

    bf: BitfieldsArray = make_bitfields_array(
        config.num_buckets, fields, config.bucket_size, mode
    )
    key_count_index = bf.size
    storage_type = uint64[:]
    signatures = get_storage_access_signatures(storage_type)
    disable_occupied_flag = bool(config.storage.disable_occupied_flag)

    @njit(signatures.compare_entry)
    def compare_entry(storage, index, slot, cmp_hash_index, cmp_key_part):
        occupied, relocating, hash_index, key_part, value = bf.get_entry(
            storage, index, slot
        )
        if (
            (occupied == 1 if disable_occupied_flag == False else key_part != 0)
            and cmp_hash_index == hash_index
            and cmp_key_part == key_part
        ):
            return True, relocating == 1, value

        return False, False, 0

    @njit(signatures.cmp_swap_entry)
    def cmp_swap_entry(
        storage,
        index,
        slot,
        cmp_hash_index,
        cmp_key_part,
        cmp_value,
        swap_hash_index,
        swap_key_part,
        swap_value,
    ):
        return bf.cmp_swap_entry(
            storage,
            index,
            slot,
            1,  # occupied
            0,  # relocating
            cmp_hash_index,
            cmp_key_part,
            cmp_value,
            1,  # occupied
            0,  # relocating
            swap_hash_index,
            swap_key_part,
            swap_value,
        )

    @njit(signatures.set_empty_entry)
    def set_empty_entry(storage, index, slot, relocating, hash_index, key_part, value):
        return bf.cmp_swap_entry(
            storage,
            index,
            slot,
            0,  # occupied
            0,  # relocating
            0,  # hash_index
            0,  # key_part
            0,  # value
            1,  # occupied
            int(relocating),
            hash_index,
            key_part,
            value,
        )

    @njit(signatures.clear_entry)
    def clear_entry(storage, index, slot, relocating, hash_index, key_part, value):
        return bf.cmp_swap_entry(
            storage,
            index,
            slot,
            1,  # occupied
            int(relocating),
            hash_index,
            key_part,
            value,
            0,  # occupied
            0,  # relocating
            0,  # hash_index
            0,  # key_part
            0,  # value
        )

    @njit(signatures.get_entry)
    def get_entry(storage, index, slot):
        occupied, relocating, hash_index, key_part, value = bf.get_entry(
            storage, index, slot
        )
        if disable_occupied_flag:
            occupied = int(key_part != 0)
        return occupied, relocating, hash_index, key_part, value

    @njit(signatures.mark_relocating_entry)
    def mark_relocating_entry(
        storage, index, slot, hash_index, key_part, value, relocating
    ):
        return bf.cmp_swap_entry(
            storage,
            index,
            slot,
            1,  # occupied
            int(not relocating),
            hash_index,
            key_part,
            value,
            1,  # occupied
            int(relocating),
            hash_index,
            key_part,
            value,
        )

    (
        get_key_count,
        set_key_count,
        inc_key_count,
        dec_key_count,
    ) = make_key_count_accessors_array(storage_type, key_count_index)

    access = StorageAccess(
        compare_entry=compare_entry,
        cmp_swap_entry=cmp_swap_entry,
        set_empty_entry=set_empty_entry,
        clear_entry=clear_entry,
        get_entry=get_entry,
        mark_relocating_entry=mark_relocating_entry,
        get_key_count=get_key_count,
        set_key_count=set_key_count,
        inc_key_count=inc_key_count,
        dec_key_count=dec_key_count,
    )

    # The +1 in the size is reserved for storing the key-count inside the array in the last address
    return bf.size + 1, access


def generate_storage_bitfields_buckets(
    config: LLConfig, given_array: Optional[Array] = None
) -> Tuple[Any, Type, StorageAccess]:
    """
    Generate the an instance of the `BUCKETS` storage mode for the given configuration.

    :param config: the configuration
    :param given_array: optionally an array to reuse for the storage which must have the required size
    :return:
        a tuple consisting of

        - the storage's backing object
        - the storage object's type for Numba signatures
        - the storage access abstraction functions
    """

    required_size, access = _get_storage_access(config)
    assert given_array is None or len(given_array) == required_size
    array = (
        given_array
        if given_array is not None
        else np.zeros(required_size, dtype=np.uint64)
    )
    storage_type = uint64[:]

    return array, storage_type, access
