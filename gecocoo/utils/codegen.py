"""The module contains utilities for generating python code at runtime."""

from typing import Callable, Dict, Iterable


class FunctionGenerator:
    """
    The class allows generating python code at runtime more comfortable.

    An instance represents a function to be generated.
    More lines of code can be appended until the code is actually evaluated.
    It deals with the code's indentation and provides some helpers for common
    statements like ``while`` and ``if`` using context managers.

    :param name: the function's name, defaults to ``"f"``
    :param args: defines the names of the argument of the function, defaults to an empty tuple ``tuple()``
    :param globals: a dictionary of global variables that should be accessible within the function, defaults to None
    """

    def __init__(
        self, name: str = "f", args: Iterable[str] = tuple(), globals: Dict = None
    ):
        """Create a new function generator."""

        self.name = name
        self.code = f"def {name}({', '.join(args)}):"
        self.indent = [False]
        self.generated = None
        self.globals = globals or dict()

    def __iadd__(self, line: str):
        r"""
        Append the ``line`` to the current code block.

        The code is automatically indented for the current block.

        :param line: the line to add (can be multiple lines separated by ``"\n"``)
        :raises Exception: if the function has already been generated
        """

        if self.generated:
            raise Exception("function has been generated already")

        self.indent[-1] = True
        indent_count = len(self.indent)
        self.code += "".join(
            map(lambda l: "\n" + (indent_count * "\t") + l, line.splitlines())
        )

        return self

    def __enter__(self):
        """Start a new indentation block."""

        self.indent.append(False)

    def __exit__(self, type, value, traceback):
        """
        End an indentation block.

        If a block was previously started, but no lines have been added ``pass`` is automatically
        emitted before closing the block.
        """

        if not self.indent[-1]:
            self += "pass"
        self.indent.pop()

    def block(self, line: str):
        """
        Start a new indentation block using ``line`` as the opening statement like ``if x > 5:``.

        :param line: the opening line
        """

        self += line
        return self

    def if_(self, condition: str):
        """
        Start a new ``if`` block with the given ``condition``.

        :param condition: the condition to use like ``"x > 99 and y < 42"``
        """

        return self.block(f"if {condition}:")

    def elif_(self, condition):
        """
        Start an ``elif`` block with the given ``condition``.

        :param condition: the condition to use like ``"x > 99 and y < 42"``
        """

        return self.block(f"elif {condition}:")

    def else_(self):
        """Start an ``else`` block."""

        return self.block(f"else:")

    def while_(self, condition):
        """
        Start a new ``while`` block with the given ``condition``.

        :param condition: the condition to use like ``"x > 99 and y < 42"``
        """

        return self.block(f"while {condition}:")

    def generate(self) -> Callable:
        """
        Generate a python function using the generated code.

        :return: the generated python function
        """

        if self.generated:
            return self.generated

        my_locals = dict()
        exec(str(self), self.globals, my_locals)
        self.generated = my_locals[self.name]

        return self.generated

    def generate_jit(self, *options):
        """
        Generate a python function using the generated code, but also jit-compile it using Numba.

        :param options: options passed to Numba
        :return: [description]
        """
        import numba

        return numba.njit(*options)(self.generate())

    def __str__(self):
        """Return the generated code so far."""

        code = self.code
        # Add "pass" if we are in an empty block
        if not self.indent[-1]:
            code += "\n" + (len(self.indent) * "\t") + "pass"
        return code
