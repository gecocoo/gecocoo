YAML File
=========

It is possible to define the hash table configuration in a YAML file.
This requires the module `strictyaml`_ to be installed.
Shown below is an exemplary configuration file including some parameter descriptions.
Next to it is a minimal Python example code that loads the configuration and generates a hash table from it.

.. _strictyaml: https://pypi.org/project/strictyaml

Example configuration
---------------------

.. literalinclude:: ../../../examples/example_config.yaml
    :language: yaml

Example Usage
-------------

Remember to install `strictyaml`_ as it not required to use GeCoCoo,
but is required to load a configuration file.

.. literalinclude:: ../../../examples/config.py
    :language: python