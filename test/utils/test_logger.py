import pytest

from gecocoo.utils.logger import debug, trace


@pytest.mark.parametrize("func", [trace, debug])
def test_log(mock_env, capfd, func):
    func("abc")
    captured = capfd.readouterr()
    assert captured.out == "[HASHTABLE] - [" + func.__name__.upper() + "]: abc\n"
