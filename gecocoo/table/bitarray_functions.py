"""The module provides some functions for accessing bit-precise fields in consecutive memory."""

import math

from numba import njit, uint64, void


@njit(
    uint64(uint64[:], uint64, uint64),
    locals=dict(
        start=uint64,
        chunk_index=uint64,
        chunk_bit_offset=uint64,
        result=uint64,
    ),
    inline="always",
    nogil=True,
)
def get_bits_fast(array, entry_width, index):
    """
    Return the element at the 'index' of the 'entry_width' bit per entry sized array 'array'. Only use this implementation when entry_width is a power of 2.

    Implementation based on getquick function from https://gitlab.com/cuckopt/cuckopt/-/blob/master/cuckopt/intbitarray.py.

    :param array: The array in which the element is stored
    :param entry_width: The number of bits each element takes up in the array
    :param index: The index of the element to return
    :return: The element at the index 'index'
    """
    start = index * entry_width
    chunk_index = start // 64
    chunk_bit_offset = start % 64
    result = array[chunk_index]
    return (result >> chunk_bit_offset) & (2 ** entry_width - 1)


@njit(
    void(uint64[:], uint64, uint64, uint64),
    locals=dict(
        start=uint64,
        chunk_index=uint64,
        chunk_bit_offset=uint64,
        mask=uint64,
    ),
    inline="always",
    nogil=True,
)
def set_bits_fast(array, entry_width, index, value):
    """
    Set the element at the 'index' of the 'entry_width' bit per entry sized array 'array' to 'value'. Only use this implementation when entry_width is a power of 2.

    Implementation based on setquick function from https://gitlab.com/cuckopt/cuckopt/-/blob/master/cuckopt/intbitarray.py.

    :param array: The array in which the element is stored
    :param entry_width: The number of bits each element takes up in the array
    :param index: The index of the element to set
    :param value: The new value of the element at the given parameters
    """
    start = entry_width * index
    chunk_index = start // 64
    chunk_bit_offset = start % 64
    mask = uint64(~((2 ** entry_width - 1) << chunk_bit_offset))

    array[chunk_index] = (array[chunk_index] & mask) | (value << chunk_bit_offset)


@njit(
    uint64(uint64[:], uint64, uint64),
    inline="always",
    locals=dict(
        chunk_index=uint64,
        chunk_bit_offset=uint64,
        offset_right=uint64,
        mask_left=uint64,
        result=uint64,
    ),
)
def get_bits(array, entry_width, index):
    """
    Return the element at the 'index' of the 'entry_width' bit per entry sized array 'array'. Use if entry_width is not a power of 2.

    :param array: The array in which the element is stored
    :param entry_width: The number of bits each element takes up in the array
    :param index: The index of the element to return
    :return: The element at the index 'index'
    """
    chunk_index = (index * entry_width) // 64
    chunk_bit_offset = (index * entry_width) % 64
    mask = uint64(2 ** entry_width - 1)
    if chunk_bit_offset + entry_width <= 64:
        result = array[chunk_index] >> chunk_bit_offset
    else:
        offset_right = 64 - chunk_bit_offset
        mask_left = uint64(2 ** offset_right - 1)
        result = (uint64(array[chunk_index] >> chunk_bit_offset) & mask_left) | uint64(
            array[chunk_index + 1] << offset_right
        )

    return uint64(result & mask)


@njit(
    void(uint64[:], uint64, uint64, uint64),
    inline="always",
    locals=dict(
        chunk_index=uint64,
        chunk_bit_offset=uint64,
        offset_right=uint64,
        offset_end=uint64,
        mask=uint64,
        value_left=uint64,
        mask_left=uint64,
        mask_right=uint64,
    ),
)
def set_bits(array, entry_width, index, value):
    """
    Set the element at the 'index' of the 'entry_width' bit per entry sized array 'array' to 'value'. Use if entry_width is not a power of 2.

    :param array: The array in which the element is stored
    :param entry_width: The number of bits each element takes up in the array
    :param index: The index of the element to set
    :param value: The new value of the element at the given parameters
    """
    chunk_index = (index * entry_width) // 64
    chunk_bit_offset = (index * entry_width) % 64
    if chunk_bit_offset + entry_width <= 64:
        mask = ~((2 ** entry_width - 1) << chunk_bit_offset)
        array[chunk_index] = (array[chunk_index] & mask) | (value << chunk_bit_offset)
    else:
        offset_right = 64 - chunk_bit_offset
        offset_end = entry_width - offset_right
        mask_left = uint64(2 ** chunk_bit_offset - 1)
        mask_right = ~(uint64(2 ** offset_end - 1))
        value_left = value & uint64(2 ** offset_right - 1)
        array[chunk_index] = (array[chunk_index] & mask_left) | (
            value_left << chunk_bit_offset
        )
        array[chunk_index + 1] = (array[chunk_index + 1] & mask_right) | (
            value >> offset_right
        )


@njit(uint64(uint64, uint64))
def calculate_array_size(entry_width, capacity):
    """
    Return the necessary number of 64 bit elements to store 'capacity' elements which each take up 'entry_width' bits.

    :param entry_width: The size of each element to store in bits
    :param capacity: The number of 'entry_width' sized elements to store
    :return: The number of 64 bit elements the given configuration requires
    """
    return math.ceil((capacity * entry_width) / 64)
