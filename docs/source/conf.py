# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import datetime
import inspect
import os
import sys

# Add gecocoo packeg to sys.path
sys.path.insert(0, os.path.abspath("../.."))

import gecocoo

# -- Project information -----------------------------------------------------

version = gecocoo.VERSION
project = "GeCoCoo"
copyright = f"{datetime.date.today().year}, Project Group GeCoCoo"
author = "Project Group GeCoCoo"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.coverage",
    "sphinx.ext.napoleon",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.linkcode",
    "sphinx_rtd_theme",
    "sphinx_autodoc_typehints",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

default_branch = "develop"
branch_or_tag = os.environ.get("CI_COMMIT_REF_NAME", default_branch)
repo_base_url = f"https://gitlab.com/gecocoo/gecocoo/-/tree/{branch_or_tag}/"

extlinks = {"repo_url": (f"{repo_base_url}%s", None)}

# Allow viewing code on GitLab.
# Adjusted from numpy docs:
# https://github.com/numpy/numpy/blob/af8e67c4fd368bae710b01f32611b10dfab02b61/doc/source/conf.py#L390
def linkcode_resolve(domain, info):
    """Determine the URL corresponding to Python object."""

    if domain != "py":
        return None

    modname = info["module"]
    fullname = info["fullname"]

    submod = sys.modules.get(modname)
    if submod is None:
        return None

    obj = submod
    for part in fullname.split("."):
        try:
            obj = getattr(obj, part)
        except Exception:
            return None

    # strip decorators, which would resolve to the source of the decorator
    # possibly an upstream bug in getsourcefile, bpo-1764286
    try:
        unwrap = inspect.unwrap
    except AttributeError:
        pass
    else:
        obj = unwrap(obj)

    fn = None
    lineno = None

    try:
        fn = inspect.getsourcefile(obj)
    except Exception:
        fn = None
    if not fn:
        return None

    try:
        source, lineno = inspect.getsourcelines(obj)
    except Exception:
        lineno = None

    fn = os.path.relpath(fn, start=os.path.dirname(gecocoo.__file__))

    if lineno:
        linespec = f"#L{lineno}-{lineno + len(source) - 1}"
    else:
        linespec = ""

    return f"{repo_base_url}/gecocoo/{fn}{linespec}"


# Link to dependencies
intersphinx_mapping = {
    "python": ("https://docs.python.org/3", None),
    "matplotlib": ("https://matplotlib.org/", None),
    "numpy": ("https://numpy.org/doc/stable/", None),
}

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = 'alabaster'
html_theme = "sphinx_rtd_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

# Keep navigation on open on the side while viewing a page
html_theme_options = {
    "navigation_depth": 4,
}

html_context = {
    # Link to GitLab repository: https://docs.readthedocs.io/en/stable/guides/vcs.html#gitlab
    "display_gitlab": True,
    "gitlab_user": "gecocoo",
    "gitlab_repo": "gecocoo",
    "gitlab_version": os.environ.get("CI_COMMIT_REF_NAME", "develop"),
    "conf_py_path": "/docs/source/",
}

html_static_path = ["_static"]

html_css_files = [
    "css/custom.css",
]

html_favicon = "_static/assets/favicon-32x32.png"


# Run sphinx-apidoc when building docs
# Adjusted from https://github.com/readthedocs/readthedocs.org/issues/1139#issuecomment-187884185
def run_apidoc(_):
    from sphinx.ext.apidoc import main

    modules = ["../gecocoo"]
    for module in modules:
        cur_dir = os.path.abspath(os.path.dirname(__file__))
        out_dir = os.path.join(cur_dir, "api", os.path.basename(module))
        main(
            [
                "-e",
                "-o",
                out_dir,
                module,
                "--force",
                # Put the package/module summary on top of the page
                "--module-first",
                # Make the breadcrumbs look better
                "--no-toc",
            ]
        )


def setup(app):
    app.connect("builder-inited", run_apidoc)
