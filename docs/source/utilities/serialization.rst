Serialization
=============

GeCoCoo allows saving a generated hash table with all its contents to a file.
This file will contain all the necessary configuration information from the 
hash table. This means the file will not contain any of the generated code,
but the code for the hash tables will be re-generated when the hash table is
loaded again.

Saving the hash table is possible with the method :meth:`gecocoo.HashtableUtilities.serialize`.
The method allows specifying a file to save to and a flag to enable compression
of the saved data. For details check out the methods documentation or the example below.

Loading a hash table is done using the function :func:`gecocoo.table.serialize.deserialize`.
It just expects to be given a file to load the hash table from.
The result will be similar to what is received when generating a new hash table, except
that the hash table will contain some data already.

It also possible to attach metadata to the hash table.
This is possible using the :attr:`~gecocoo.HashtableUtilities.metadata` attribute of :class:`gecocoo.HashtableUtilities`.
The value that this attribute is set to will be serialized using :class:`json.JSONEncoder`.
Note the supported data types which does only include the simple builtin types of Python.
After loading a hash table the attribute will contain the value it was set to on
the original table when the original table was saved.
Example usage of the the attribute is included below.

Example Code
------------

The following example demonstrates serializing a hash table to a file
and then loading it again from said file.

File in the repository: :repo_url:`examples/serialization.py`

.. literalinclude:: ../../../examples/serialization.py
  :language: python
  :linenos: