"""GeCoCoo is an efficient and configurable hash table for genome processing using cuckoo hashing."""

VERSION = "0.1.0"
