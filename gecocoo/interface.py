"""The module defines the user interface for hash tables generated using GeCoCoo."""

from dataclasses import dataclass
from typing import IO, Any, Callable, Dict, Iterator, NamedTuple, Optional, Tuple, Union

import numpy as np

from gecocoo.config.lowlevel import LLConfig


class HashtableInterface(NamedTuple):
    """
    The named tuple contains all the jit-compiled functions that can be used to operate on a hash table.

    All functions expect the value in :py:attr:`storage` as their first argument.
    """

    storage: Any
    insert_single: Callable[..., bool]
    initialize: Callable
    search_single: Callable[..., Any]
    remove_single: Callable[..., Optional[Any]]
    clear: Callable[[Any], None]
    key_value_iterator: Callable[[Any], Iterator[Tuple[int, Any]]]
    debug_iterator: Callable[[Any], Iterator[Tuple[int, Any, int, int, int]]]
    get_key_count: Callable[[Any], int]


HashtableInterface.storage.__doc__ = """
    The opaque object represents the storage for the hash table.
    Its implementation can change depending on the configuration.
    """
HashtableInterface.insert_single.__doc__ = """
    Insert a single key into the hash table.
    
    :param storage: the hash table's storage
    :param key: the key to insert
    :param `*values`: as many values as the value module requires
    :return: :code:`True` if the key was inserted for the first time, :code:`False` if an existing entry was updated
    :rtype: :py:class:`bool`
    """
HashtableInterface.initialize.__doc__ = """
    Initializes an empty hash table.
    It expects to be given a NumPy array of keys and as many NumPy arrays of values as needed.
    
    :param storage: the hash table's storage
    :param keys: an array of keys to insert
    :param `*values`: as many arrays of values as the value module requires
    """
HashtableInterface.search_single.__doc__ = """
    Search for a single key in the hash table.

    :param storage: the hash table's storage
    :param key: the key to search for
    :return: the value produced by the value module.
    """
HashtableInterface.remove_single.__doc__ = """
    Remove a single key from the hash table.

    :param storage: the hash table's storage
    :param key: the key to remove
    :return: the value produced by the value module or :code:`None` if the key was not found
    """
HashtableInterface.clear.__doc__ = """
    Clear the whole hash table.

    :param storage: the hash table's storage"""
HashtableInterface.key_value_iterator.__doc__ = """
    Create an iterator that yields a tuple for each entry containing the following:

    - the key 
    - the value produced by the value module

    :return: the iterator
    """
HashtableInterface.debug_iterator.__doc__ = """
    Create an iterator that yields a tuple for each entry containing the following:
    
    - the key
    - the value produced by the value module
    - the index of the hash function in use for the entry
    - the entry's bucket index
    - the entry's slot in the bucket

    The information can for example be used to debug a table or get information about its occupancy (see :func:`HashtableUtilities.occupancy`).

    :return: the iterator
    """
HashtableInterface.get_key_count.__doc__ = """
    Return the count of unique keys in the hash table.

    :return: the count
    """


@dataclass
class OccupancyData:
    """The data contains information about a hash table's occupancy."""

    bucket_count: int
    bucket_size: int
    hash_count: int
    slot_usage: Any
    hash_usage: Any


class HashtableUtilities:
    """The class provides utilities for the hash table that cannot be used in a jit-compiled context."""

    llconfig: LLConfig
    """The config used to generate the hash table"""

    table: HashtableInterface
    """The generated hash table functions"""

    metadata: Optional[Dict[str, Any]]
    """
    Metadata that will be attached to a the serialized hash table.
    If the hash table was created by de-serializing a stored hash table, this
    attribute contains the data that was stored when the hash table was serialized.
    If the hash table was freshly generated the value is ``None``.
    """

    def __init__(self, llconfig: LLConfig, table: HashtableInterface):
        """Construct a new instance."""
        self.llconfig = llconfig
        self.table = table
        self.metadata = None

    def serialize(self, file: Union[str, IO], compress: bool = False):
        """
        Serialize the current hash table to a file on the file system.

        :param file:
            Either the filename (string) or an open file (file-like object) where the data will be saved.
            See :func:`gecocoo.config.serialize` for details.
        :param compress:
            If ``True`` the saved data will be compressed using :attr:`zipfile.ZIP_DEFLATED`.
            See :func:`numpy.savez_compressed` for details.
        """

        from gecocoo.table.serialize import serialize

        # TODO: This assumes self.table.storage is Numpy array, maybe add an abstraction here later
        serialize(
            file, self.llconfig, self.table.storage, self.metadata, compress=compress
        )

    def occupancy(self, segment_count: int) -> OccupancyData:
        """
        Calculate some data about the current hash table's occupancy by dividing the whole table in the given number of segments.

        :param segment_count: the number of segments to divide the table in
        :return: the data
        """

        bucket_count = self.llconfig.num_buckets
        bucket_size = self.llconfig.bucket_size
        hash_count = len(self.llconfig.hash_functions)
        segment_size = bucket_count / segment_count

        # Count the number of keys that are in each slot index
        slot_usage = np.zeros((segment_count, bucket_size), dtype=np.uint32)
        # Count the number of keys using each hash function
        hash_usage = np.zeros((segment_count, hash_count), dtype=np.uint32)

        for _, _, hash_index, index, slot in self.table.debug_iterator(
            self.table.storage
        ):
            # Determine which segment the bucket index belongs to
            segment_index = int(index / segment_size)

            slot_usage[segment_index, slot] += 1
            hash_usage[segment_index, hash_index] += 1

        return OccupancyData(
            bucket_count=bucket_count,
            bucket_size=bucket_size,
            hash_count=hash_count,
            slot_usage=slot_usage,
            hash_usage=hash_usage,
        )

    @property
    def current_key_count(self) -> int:
        """Return the number of unique keys currently stored in the table."""
        return self.table.get_key_count(self.table.storage)

    @property
    def capacity(self) -> int:
        """Return the maximum capcity of the table."""
        return self.llconfig.num_buckets * self.llconfig.bucket_size

    @property
    def current_load_factor(self) -> float:
        """Return the current load factor of the table."""
        return self.current_key_count / self.capacity

    @property
    def byte_size(self) -> int:
        """Return the number of bytes approximatly needed for the table in memory."""
        # TODO: This assumes the table is stored in a single numpy array, which could change.
        return self.table.storage.nbytes
